<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define some reusable logic or constants
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Config for tracking URL
 */
const QUIZACCESS_QUILGO_PLASM_URL = 'https://moodle.integrations.quilgo.com';
/**
 * Config for tracking api prefix
 */
const QUIZACCESS_QUILGO_API_PREFIX = 'api/v1';
/**
 * Config for max shots per quiz
 */
const QUIZACCESS_QUILGO_PLASM_MAX_SHOTS_PER_QUIZ = 30;
/**
 * Config for image on report expiration in days
 */
const QUIZACCESS_QUILGO_PLASM_REPORTS_EXPIRE_IN_DAYS = 90;
/**
 * Config for plugin API token stored setting in moodle
 */
const QUIZACCESS_QUILGO_CONFIG_API_TOKEN_NAME = 'api_token';
/**
 * Config for plugin client token stored setting in moodle
 */
const QUIZACCESS_QUILGO_CONFIG_CLIENT_TOKEN_NAME = 'client_token';

/**
 * Config for tracking report file path
 */
const QUIZACCESS_QUILGO_REPORT_FILE_PATH = '/mod/quiz/accessrule/quilgo/report.php';
/**
 * Config for plugin API settings file path
 */
const QUIZACCESS_QUILGO_API_SETTINGS_PATH = '/mod/quiz/accessrule/quilgo/api_config.php';
/**
 * Config for plugin questions file path (for iframe inside individual report)
 */
const QUIZACCESS_QUILGO_QUESTIONS_FILE_PATH = '/mod/quiz/accessrule/quilgo/questions.php';

/**
 * Code for stored report status queued
 */
const QUIZACCESS_QUILGO_REPORT_STATUS_QUEUED = -1;
/**
 * Code for stored report status not ready
 */
const QUIZACCESS_QUILGO_REPORT_STATUS_NOT_READY = 0;
/**
 * Code for stored report status ready
 */
const QUIZACCESS_QUILGO_REPORT_STATUS_READY = 1;

/**
 * Code for confidence level low
 */
const QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_LOW = 'low';
/**
 * Code for confidence level moderate
 */
const QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_MODERATE = 'moderate';
/**
 * Code for confidence level high
 */
const QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_HIGH = 'high';

/**
 * Code for copy paste tracking pattern
 */
const QUIZACCESS_QUILGO_PATTERN_TYPE_COPY_PASTE = 'copy-paste';
/**
 * Code for copy change tracking pattern
 */
const QUIZACCESS_QUILGO_PATTERN_TYPE_COPY_CHANGE = 'copy-change';
/**
 * Code for multiple screens tracking pattern
 */
const QUIZACCESS_QUILGO_PATTERN_TYPE_MULTIPLE_SCREENS = 'multiple-screens';
/**
 * Config sentry DSN
 */
const QUIZACCESS_QUILGO_SENTRY_DSN =
    "https://a3b31d52600268dc989806cb16a7aa89@o4505250149236736.ingest.us.sentry.io/4507463553056768";
/**
 * Config sentry env
 * Possible values dev/integrations/preprod/staging
 */
const QUIZACCESS_QUILGO_SENTRY_ENV = "integrations";

/**
 * Code for create session error limit reached
 */
const QUIZACCESS_QUILGO_CREATE_SESSION_ERROR_LIMIT_REACHED = 'create_session_error_limit_reached';

/**
 * Code for create session error default
 */
const QUIZACCESS_QUILGO_CREATE_SESSION_ERROR_DEFAULT = 'create_session_error_default';

/**
 * Config for plugin when request ask later for email confirmation stored setting in moodle
 */
const QUIZACCESS_QUILGO_CONFIG_REQUEST_ASK_LATER_FOR_EMAIL_CONFIMRATION_AT = 'request_ask_later_for_email_confirmation_at';

/**
 * Config for plugin confirmed email in moodle
 */
const QUIZACCESS_QUILGO_CONFIG_CONFIRMED_EMAIL = 'confirmed_email';

/**
 * Check is Quilgo plugin configured properly or not
 *
 * @return bool
 */
function quizaccess_quilgo_is_plugin_configured() {
    $apitoken = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_API_TOKEN_NAME);
    $clienttoken = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_CLIENT_TOKEN_NAME);

    return !empty($apitoken) && !empty($clienttoken);
}

/**
 * Get Quilgo Plasm client library URL
 *
 * @return string
 */
function quizaccess_quilgo_get_plasm_lib_url() {
    return QUIZACCESS_QUILGO_PLASM_URL . '/public/collector.min.js';
}

/**
 * Get Quilgo Plasm tracking configuration
 *
 * @return object
 */
function quizaccess_quilgo_get_plasm_config() {
    $plasmconfig = new stdClass();
    $plasmconfig->clientToken = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_CLIENT_TOKEN_NAME);
    $plasmconfig->apiEndpoint = QUIZACCESS_QUILGO_PLASM_URL . '/api/v1/&';
    $plasmconfig->maxShotsPerQuiz = QUIZACCESS_QUILGO_PLASM_MAX_SHOTS_PER_QUIZ;

    return $plasmconfig;
}

/**
 * Build quiz object before processed on Quilgo plugin
 * @param mixed $quiz Moodle quiz data
 * @param string $plasmsessionid Quilgo Plasm session id
 *
 * @return object
 */
function quizaccess_quilgo_get_quiz_data($quiz, $plasmsessionid = null) {
    $quizdata = new stdClass();
    $quizdata->id = $quiz->id;
    $quizdata->plasm_screen = $quiz->plasm_screen ?? 0;
    $quizdata->plasm_camera = $quiz->plasm_camera ?? 0;
    $quizdata->plasm_force = $quiz->plasm_force ?? 0;
    $quizdata->plasm_sessionid = $plasmsessionid;
    $quizdata->timeclose = $quiz->timeclose;
    $quizdata->timelimit = $quiz->timelimit;

    return $quizdata;
}

/**
 * Build report url
 * @param mixed $cid Moodle course id
 * @param mixed $cmid Moodle course module id
 * @param mixed $quizid Moodle quiz id
 *
 * @return moodle_url
 */
function quizaccess_quilgo_get_report_url($cid, $cmid, $quizid) {
    $reportparameters = [
        'cid' => $cid,
        'cmid' => $cmid,
        'qid' => $quizid,
    ];

    return new moodle_url(QUIZACCESS_QUILGO_REPORT_FILE_PATH, $reportparameters);
}

/**
 * Get quiz attempt by attemptid
 * @param mixed $attemptid
 *
 * @return mixed
 */
function quizaccess_quilgo_get_attempt($attemptid) {
    global $DB;

    $sql = "SELECT * FROM {quiz_attempts} WHERE id = :attemptid";
    $attempt = $DB->get_record_sql($sql, ['attemptid' => $attemptid]);

    return $attempt ? $attempt : null;
}

/**
 * Get quiz attempt session by attemptid
 * @param mixed $attemptid
 *
 * @return mixed
 */
function quizaccess_quilgo_get_attempt_session($attemptid) {
    global $DB;

    $sql = "SELECT * FROM {quizaccess_quilgo_reports} WHERE attemptid = :attemptid";
    $attemptreportrow = $DB->get_record_sql($sql, ['attemptid' => $attemptid]);

    return $attemptreportrow ? $attemptreportrow : null;
}

/**
 * Showing debugging data
 * @param string $string String data that needs to be shown
 *
 * @return void
 */
function quizaccess_quilgo_show_error_log($string) {
    debugging("QUIZACCESS QUILGO ERROR");
    debugging($string);
}

/**
 * Count report images expiration in days
 * @param int $timefinish Attempt timefinish in unix timestamp
 *
 * @return int
 */
function quizaccess_quilgo_build_report_expires($timefinish) {
    $configexpiration = QUIZACCESS_QUILGO_PLASM_REPORTS_EXPIRE_IN_DAYS;
    $currenttime = new DateTime(date("Y-m-d", time()));
    $expirestime = new DateTime(date("Y-m-d", strtotime("+$configexpiration days", $timefinish)));
    $diffinterval = $currenttime->diff($expirestime);
    $diffintervalindays = (int) $diffinterval->format("%r%a");

    return $diffintervalindays;
}

/**
 * Register moodle site to Quilgo
 *
 * @return mixed
 */
function quizaccess_quilgo_register_site() {
    $plasmapi = new \quizaccess_quilgo\plasmapi();
    $resp = null;

    try {
        $resp = $plasmapi->register();
    } catch (\Throwable $th) {
        $resp = null;
    }

    $apitoken = '';
    $clienttoken = '';
    if (!empty($resp)) {
        $apitoken = $resp->apiToken;
        $clienttoken = $resp->clientToken;
    }

    set_config(QUIZACCESS_QUILGO_CONFIG_API_TOKEN_NAME, $apitoken, 'quizaccess_quilgo');
    set_config(QUIZACCESS_QUILGO_CONFIG_CLIENT_TOKEN_NAME, $clienttoken, 'quizaccess_quilgo');

    return $resp;
}

/**
 * Calculate report confidence level
 * @param int $cameraenabled Quiz attempt camera tracking setting
 * @param object $reportstat Quiz attempt stat
 *
 * @return string
 */
function quizaccess_quilgo_define_confidence_level($cameraenabled, $reportstat) {
    if (!empty($reportstat->patterns) || (!empty($reportstat->pageUnfocusedCount) && $reportstat->pageUnfocusedCount > 0)) {
        return QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_LOW;
    }

    if ($cameraenabled == 0) {
        return QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_MODERATE;
    }

    if (
        empty($reportstat->maxFacesPerFrame) ||
        empty($reportstat->facesPresence) ||
        $reportstat->maxFacesPerFrame > 1 ||
        $reportstat->facesPresence < 0.7
    ) {
        return QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_LOW;
    }

    if ($reportstat->facesPresence >= 0.9) {
        return QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_HIGH;
    }

    return QUIZACCESS_QUILGO_CONFIDENCE_LEVEL_MODERATE;
}

/**
 * Get iframe questions url
 * @param mixed $cmid Moodle course module id
 * @param mixed $attemptid Quiz attempt id
 * @param string $patternsquery Patterns detected query parameters
 *
 * @return moodle_url
 */
function quizaccess_quilgo_get_questions_url($cmid, $attemptid, $patternsquery) {
    $questionsparameters = [
        'cmid' => $cmid,
        'attemptid' => $attemptid,
        'patterns' => $patternsquery,
    ];

    return new moodle_url(QUIZACCESS_QUILGO_QUESTIONS_FILE_PATH, $questionsparameters);
}

/**
 * Get plugin version
 * @return int
 */
function quizaccess_quilgo_get_plugin_version() {
    $plugin = get_config("quizaccess_quilgo");
    return $plugin->version;
}

/**
 * Get sentry client options
 * @return object
 */
function quizaccess_quilgo_get_sentry_client_options() {
    global $CFG;

    $pluginversion = quizaccess_quilgo_get_plugin_version();

    $sentryoptions = new stdClass();
    $sentryoptions->pluginVersion = $pluginversion;
    $sentryoptions->sentryDSN = QUIZACCESS_QUILGO_SENTRY_DSN;
    $sentryoptions->env = QUIZACCESS_QUILGO_SENTRY_ENV;
    $sentryoptions->moodleVersion = $CFG->release;

    return $sentryoptions;
}

/**
 * Get application dashboard url
 * @param mixed $token
 * @return string
 */
function quizaccess_quilgo_get_application_dashboard_url($token) {
    return QUIZACCESS_QUILGO_PLASM_URL . '/dashboard?t=' . $token;
}

/**
 * Check is Moodle app should confirm email or not
 *
 * @return bool
 */
function quizaccess_quilgo_is_should_confirm_email() {
    $confirmedemail = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_CONFIRMED_EMAIL);
    if (!empty($confirmedemail)) {
        return false;
    }

    $requestasklaterat = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_REQUEST_ASK_LATER_FOR_EMAIL_CONFIMRATION_AT);
    if (!empty($requestasklaterat)) {
        $currenttime = new DateTime(date("Y-m-d H:i:s", time()));
        $requestasklaterattime = new DateTime(date("Y-m-d H:i:s", $requestasklaterat));
        $difftime = $requestasklaterattime->diff($currenttime);
        $diffintervalindays = (int) $difftime->format("%r%a");

        return $diffintervalindays > 0;
    }

    return true;
}
