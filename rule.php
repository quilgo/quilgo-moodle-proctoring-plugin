<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

use quizaccess_quilgo\plasmapi;

// In moodle 4.1 or lower there is update for access rule base class.
if (class_exists('\mod_quiz\local\access_rule_base')) {
    class_alias('\mod_quiz\local\access_rule_base', '\quizaccess_quilgo_parent_class_alias');
    class_alias('\mod_quiz\form\preflight_check_form', '\quizaccess_quilgo_preflight_form_alias');
    class_alias('\mod_quiz\quiz_settings', '\quizaccess_quilgo_quiz_settings_class_alias');
} else {
    require_once($CFG->dirroot . '/mod/quiz/accessrule/accessrulebase.php');
    class_alias('\quiz_access_rule_base', '\quizaccess_quilgo_parent_class_alias');
    class_alias('\mod_quiz_preflight_check_form', '\quizaccess_quilgo_preflight_form_alias');
    class_alias('\quiz', '\quizaccess_quilgo_quiz_settings_class_alias');
}

require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');

/**
 * Rule implementation of the quilgo proctoring plugin
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class quizaccess_quilgo extends quizaccess_quilgo_parent_class_alias {

    /**
     * Handle some logic for quiz setting fields
     * @param mod_quiz_mod_form $quizform Quiz form data
     * @param MoodleQuickForm $mform Quick form element
     *
     * @return void
     */
    public static function add_settings_form_fields(mod_quiz_mod_form $quizform, MoodleQuickForm $mform) {
        global $OUTPUT, $PAGE;

        if (!quizaccess_quilgo_is_plugin_configured()) {
            return;
        }

        $PAGE->requires->css('/mod/quiz/accessrule/quilgo/settings.css');
        $settingelements = ['enabled', 'focus', 'camera', 'screen', 'force'];
        $settings = [];
        foreach ($settingelements as $setting) {
            $settingtarget = 'plasm_' . $setting;
            $helpicon = $OUTPUT->render(new help_icon($settingtarget, 'quizaccess_quilgo'));
            if ($setting == 'focus') {
                $settingcaption = html_writer::span(get_string($settingtarget, 'quizaccess_quilgo'), 'mr-2');
                $element = $mform->createElement(
                    'html',
                    html_writer::span(
                        $OUTPUT->pix_icon('t/check', $settingtarget, 'moodle', ['class' => 'mr-1']) . $settingcaption . $helpicon,
                    ),
                );
            } else {
                $settingcaption = get_string($settingtarget, 'quizaccess_quilgo');
                $element = $mform->createElement(
                    'checkbox',
                    $settingtarget,
                    $settingcaption,
                    $setting === 'enabled' ? '' : $helpicon,
                    ['class' => 'mr-2']
                );
            }

            $settings[] = $mform->createElement('html', html_writer::start_div('plasm-setting-option d-flex flex-wrap mb-3'));
            $settings[] = $element;
            $settings[] = $mform->createElement('html', html_writer::end_div());
        }
        $PAGE->requires->js_call_amd('quizaccess_quilgo/setting', 'initQuizSettingActions');

        $hassiteconfig = has_capability('moodle/site:config', context_system::instance());
        $settings[] = $mform->createElement(
            'html',
            html_writer::start_div('plasm-setting-option d-none mb-3 alert alert-warning upgrade-failed-info'),
        );
        $settings[] = $mform->createElement('html', get_string('upgrade_failed_info', 'quizaccess_quilgo'));
        $settings[] = $mform->createElement('html', html_writer::end_div());

        $limitinfoicon = html_writer::tag('i', '', ['class' => 'fa fa-info-circle']);
        $limitinforunup = html_writer::span(get_string('limit_info_run_up', 'quizaccess_quilgo'));
        $limitinfoaction = '';
        if ($hassiteconfig) {
            $loadingicon = html_writer::tag(
                'i',
                '',
                ['class' => 'fa fa-circle-notch fa-spin mr-1 d-none upgrade-cta-loading-icon']
            );
            $limitinfoaction = html_writer::div(get_string('limit_info_upgrade', 'quizaccess_quilgo', $loadingicon), 'mt-2');
        } else {
            $limitinfoaction = html_writer::div(get_string('limit_info_contact_admin', 'quizaccess_quilgo'), 'mt-2');
        }

        $settings[] = $mform->createElement(
            'html',
            html_writer::start_div('plasm-setting-option d-none mb-3 alert alert-info upgrade-info-container'),
        );
        $settings[] = $mform->createElement('html', "$limitinfoicon $limitinforunup $limitinfoaction");
        $settings[] = $mform->createElement('html', html_writer::end_div());

        // Do not call application script when quiz (course module) not created yet.
        // Example case open new create new quiz.
        $coursemodule = $quizform->get_coursemodule();
        if (!empty($coursemodule)) {
            $PAGE->requires->js_call_amd(
                'quizaccess_quilgo/application',
                'initQuizSettingInfo',
                [$quizform->get_coursemodule()->id]
            );
        }

        $mform->addGroup($settings, 'quizaccess_quilgo_setting', get_string('setting_group', 'quizaccess_quilgo'), '', false);
    }

    /**
     * Handle logic for store quilgo proctoring settings
     * @param mixed $quiz Quiz data
     * @return void
     */
    public static function save_settings($quiz) {
        global $DB;

        $currentsetting = $DB->get_record('quizaccess_quilgo_settings', ['quizid' => $quiz->id]);
        $settingrecord = new stdClass();
        if (isset($quiz->plasm_enabled)) {
            $settingrecord->proctoring_enabled = 1;
            $settingrecord->camera_enabled = isset($quiz->plasm_camera) ? 1 : 0;
            $settingrecord->screen_enabled = isset($quiz->plasm_screen) ? 1 : 0;

            $settingrecord->force_enabled = isset($quiz->plasm_force) ? 1 : 0;
            if ($settingrecord->camera_enabled == 0 && $settingrecord->screen_enabled == 0) {
                $settingrecord->force_enabled = 0;
            }
        } else {
            $settingrecord->proctoring_enabled = 0;
            $settingrecord->camera_enabled = 0;
            $settingrecord->screen_enabled = 0;
            $settingrecord->force_enabled = 0;
        }

        if ($currentsetting) {
            $settingrecord->id = $currentsetting->id;
            $DB->update_record('quizaccess_quilgo_settings', $settingrecord);
        } else {
            $settingrecord->quizid = $quiz->id;
            $DB->insert_record('quizaccess_quilgo_settings', $settingrecord);
        }
    }

    /**
     * Preflight form checking
     * Always check Quilgo preflight on view.php
     * When user on startattempt.php, path will still on view.php
     *
     * @param int|null $attemptid
     *
     * @return bool
     */
    public function is_preflight_check_required($attemptid) {
        global $PAGE;

        if (!empty($attemptid)) {
            $attemptsession = quizaccess_quilgo_get_attempt_session($attemptid);
            if (!empty($attemptsession) && $attemptsession->proctoring_enabled == 0) {
                return false;
            }
        }

        $isonview = strpos($PAGE->url->get_path(), 'view.php');

        return !!$isonview;
    }

    /**
     * Preflight form validation
     * Do not re-initialize $errors
     * because it will cause the previous errors message from other access rule quiz plugin
     * not shown to the users
     *
     * @param mixed $data Form data
     * @param mixed $files Form files
     * @param array $errors Form errors data
     * @param int|null $attemptid Attempt id
     *
     * @return array
     */
    public function validate_preflight_check($data, $files, $errors, $attemptid) {
        global $SESSION;

        // Ignore all quilgo preflight check when $errors not empty.
        if (!empty($errors)) {
            return $errors;
        }

        // Re-assign quiz plasm_enabled from existing attempt session if exist.
        $plasmsessionid = null;
        if (!empty($attemptid)) {
            $attemptsession = quizaccess_quilgo_get_attempt_session($attemptid);
            if (!empty($attemptsession)) {
                $plasmsessionid = $attemptsession->plasmsessionid;
                $this->quiz->plasm_enabled = $attemptsession->proctoring_enabled;
                $this->quiz->plasm_camera = $attemptsession->camera_enabled;
                $this->quiz->plasm_screen = $attemptsession->screen_enabled;
                $this->quiz->plasm_force = $attemptsession->force_enabled;
            }
        }

        if ($this->quiz->plasm_enabled != 0) {
            if (isset($data['quilgocheckfinished']) && empty($data['quilgocheckfinished'])) {
                $createsessionerror = 'create_session_error_default';
                $errors['quilgocheckfinished'] = get_string("{$createsessionerror}_description", 'quizaccess_quilgo');
            } else {
                if (!empty($plasmsessionid)) {
                    $SESSION->quilgosessiontracking[$this->quiz->id] = $plasmsessionid;
                } else {
                    $sessionexpiresin = $data['quilgosessionduration'];
                    $plasmapi = new plasmapi();
                    $createdsessionresponse = $plasmapi->create_session($sessionexpiresin);
                    if (!empty($createdsessionresponse->errorcode)) {
                        $createsessionerror = $createdsessionresponse->errorcode;
                        $errors['quilgocheckfinished'] = get_string("{$createsessionerror}_description", 'quizaccess_quilgo');
                    } else {
                        $SESSION->quilgosessiontracking[$this->quiz->id] = $createdsessionresponse->uuid;
                    }
                }
            }
        } else {
            if (!empty($SESSION->quilgosessiontracking[$this->quiz->id])) {
                unset($SESSION->quilgosessiontracking[$this->quiz->id]);
            }
        }

        return $errors;
    }

    /**
     * Build loading setup HTML strings screen
     * @return string
     */
    protected function build_loading_screen_setup() {
        $contents = html_writer::start_div('plasm-loading-setup quilgo-hidden d-flex flex-column align-items-center');
        $contents .= html_writer::tag('div', '', ['class' => 'spinner-border text-primary']);
        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build consent HTML strings screen
     * @return string
     */
    protected function build_consent() {
        $contents = html_writer::start_div('plasm-consent quilgo-hidden d-flex flex-column');
        $camerahinttitle = html_writer::tag(
            'span',
            get_string('setup_camera_hint', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-camera quilgo-hidden']
        );
        $hintconnectiontitle = html_writer::tag(
            'span',
            get_string('setup_connection_hint', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-camera-and-screen quilgo-hidden']
        );
        $screenhinttitle = html_writer::tag(
            'span',
            get_string('setup_screen_hint', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-screen quilgo-hidden']
        );
        $trackingenabledhinttitle = html_writer::tag(
            'span',
            get_string('setup_tracking_enabled_hint', 'quizaccess_quilgo'),
        );
        $setuptitle = "$camerahinttitle $hintconnectiontitle $screenhinttitle $trackingenabledhinttitle";
        $contents .= html_writer::tag('h5', $setuptitle, ['class' => 'text-center mt-1 mb-3 consent-title']);

        $consentcontents = "";
        $consentcontentattributes = ['class' => 'plasm-consent-item'];
        $consentneedprovideaccess = get_string('setup_consent_to_start_quiz', 'quizaccess_quilgo');
        $consentneedprovideaccess = "$consentneedprovideaccess $camerahinttitle $hintconnectiontitle $screenhinttitle";
        $consentcontents .= html_writer::tag(
            'li',
            $consentneedprovideaccess,
            $consentcontentattributes,
        );

        $snapshotsfrom = get_string('setup_consent_snapshots_from', 'quizaccess_quilgo');
        $consentsnapshotstaken = get_string('setup_consent_snapshots_will_taken', 'quizaccess_quilgo');
        $snapshotsfrom = "$snapshotsfrom $camerahinttitle $hintconnectiontitle $screenhinttitle $consentsnapshotstaken";
        $consentcontents .= html_writer::tag(
            'li',
            $snapshotsfrom,
        );
        $consentcontents .= html_writer::tag(
            'li',
            get_string('setup_consent_provided_report', 'quizaccess_quilgo'),
        );
        $contents .= html_writer::tag('ul', $consentcontents);

        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build warning message about disable device
     * @param mixed $quizdata quiz data build from quizaccess_quilgo_get_quiz_data
     * @return string
     */
    protected function build_warning_disable_device($quizdata) {
        $contents = html_writer::start_div('quilgo-warning-disable-device quilgo-hidden d-flex flex-column');
        $contents .= html_writer::tag('i', '', ['class' => 'fa fa-exclamation-triangle fa-4x text-center text-warning']);
        $contents .= html_writer::tag(
            'h4',
            get_string('setup_disable_device_warning_attention', 'quizaccess_quilgo'),
            ['class' => 'text-center my-2']
        );

        $warningdescription = '';
        $warningcheckcaption = '';
        if ($quizdata->plasm_camera == 1 && $quizdata->plasm_screen == 1) {
            $warningdescription = get_string('setup_disable_device_warning_description_camera_or_screen', 'quizaccess_quilgo');
            $warningcheckcaption = get_string('setup_disable_device_warning_check_camera_or_screen', 'quizaccess_quilgo');
        } else if ($quizdata->plasm_camera == 1 && $quizdata->plasm_screen == 0) {
            $warningdescription = get_string('setup_disable_device_warning_description_camera_only', 'quizaccess_quilgo');
            $warningcheckcaption = get_string('setup_disable_device_warning_check_camera_only', 'quizaccess_quilgo');
        } else if ($quizdata->plasm_camera == 0 && $quizdata->plasm_screen == 1) {
            $warningdescription = get_string('setup_disable_device_warning_description_screen_only', 'quizaccess_quilgo');
            $warningcheckcaption = get_string('setup_disable_device_warning_check_screen_only', 'quizaccess_quilgo');
        }

        $contents .= html_writer::tag('div', $warningdescription, ['class' => 'mt-2 mb-4']);

        $disabledevicewarninglabel = html_writer::tag(
            'label',
            $warningcheckcaption,
            [
                'for' => 'quilgo-disable-device-warning-check',
                'class' => 'ml-2 font-weight-bold quilgo-disable-device-warning-check',
            ]
        );
        $disabledevicewarningcheck = html_writer::tag(
            'input',
            $disabledevicewarninglabel,
            [
                'type' => 'checkbox',
                'id' => 'quilgo-disable-device-warning-check',
            ],
        );
        $contents .= html_writer::tag(
            'div',
            $disabledevicewarningcheck,
            ['class' => 'mb-1 d-flex flex-row align-items-baseline text-break']
        );

        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build setup HTML strings screen
     * @return string
     */
    protected function build_setup() {
        $contents = html_writer::start_div('plasm-setup quilgo-hidden d-flex flex-column align-items-center');
        $contents .= html_writer::tag(
            'button',
            get_string('setup_provide_access_camera', 'quizaccess_quilgo'),
            ['class' => 'plasm-setup-camera plasm-setup-collector btn btn-primary mt-2'],
        );

        $cameraenabledhint = html_writer::start_div(
            'plasm-setup-camera-enabled-hint quilgo-hidden d-flex flex-column align-items-center'
        );
        $cameraenabledhint .= html_writer::tag(
            'h5',
            get_string('setup-additional-collector-title', 'quizaccess_quilgo'),
            ['class' => 'mt-1 mb-3'],
        );
        $cameraenabledhint .= html_writer::tag(
            'p',
            get_string('setup-additional-collector-description', 'quizaccess_quilgo'),
        );
        $cameraenabledhint .= html_writer::end_div();

        $contents .= $cameraenabledhint;
        $contents .= html_writer::tag(
            'button',
            get_string('setup_provide_access_screen', 'quizaccess_quilgo'),
            ['class' => 'plasm-setup-screen plasm-setup-collector btn btn-primary mt-2'],
        );

        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build setup error HTML strings screen
     * @param string $containerclass Container class name
     * @param string $title Content title
     * @param string $desc Content description
     * @param string $buttonclass Retry button class name
     *
     * @return string
     */
    protected function build_setup_error($containerclass, $title, $desc, $buttonclass) {
        $contents = html_writer::start_div("$containerclass plasm-error quilgo-hidden d-flex flex-column align-items-center");
        $contents .= html_writer::tag('h5', $title, ['class' => 'mt-1 mb-3']);
        $contents .= html_writer::tag('p', $desc, ['class' => 'text-center']);
        $contents .= html_writer::tag(
            'button',
            get_string('setup_retry', 'quizaccess_quilgo'),
            ['class' => "$buttonclass btn btn-primary mt-2"],
        );
        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build not supported error HTML strings screen
     * @return string
     */
    protected function build_not_supported_error() {
        $contents = html_writer::start_div('plasm-not-supported plasm-error quilgo-hidden d-flex flex-column align-items-center');
        $contents .= html_writer::tag(
            'h5',
            get_string('setup_not_supported_error_title', 'quizaccess_quilgo')
        );
        $contents .= html_writer::tag(
            'p',
            get_string('setup_not_supported_error_desc', 'quizaccess_quilgo'),
            ['class' => 'text-center']
        );
        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Build finish setup HTML strings screen
     * @return string
     */
    protected function build_finish_step() {
        $contents = html_writer::start_div('plasm-caliber quilgo-hidden d-flex flex-column align-items-center');

        $title = html_writer::tag(
            'span',
            get_string('setup_finish_title', 'quizaccess_quilgo'),
        );
        $camera = html_writer::tag(
            'span',
            get_string('setup_finish_yourself', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-camera ml-1 quilgo-hidden']
        );
        $connection = html_writer::tag(
            'span',
            get_string('setup_connection_hint', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-camera-and-screen ml-1 quilgo-hidden']
        );
        $screen = html_writer::tag(
            'span',
            get_string('setup_finish_your_screen', 'quizaccess_quilgo'),
            ['class' => 'plasm-hint-screen ml-1 quilgo-hidden']
        );
        $titlecontents = "$title$camera$connection$screen?";
        $contents .= html_writer::tag('h5', $titlecontents, ['class' => 'mt-1 mb-3']);

        $previewcontents = html_writer::start_div('d-flex flex-column flex-md-row');
        $cameracontents = html_writer::start_div('quilgo-video plasm-hint-camera bg-secondary', ['id' => 'quilgo-video']);
        $cameracontents .= html_writer::tag('video', '');
        $cameracontents .= html_writer::end_div();
        $previewcontents .= $cameracontents;

        $screencontents = html_writer::start_div(
            'quilgo-video plasm-hint-screen bg-secondary',
            ['id' => 'plasm-screen-video']
        );
        $screencontents .= html_writer::tag('video', '');
        $screencontents .= html_writer::end_div();
        $previewcontents .= $screencontents;
        $previewcontents .= html_writer::end_div();
        $contents .= $previewcontents;

        $contents .= html_writer::tag('p', get_string('setup_finish_tick_box', 'quizaccess_quilgo'), ['class' => 'w-100 mt-2']);
        $contents .= html_writer::end_div();

        return $contents;
    }

    /**
     * Add checkbox consent to form
     * @param MoodleQuickForm $mform Moodle form
     * @param mixed $quizdata quiz data build from quizaccess_quilgo_get_quiz_data
     *
     * @return void
     */
    public static function build_consent_checkbox($mform, $quizdata) {
        $contents = "";
        if ($quizdata->plasm_camera == 1 && $quizdata->plasm_screen == 1) {
            $contents = get_string('setup_consent_camera_and_screen_tracking_enabled', 'quizaccess_quilgo');
        } else if ($quizdata->plasm_camera == 1 && $quizdata->plasm_screen == 0) {
            $contents = get_string('setup_consent_camera_tracking_enabled', 'quizaccess_quilgo');
        } else if ($quizdata->plasm_camera == 0 && $quizdata->plasm_screen == 1) {
            $contents = get_string('setup_consent_screen_tracking_enabled', 'quizaccess_quilgo');
        } else {
            $contents = get_string('setup_consent_activity_tracking_enabled', 'quizaccess_quilgo');
        }

        $mform->addElement('checkbox', 'quilgocheckfinished', '', $contents, ['class' => 'font-weight-bold']);
        $mform->addElement('hidden', 'quilgosessionduration');
        $mform->setType('quilgosessionduration', PARAM_FLOAT);

        $todisabledevicewarningstep = html_writer::tag(
            'button',
            get_string('general_continue', 'quizaccess_quilgo'),
            ['class' => 'quilgo-to-disable-device-warning quilgo-hidden btn btn-primary'],
        );
        $todisabledevicewarningstep = html_writer::tag(
            'div',
            $todisabledevicewarningstep,
            ['class' => 'd-flex flex-row justify-content-center mb-2']
        );
        $mform->addElement('html', $todisabledevicewarningstep);
    }

    /**
     * Define should show preflight form or not
     * @param mixed $quizobj
     * @param mixed $timenow
     * @param mixed $canignoretimelimits
     * @return mixed
     */
    public static function make($quizobj, $timenow, $canignoretimelimits) {
        return quizaccess_quilgo_is_plugin_configured() ? new self($quizobj, $timenow) : null;
    }

    /**
     * Add fields to preflight form
     * @param quizaccess_quilgo_preflight_form_alias $quizform
     * @param MoodleQuickForm $mform
     * @param int $attemptid
     *
     * @return void
     */
    public function add_preflight_check_form_fields(
        quizaccess_quilgo_preflight_form_alias $quizform,
        MoodleQuickForm $mform,
        $attemptid
    ) {
        global $PAGE, $DB, $USER, $CFG, $OUTPUT, $SESSION;

        $plasmsessionid = null;
        if (!empty($attemptid)) {
            $attemptsession = quizaccess_quilgo_get_attempt_session($attemptid);
            if (!empty($attemptsession)) {
                $plasmsessionid = $attemptsession->plasmsessionid;
                $this->quiz->plasm_enabled = $attemptsession->proctoring_enabled;
                $this->quiz->plasm_camera = $attemptsession->camera_enabled;
                $this->quiz->plasm_screen = $attemptsession->screen_enabled;
                $this->quiz->plasm_force = $attemptsession->force_enabled;
            }
        }

        if ($this->quiz->plasm_enabled == 0) {
            return;
        }

        $PAGE->requires->css('/mod/quiz/accessrule/quilgo/styles.css');
        $PAGE->requires->js(new moodle_url(quizaccess_quilgo_get_plasm_lib_url()), true);

        $plasmconfig = quizaccess_quilgo_get_plasm_config();
        $quizdata = quizaccess_quilgo_get_quiz_data($this->quiz, $plasmsessionid);
        $datenow = new DateTime();

        $attemptstarttime = null;
        if (!empty($attemptid)) {
            $attempt = quizaccess_quilgo_get_attempt($attemptid);
            $attemptstarttime = $attempt ? $attempt->timestart : null;
        }

        $sentryoptions = quizaccess_quilgo_get_sentry_client_options();

        $cameraaccesserrorscreen = $this->build_setup_error(
            'plasm-camera-access-error',
            get_string('setup_camera_error_title', 'quizaccess_quilgo'),
            get_string('setup_camera_error_desc', 'quizaccess_quilgo'),
            'plasm-setup-camera-retry'
        );
        $screenaccesserrorscreen = $this->build_setup_error(
            'plasm-screen-access-error',
            get_string('setup_screen_error_title', 'quizaccess_quilgo'),
            get_string('setup_screen_error_desc', 'quizaccess_quilgo'),
            'plasm-setup-screen-retry'
        );
        $screenareaerrorscreen = $this->build_setup_error(
            'plasm-screen-area-error',
            get_string('setup_screen_area_error_title', 'quizaccess_quilgo'),
            get_string('setup_screen_area_error_desc', 'quizaccess_quilgo'),
            'plasm-setup-screen-retry'
        );

        $mform->addElement('html', $this->build_loading_screen_setup());
        $mform->addElement('html', $this->build_consent());
        $mform->addElement('html', $this->build_setup());
        $mform->addElement('html', $cameraaccesserrorscreen);
        $mform->addElement('html', $screenaccesserrorscreen);
        $mform->addElement('html', $screenareaerrorscreen);
        $mform->addElement('html', $this->build_not_supported_error());
        $mform->addElement('html', $this->build_finish_step());
        $mform->addElement('html', $this->build_plasm_not_available_error());
        $this->build_consent_checkbox($mform, $quizdata);
        $mform->addElement('html', $this->build_warning_disable_device($quizdata));

        $moodleversion = $CFG->release;
        $pluginversion = quizaccess_quilgo_get_plugin_version();

        $PAGE->requires->js_call_amd(
            'quizaccess_quilgo/plasm-service',
            'init',
            [
                $plasmconfig,
                $quizdata,
                $datenow->format(DATE_ISO8601),
                $attemptstarttime,
                $this->quiz->cmid,
                $this->quizobj->is_preview_user(),
                $sentryoptions,
                $moodleversion,
                $pluginversion,
            ]
        );
    }

    /**
     * Delete settings once quiz deleted
     * @param mixed $quiz
     * @return void
     */
    public static function delete_settings($quiz) {
        global $DB;
        $DB->delete_records('quizaccess_quilgo_settings', ['quizid' => $quiz->id]);
    }

    /**
     * Add custom column to quiz detail
     * @param int $quizid
     * @return array
     */
    public static function get_settings_sql($quizid) {
        return [
            '
                camera_enabled AS plasm_camera, screen_enabled AS plasm_screen,
                force_enabled AS plasm_force, proctoring_enabled AS plasm_enabled',
            'LEFT JOIN {quizaccess_quilgo_settings} plasm_setting ON plasm_setting.quizid = quiz.id',
            [],
        ];
    }

    /**
     * Show custom description. Show the report link here
     * @return mixed
     */
    public function description() {
        global $OUTPUT, $USER, $DB;

        $context = context_module::instance($this->quiz->cmid, MUST_EXIST);
        if (!has_capability('quizaccess/quilgo:viewreport', $context)) {
            return "";
        }

        $messages = [];
        $proctoringsetting = $DB->get_record('quizaccess_quilgo_settings', ['quizid' => $this->quiz->id]);
        if (empty($proctoringsetting) || $proctoringsetting->proctoring_enabled == 0) {
            $quizsettingsurl = new moodle_url('/course/modedit.php', ['update' => $this->quiz->cmid, 'return' => 1]);
            $proctoringdisabledinfoicon = html_writer::tag('i', '', ['class' => 'fa fa-info-circle']);
            $proctoringdisabledinfotext = get_string('proctoring_disabled_info', 'quizaccess_quilgo', $quizsettingsurl->out());
            $messages[] = html_writer::tag('div',
                "<span>$proctoringdisabledinfoicon</span> <span>$proctoringdisabledinfotext</span>",
                ['class' => 'alert alert-info text-left']
            );
        }

        $reporttext = get_string('report_link_caption', 'quizaccess_quilgo');
        $reporticon = html_writer::tag('i', '', ['class' => 'fa fa-bar-chart']);
        $reporturl = quizaccess_quilgo_get_report_url($this->quiz->course, $this->quiz->cmid, $this->quiz->id);
        $messages[] = $OUTPUT->action_link(
            $reporturl,
            "$reporticon $reporttext",
            null,
            ['class' => 'btn btn-outline-primary']
        );

        return $messages;
    }

    /**
     * Sets up the attempt (review or summary) page with any special extra
     * properties required by this rule. securewindow rule is an example of where
     * this is used.
     *
     * @param moodle_page $page the page object to initialise.
     */
    public function setup_attempt_page($page) {
        $attemptid = $page->url->get_param('attempt');

        if (empty($attemptid)) {
            return;
        }

        $attemptsession = quizaccess_quilgo_get_attempt_session($attemptid);
        $plasmsessionid = null;
        if (empty($attemptsession)) {
            global $SESSION, $DB;

            if (!empty($SESSION->quilgosessiontracking[$this->quiz->id])) {
                $plasmsessionid = $SESSION->quilgosessiontracking[$this->quiz->id];
                unset($SESSION->quilgosessiontracking[$this->quiz->id]);
            }

            $sessionreportparams = [
                'attemptid' => $attemptid,
                'plasmsessionid' => $plasmsessionid,
            ];

            $currentsetting = $DB->get_record('quizaccess_quilgo_settings', ['quizid' => $this->quiz->id]);
            if (!empty($currentsetting)) {
                $sessionreportparams['proctoring_enabled'] = $currentsetting->proctoring_enabled;
                $sessionreportparams['camera_enabled'] = $currentsetting->camera_enabled;
                $sessionreportparams['screen_enabled'] = $currentsetting->screen_enabled;
                $sessionreportparams['force_enabled'] = $currentsetting->force_enabled;
            }

            $DB->insert_record('quizaccess_quilgo_reports', $sessionreportparams);
        } else {
            if (!empty($attemptsession->plasmsessionid)) {
                $plasmsessionid = $attemptsession->plasmsessionid;
            }
        }

        if (!empty($plasmsessionid)) {
            $page->add_body_class("quiz-quilgo-tracking-sessionid-$plasmsessionid");
        }
    }

    /**
     * Build plasm not available error screen
     * @return string
     */
    protected function build_plasm_not_available_error() {
        $quizviewurl = new moodle_url('/mod/quiz/view.php', ['id' => $this->quiz->cmid]);
        $contents = html_writer::start_div('d-flex flex-column align-items-center quilgo-hidden plasm-not-available');
        $contents .= html_writer::tag(
            'h5',
            get_string('proctoring_setup_failed_title', 'quizaccess_quilgo'),
        );
        $contents .= html_writer::tag(
            'p',
            get_string('create_session_error_default_description', 'quizaccess_quilgo'),
            ['class' => 'text-center']
        );
        $contents .= html_writer::tag(
            'a',
            get_string('refresh_quiz_page', 'quizaccess_quilgo'),
            [
                'class' => 'btn btn-primary mt-2 mb-1',
                'role' => 'button',
                'href' => $quizviewurl->out(),
            ],
        );
        $contents .= html_writer::end_div();

        return $contents;
    }
}
