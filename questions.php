<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Questions list with pattern page
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../../../config.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');

$cmid = required_param('cmid', PARAM_INT);
$attemptid = required_param('attemptid', PARAM_INT);
$patternsquery = required_param('patterns', PARAM_RAW);

$context = context_module::instance($cmid, MUST_EXIST);
$attemptobj = quiz_create_attempt_handling_errors($attemptid, $cmid);
$attemptobj->preload_all_attempt_step_users();

require_login($attemptobj->get_course(), false, $attemptobj->get_cm());

$patternperquestion = [];
if (strlen($patternsquery) > 1) {
    $patterns = explode("|", $patternsquery);
    foreach ($patterns as $pattern) {
        $p = new stdClass;
        $explodedpattern = explode("#", $pattern);
        $p->questionId = $explodedpattern[0];
        $p->type = $explodedpattern[1];
        $patternperquestion[] = $p;
    }
}

$renderer = $PAGE->get_renderer('mod_quiz');
$slots = $attemptobj->get_slots();

/**
 * Build HTML strings for question pattern steps
 * @param array $steps question pattern steps
 * @return string
 */
function quizaccess_quilgo_build_question_pattern_steps($steps) {
    $stepicon = html_writer::tag('i', '', ['class' => 'fa fa-arrow-right mr-1']);

    $stepcontents = "";
    foreach ($steps as $key => $step) {
        $badge = $key > 1 ? 'badge-secondary' : 'badge-danger';
        $stepcontents .= html_writer::tag(
            'span',
            $step,
            ['class' => "badge $badge mr-1"],
        );

        if ($key < count($steps) - 1) {
            $stepcontents .= $stepicon;
        }
    }

    return $stepcontents;
}

$questioncontents = "";
foreach ($slots as $slot) {
    $questionattempt = $attemptobj->get_question_attempt($slot);
    $outeruniqueid = $questionattempt->get_outer_question_div_unique_id();
    $questioncontents .= $attemptobj->render_question($slot, false, $renderer);

    $patternquestioncontents = "";
    $patternperquestionindex = array_search($outeruniqueid, array_column($patternperquestion, 'questionId'));
    if ($patternperquestionindex !== false) {
        $pattern = $patternperquestion[$patternperquestionindex];
        $patternquestioncontents .= html_writer::tag(
            'div',
            get_string('report_patterns_detected', 'quizaccess_quilgo'),
            ['class' => 'text-danger font-weight-bold']
        );

        $stepscontents = "";
        $steps = [];
        if ($pattern->type == QUIZACCESS_QUILGO_PATTERN_TYPE_COPY_PASTE) {
            $steps = [
                get_string('report_patterns_step_copy_question', 'quizaccess_quilgo'),
                get_string('report_patterns_step_leave_test', 'quizaccess_quilgo'),
                get_string('report_patterns_step_return', 'quizaccess_quilgo'),
                get_string('report_patterns_step_paste_answer', 'quizaccess_quilgo'),
            ];
        } else if ($pattern->type == QUIZACCESS_QUILGO_PATTERN_TYPE_COPY_CHANGE) {
            $steps = [
                get_string('report_patterns_step_copy_question', 'quizaccess_quilgo'),
                get_string('report_patterns_step_leave_test', 'quizaccess_quilgo'),
                get_string('report_patterns_step_return', 'quizaccess_quilgo'),
                get_string('report_patterns_step_change_answer', 'quizaccess_quilgo'),
            ];
        }

        $stepscontents .= quizaccess_quilgo_build_question_pattern_steps($steps);
        $stepscontents = html_writer::tag(
            'div',
            $stepscontents,
            ['class' => 'd-flex align-items-center flex-wrap']
        );

        $patternquestioncontents = html_writer::tag(
            'div',
            $patternquestioncontents . $stepscontents,
            ['class' => 'content']
        );

        $patternquestioncontents = html_writer::tag(
            'div',
            $patternquestioncontents,
            ['class' => 'que mb-5 pb-5']
        );
    }

    $questioncontents .= $patternquestioncontents;
}

$questionurl = quizaccess_quilgo_get_questions_url($cmid, $attemptid, $patternsquery);
$PAGE->set_url($questionurl);
$PAGE->set_pagelayout('embedded');
$PAGE->requires->css('/mod/quiz/accessrule/quilgo/styles.css');

echo $OUTPUT->header();
echo $questioncontents;
echo $OUTPUT->footer();
