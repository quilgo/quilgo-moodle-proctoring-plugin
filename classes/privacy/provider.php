<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace quizaccess_quilgo\privacy;

use core_privacy\local\metadata\collection;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\userlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\writer;

/**
 * Privacy for the quizaccess_quilgo plugin.
 *
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
    \core_privacy\local\metadata\provider,
    \core_privacy\local\request\core_userlist_provider,
    \core_privacy\local\request\data_provider,
    \core_privacy\local\request\plugin\provider {

    /**
     * Returns information about the user data stored in this component.
     *
     * @param  collection $collection A list of information about this component
     * @return collection The collection object filled out with information about this component.
     */
    public static function get_metadata(collection $collection): collection {
        $collection->add_database_table(
            'quizaccess_quilgo_settings',
            [
                'quizid' => 'privacy:metadata:quizaccess_quilgo_settings:quizid',
                'camera_enabled' => 'privacy:metadata:quizaccess_quilgo_settings:camera_enabled',
                'screen_enabled' => 'privacy:metadata:quizaccess_quilgo_settings:screen_enabled',
                'force_enabled' => 'privacy:metadata:quizaccess_quilgo_settings:force_enabled',
            ],
            'privacy:metadata:quizaccess_quilgo_settings'
        );

        $collection->add_database_table(
            'quizaccess_quilgo_reports',
            [
                'attemptid' => 'privacy:metadata:quizaccess_quilgo_reports:attemptid',
                'plasmsessionid' => 'privacy:metadata:quizaccess_quilgo_reports:plasmsessionid',
                'camera_enabled' => 'privacy:metadata:quizaccess_quilgo_reports:camera_enabled',
                'screen_enabled' => 'privacy:metadata:quizaccess_quilgo_reports:screen_enabled',
                'force_enabled' => 'privacy:metadata:quizaccess_quilgo_reports:force_enabled',
                'error_reason' => 'privacy:metadata:quizaccess_quilgo_reports:error_reason',
            ],
            'privacy:metadata:quizaccess_quilgo_reports'
        );

        $collection->add_external_location_link(
            'quilgo.com',
            [
                'images' => 'privacy:metadata:quizaccess_quilgo_proctoring:images',
                'focuses' => 'privacy:metadata:quizaccess_quilgo_proctoring:focuses',
            ],
            'privacy:metadata:quizaccess_quilgo_proctoring'
        );

        return $collection;
    }

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param   int $userid The user to search.
     * @return  contextlist   $contextlist  The contextlist containing the list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid): contextlist {
        $contextlist = new contextlist();

        $sql = "SELECT DISTINCT c.id
            FROM {quizaccess_quilgo_reports} r
            INNER JOIN {quiz_attempts} a ON a.id = r.attemptid
            INNER JOIN {quiz} q ON q.id = a.quiz
            INNER JOIN {course_modules} cm ON cm.instance = q.id
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {context} c ON c.instanceid = cm.id AND contextlevel = :contextlevel
            WHERE a.userid = :userid
        ";

        $params = [
            'modname' => 'quiz',
            'contextlevel' => CONTEXT_MODULE,
            'userid' => $userid,
        ];

        $contextlist->add_from_sql($sql, $params);

        return $contextlist;
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param   approved_contextlist $contextlist The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        $userid = $contextlist->get_user()->id;
        foreach ($contextlist->get_contexts() as $context) {
            if ($context->contextlevel === CONTEXT_MODULE && $context->instanceid) {
                $cm = $DB->get_record('course_modules', ['id' => $context->instanceid]);
                $quizid = $cm->instance;

                $reportsql = "SELECT r.id AS reportid,
                    a.userid AS userid,
                    a.quiz AS quizid,
                    a.attempt AS attempt,
                    r.camera_enabled AS camera_enabled,
                    r.screen_enabled AS screen_enabled,
                    r.force_enabled AS force_enabled
                    FROM {quizaccess_quilgo_reports} r
                    INNER JOIN {quiz_attempts} a ON a.id = r.attemptid
                    INNER JOIN {quiz} q ON q.id = a.quiz
                    INNER JOIN {course_modules} cm ON cm.instance = q.id
                    INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                    INNER JOIN {context} c ON c.instanceid = cm.id AND contextlevel = :contextlevel
                    WHERE a.userid = :userid AND a.quiz = :quizid
                    ORDER BY a.quiz, a.attempt ASC
                ";

                $reportparams = [
                    'userid' => $userid,
                    'quizid' => $quizid,
                    'modname' => 'quiz',
                    'contextlevel' => CONTEXT_MODULE,
                ];
                $reports = $DB->get_records_sql($reportsql, $reportparams);

                if (!empty($reports)) {
                    $mappedreports = (object) array_map(function($report) {
                        return [
                            'userid' => $report->userid,
                            'quizid' => $report->quizid,
                            'attempt' => $report->attempt,
                            'camera_enabled' => $report->camera_enabled == 1
                                ? get_string('privacy:export:quizaccess_quilgo_settings:camera_enabled', 'quizaccess_quilgo')
                                : get_string('privacy:export:quizaccess_quilgo_settings:camera_disabled', 'quizaccess_quilgo'),
                            'screen_enabled' => $report->screen_enabled == 1
                                ? get_string('privacy:export:quizaccess_quilgo_settings:screen_enabled', 'quizaccess_quilgo')
                                : get_string('privacy:export:quizaccess_quilgo_settings:screen_disabled', 'quizaccess_quilgo'),
                            'force_enabled' => $report->force_enabled == 1
                                ? get_string('privacy:export:quizaccess_quilgo_settings:force_enabled', 'quizaccess_quilgo')
                                : get_string('privacy:export:quizaccess_quilgo_settings:force_disabled', 'quizaccess_quilgo'),
                        ];
                    }, $reports);

                    writer::with_context($context)->export_data(
                        [get_string('privacy:export:quizaccess_quilgo_reports', 'quizaccess_quilgo')],
                        $mappedreports
                    );
                }

                $settingssql = "SELECT * FROM {quizaccess_quilgo_settings} WHERE quizid = :quizid";
                $settingparams = ['quizid' => $quizid];
                $setting = $DB->get_record_sql($settingssql, $settingparams);

                if (!empty($setting)) {
                    $settingdata = (object)[
                        'quizid' => $setting->quizid,
                        'camera_enabled' => $setting->camera_enabled == 1
                            ? get_string('privacy:export:quizaccess_quilgo_settings:camera_enabled', 'quizaccess_quilgo')
                            : get_string('privacy:export:quizaccess_quilgo_settings:camera_disabled', 'quizaccess_quilgo'),
                        'screen_enabled' => $setting->screen_enabled == 1
                            ? get_string('privacy:export:quizaccess_quilgo_settings:screen_enabled', 'quizaccess_quilgo')
                            : get_string('privacy:export:quizaccess_quilgo_settings:screen_disabled', 'quizaccess_quilgo'),
                        'force_enabled' => $setting->force_enabled == 1
                            ? get_string('privacy:export:quizaccess_quilgo_settings:force_enabled', 'quizaccess_quilgo')
                            : get_string('privacy:export:quizaccess_quilgo_settings:force_disabled', 'quizaccess_quilgo'),
                    ];
                    writer::with_context($context)->export_data(
                        [get_string('privacy:export:quizaccess_quilgo_settings', 'quizaccess_quilgo')],
                        $settingdata,
                    );
                }
            }
        }
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param context $context The specific context to delete data for.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;

        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }

        $cm = $DB->get_record('course_modules', ['id' => $context->instanceid]);
        $quizid = $cm->instance;
        $attemptsparams = ['quiz' => $quizid];
        $attempts = $DB->get_records_sql("SELECT id FROM {quiz_attempts} WHERE quiz = :quiz", $attemptsparams);
        $attemptsids = [];
        foreach ($attempts as $attempt) {
            $attemptsids[] = $attempt->id;
        }

        if (count($attemptsids) > 0) {
            list($attempinsql, $attempinparams) = $DB->get_in_or_equal($attemptsids, SQL_PARAMS_NAMED);
            $deletesql = "attemptid {$attempinsql}";
            $DB->delete_records_select("quizaccess_quilgo_reports", $deletesql, $attempinparams);
        }
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts and user information to delete information for.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;

        if (empty($contextlist->count())) {
            return;
        }

        $userid = $contextlist->get_user()->id;

        foreach ($contextlist->get_contexts() as $context) {
            $cm = $DB->get_record('course_modules', ['id' => $context->instanceid]);
            $quizid = $cm->instance;

            $attemptsparams = ['quiz' => $quizid, 'userid' => $userid];
            // Using left join here because quiz_attempts related with user already deleted
            // before this method executed.
            $attemptssql = "SELECT r.id AS reportid, a.id AS attemptid
                FROM {quizaccess_quilgo_reports} r
                LEFT JOIN {quiz_attempts} a ON a.id = r.attemptid
                WHERE a.id IS NULL OR (a.userid = :userid AND a.quiz = :quiz)
            ";
            $reports = $DB->get_records_sql($attemptssql, $attemptsparams);
            $reportids = [];
            foreach ($reports as $report) {
                $reportids[] = $report->reportid;
            }

            if (count($reportids) > 0) {
                list($reportinsql, $reportinparams) = $DB->get_in_or_equal($reportids, SQL_PARAMS_NAMED);
                $deletesql = "id {$reportinsql}";
                $DB->delete_records_select("quizaccess_quilgo_reports", $deletesql, $reportinparams);
            }
        }
    }

    /**
     * Get the list of users who have data within a context.
     *
     * @param userlist $userlist The userlist containing the list of users who have data in this context/plugin combination.
     */
    public static function get_users_in_context(userlist $userlist) {
        $context = $userlist->get_context();
        if (!$context instanceof \context_module) {
            return;
        }

        $sql = "SELECT DISTINCT a.userid
            FROM {quizaccess_quilgo_reports} r
            INNER JOIN {quiz_attempts} a ON a.id = r.attemptid
            INNER JOIN {quiz} q ON q.id = a.quiz
            INNER JOIN {course_modules} cm ON cm.instance = q.id
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {context} c ON c.instanceid = cm.id AND contextlevel = :contextlevel
            WHERE cm.id = :instanceid
        ";

        $params = [
            'modname' => 'quiz',
            'contextlevel' => CONTEXT_MODULE,
            'instanceid' => $context->instanceid,
        ];

        $userlist->add_from_sql('userid', $sql, $params);

        return $userlist;
    }

    /**
     * Delete multiple users within a single context.
     *
     * @param approved_userlist $userlist The approved context and user information to delete information for.
     */
    public static function delete_data_for_users(approved_userlist $userlist) {
        global $DB;

        $context = $userlist->get_context();
        $cm = $DB->get_record('course_modules', ['id' => $context->instanceid]);
        $quizid = $cm->instance;

        list($userinsql, $userinparams) = $DB->get_in_or_equal($userlist->get_userids(), SQL_PARAMS_NAMED);
        $attemptsparams = array_merge(['quiz' => $quizid], $userinparams);
        $attemptssql = "SELECT r.id AS reportid, a.id AS attemptid
            FROM {quizaccess_quilgo_reports} r
            LEFT JOIN {quiz_attempts} a ON a.id = r.attemptid
            WHERE a.id IS NULL OR (a.userid {$userinsql} AND a.quiz = :quiz)
        ";
        $reports = $DB->get_records_sql($attemptssql, $attemptsparams);
        $reportids = [];
        foreach ($reports as $report) {
            $reportids[] = $report->reportid;
        }

        if (count($reportids) > 0) {
            list($reportinsql, $reportinparams) = $DB->get_in_or_equal($reportids, SQL_PARAMS_NAMED);
            $deletesql = "id {$reportinsql}";
            $DB->delete_records_select("quizaccess_quilgo_reports", $deletesql, $reportinparams);
        }
    }
}
