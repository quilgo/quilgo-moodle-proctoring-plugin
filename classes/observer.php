<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace quizaccess_quilgo;

use mod_quiz\event\attempt_submitted;
use quizaccess_quilgo\plasmapi;

/**
 * Observe class for listen some events
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class observer {

    /**
     * Handle generate stat report when attempt submission
     * @param attempt_submitted $event
     *
     * @return void
     */
    public static function attempt_submitted(attempt_submitted $event) {
        global $DB;

        $data = $event->get_data();
        $attemptid = $data['objectid'];

        $currentreport = $DB->get_record('quizaccess_quilgo_reports', ['attemptid' => $attemptid]);

        // No need generate report because there is possibiilty plasmsessionid empty.
        // when attempt started with proctoring disabled.
        if (!empty($currentreport) && !empty($currentreport->plasmsessionid)) {
            $plasmapi = new plasmapi();
            $generateresponse = $plasmapi->genereate_report($currentreport->plasmsessionid);

            if ($generateresponse['is_success'] === false) {
                $currentreportupdated = new \stdClass();
                $currentreportupdated->id = $currentreport->id;
                $currentreportupdated->error_reason = json_encode($generateresponse['error_reason']);

                $DB->update_record('quizaccess_quilgo_reports', $currentreportupdated);
            }
        }
    }
}
