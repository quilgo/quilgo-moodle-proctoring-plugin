<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace quizaccess_quilgo;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/filelib.php');

use curl;

/**
 * HTTP helper for call external service
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class http {
    /**
     * @var mixed curl instance
     */
    protected $ch;

    /**
     * @var array used headers
     */
    protected $headers = [];

    /**
     * @var array used payload
     */
    protected $payload = [];

    /**
     * @var string used url
     */
    protected $url;

    /**
     * Constructor class
     * @param string $url
     * @param array $payload
     * @return void
     */
    public function __construct($url = null, $payload = []) {
        $this->ch = new curl();
        $this->url = $url;
        $this->payload = $payload;

        $this->ch->setopt([
            'CURLOPT_RETURNTRANSFER' => 1,
            'CURLOPT_HEADER' => 1,
        ]);
    }

    /**
     * Init class
     * @param string $url
     * @param array $payload
     * @return void
     */
    public function factory($url = null, $payload = []) {
        return new self($url, $payload);
    }

    /**
     * Init class
     * @param string $str
     * @return string
     */
    protected static function fix_string_case($str) {
        $str = explode('-', $str);
        foreach ($str as &$word) {
            $word = ucfirst($word);
        }
        return implode('-', $str);
    }

    /**
     * Set headers
     * @param mixed $data
     * @return $this
     */
    public function set_headers($data) {
        foreach ($data as $key => $val) {
            $this->headers[self::fix_string_case($key)] = $val;
        }
        return $this;
    }

    /**
     * Set response after request call
     * @param mixed $rawresponse
     *
     * @return mixed
     */
    protected function response($rawresponse) {
        $info = $this->ch->get_info();
        $status = $info['http_code'];
        $headersize = $info['header_size'];
        $error = $this->ch->errno;
        $errno = $this->ch->errno;

        if ($rawresponse === false) {
            throw new \RuntimeException($error, $errno);
        }

        $content = substr($rawresponse, $headersize);

        $response = new \stdClass();
        $response->status = $status;
        $response->body = json_decode($content);

        return $response;
    }

    /**
     * Handle post request
     * @return mixed
     */
    public function post() {
        $this->payload = json_encode($this->payload);
        $this->headers = array_merge($this->headers, ['Content-Length: ' . strlen($this->payload)]);

        $this->ch->setHeader($this->headers);
        $rawresponse = $this->ch->post($this->url, $this->payload);

        return $this->response($rawresponse);
    }

    /**
     * Handle get request
     * @return mixed
     */
    public function get() {
        $this->ch->setHeader($this->headers);
        $rawresponse = $this->ch->get($this->url);

        return $this->response($rawresponse);
    }
}
