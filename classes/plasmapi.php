<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace quizaccess_quilgo;

use quizaccess_quilgo\http;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');

/**
 * Helper for call some Plasm endpoints
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plasmapi {

    /**
     * @var mixed Http instance
     */
    protected $http;

    /**
     * @var string API url
     */
    protected $apiurl;

    /**
     * @var array Used headers
     */
    protected $headers;

    /**
     * Constructor class
     * @return void
     */
    public function __construct() {
        $baseurl = QUIZACCESS_QUILGO_PLASM_URL . '/' . QUIZACCESS_QUILGO_API_PREFIX;
        $this->http = new http();
        $this->apiurl = $baseurl . '/~';
        $this->headers = [
            'Content-type: application/json',
            'Authorization: Bearer ' . get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_API_TOKEN_NAME),
        ];
    }

    /**
     * Handle site registration
     * @return mixed
     */
    public function register() {
        global $CFG, $SITE, $DB;

        $url = $this->apiurl . '/applications';

        $admin = get_admin();
        $metadata = [
            'moodle_url' => $CFG->wwwroot,
            'moodle_site_name' => $SITE->fullname,
            'moodle_version' => $CFG->release,
            'moodle_admin_name' => fullname($admin, true),
            'moodle_admin_email' => $admin->email,
            'moodle_courses_amount' => $DB->count_records('course') - 1,
            'moodle_users_amount' => $DB->count_records('user', ['deleted' => 0]),
            'moodle_dbtype' => $CFG->dbtype,
            'clientVersion' => $CFG->release,
            'integrationVersion' => quizaccess_quilgo_get_plugin_version(),
        ];

        $payload = [
            'name' => $SITE->fullname,
            'webhookStatUrl' => $CFG->wwwroot,
            'metadata' => json_encode($metadata),
            'email' => $admin->email,
        ];

        $response = $this->http->factory($url, $payload)
            ->set_headers(['Content-type: application/json'])
            ->post();

        return $response->status == 201 ? $response->body : null;
    }

    /**
     * Handle generate report
     * @param string $plasmsessionid Plasm sessionid that will be generate
     * @return array
     */
    public function genereate_report($plasmsessionid) {
        $url = $this->apiurl . '/sessions/generate-report';
        $payload = ['sessionUuid' => $plasmsessionid];

        $response = $this->http->factory($url, $payload)
            ->set_headers($this->headers)
            ->post();

        if ($response->status >= 200 && $response->status < 300) {
            return [
                'is_success' => true,
                'error_reason' => null,
            ];
        }

        quizaccess_quilgo_show_error_log(json_encode($response->body));
        return [
            'is_success' => false,
            'error_reason' => $response->body,
        ];
    }

    /**
     * Handle fetch report
     * @param string $plasmsessionid Plasm sessionid report target
     * @return mixed
     */
    public function fetch_report($plasmsessionid) {
        $url = $this->apiurl . '/reports/' . $plasmsessionid;

        $response = $this->http->factory($url)
            ->set_headers($this->headers)
            ->get();

        if (in_array($response->status, [200, 404])) {
            return $response->body;
        }

        return null;
    }

    /**
     * Handle fetch application data
     * @return mixed
     */
    public function fetch_application_data() {
        $url = $this->apiurl . '/billing/applications/data';

        $response = $this->http->factory($url)
            ->set_headers($this->headers)
            ->get();

        if ($response->status !== 200) {
            return null;
        }

        return $response->body;
    }

    /**
     * Handle generate dashboard token
     * @return mixed
     */
    public function generate_dashboard_token() {
        $url = $this->apiurl . '/billing/applications/dashboard-token';

        $response = $this->http->factory($url)
            ->set_headers($this->headers)
            ->post();

        if ($response->status !== 201) {
            return null;
        }

        return $response->body;
    }

    /**
     * Handle create session via server
     * @param int $expiresin Plasm session expiresin
     *
     * @return object
     */
    public function create_session($expiresin) {
        $url = $this->apiurl . '/sessions';

        $payload = ['expiresIn' => $expiresin];
        $response = $this->http->factory($url, $payload)
            ->set_headers($this->headers)
            ->post();

        $createsessionresponse = new \stdClass();
        if ($response->status == 201) {
            $createsessionresponse->uuid = $response->body->uuid;
            return $createsessionresponse;
        }

        $bodyerror = $response->body->error;
        if (
            isset($bodyerror->details) &&
            isset($bodyerror->details->reason) &&
            $bodyerror->details->reason == 'SESSION_LIMIT_REACHED'
        ) {
            $createsessionresponse->errorcode = QUIZACCESS_QUILGO_CREATE_SESSION_ERROR_LIMIT_REACHED;
        } else {
            $createsessionresponse->errorcode = QUIZACCESS_QUILGO_CREATE_SESSION_ERROR_DEFAULT;
        }

        return $createsessionresponse;
    }

    /**
     * Handle set application metadata
     * @param string $confirmedemail app confirmed email
     *
     * @return bool
     */
    public function set_application_metadata($confirmedemail) {
        $url = $this->apiurl . '/applications/set-metadata';

        $payload = ['confirmedEmail' => $confirmedemail];
        $response = $this->http->factory($url, $payload)
            ->set_headers($this->headers)
            ->post();

        return $response->status == 200;
    }
}
