<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace quizaccess_quilgo\form;

use html_writer;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');

/**
 * API configuration form
 *
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class api_config extends \moodleform {

    /**
     * Define information the API for the plugin configured or not
     * Action for register new site is handled inside config page
     * @return void
     */
    public function definition() {
        global $PAGE;

        $mform = $this->_form;
        $apitoken = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_API_TOKEN_NAME);
        $clienttoken = get_config('quizaccess_quilgo', QUIZACCESS_QUILGO_CONFIG_CLIENT_TOKEN_NAME);

        if (empty($apitoken) || empty($clienttoken)) {
            $warning = html_writer::tag(
                'div',
                get_string('config_warning_not_active_yet', 'quizaccess_quilgo'),
                ['class' => 'alert alert-warning']
            );

            $mform->addElement('html', $warning);
            $mform->addElement('submit', 'register', get_string('config_register', 'quizaccess_quilgo'));

            return;
        }

        // Upgrade failed info.
        $upgradefailedinfo = html_writer::tag(
            'div',
            get_string('upgrade_failed_info', 'quizaccess_quilgo'),
            ['class' => 'alert alert-warning w-50 d-none upgrade-failed-info']
        );
        $mform->addElement('html', $upgradefailedinfo);

        // Free limitation info.
        $limitinfoicon = html_writer::tag('i', '', ['class' => 'fa fa-info-circle']);
        $limitinforunup = html_writer::span(get_string('limit_info_run_up', 'quizaccess_quilgo'));
        $loadingicon = html_writer::tag(
            'i',
            '',
            ['class' => 'fa fa-circle-notch fa-spin mr-1 d-none upgrade-cta-loading-icon']
        );
        $limitinfoaction = html_writer::div(get_string('limit_info_upgrade', 'quizaccess_quilgo', $loadingicon), 'mt-2');
        $freelimitationinfo = html_writer::tag(
            'div',
            "$limitinfoicon $limitinforunup $limitinfoaction",
            ['class' => 'alert alert-info w-50 d-none upgrade-info-container']
        );
        $mform->addElement('html', $freelimitationinfo);

        // Manage subscription info.
        $managesubscriptionfailedinfo = html_writer::tag(
            'div',
            get_string('manage_subscription_failed_info', 'quizaccess_quilgo'),
            ['class' => 'alert alert-warning w-50 d-none manage-subscription-failed-info']
        );
        $mform->addElement('html', $managesubscriptionfailedinfo);

        $managesubsriptionlinkicon = html_writer::tag('i', '', ['class' => 'fa fa-arrow-up-right-from-square mr-1']);
        $managesubscriptionmessage = html_writer::tag(
            'span',
            get_string('manage_subscription_title', 'quizaccess_quilgo'),
            ['class' => 'text-primary', 'style' => 'text-decoration: underline;']
        );
        $managesubsriptionloadingicon = html_writer::tag(
            'i',
            '',
            ['class' => 'fa fa-circle-notch fa-spin ml-1 d-none manage-subscription-loading-icon']
        );
        $managesubscription = html_writer::tag(
            'div',
            $managesubsriptionlinkicon . $managesubscriptionmessage . $managesubsriptionloadingicon,
            ['class' => 'manage-subscription-container', 'style' => 'cursor: pointer;']
        );
        $mform->addElement('html', $managesubscription);

        $PAGE->requires->js_call_amd('quizaccess_quilgo/application', 'initManageSubscription');
    }

}
