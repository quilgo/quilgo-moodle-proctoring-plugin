define(['jquery', 'core/ajax', 'core/str', 'quizaccess_quilgo/sentry'], ($, Ajax, Str, Sentry) => {
  const sizePerChunk = 5;
  const retryDelay = 5_000;
  let retryRows = [];
  let loadingCaption = "";
  let defaultFetchStatFailCaption = "";

  const chunk = (rows) => {
    const chunked = [];
    for (let i = 0; i < rows.length; i += sizePerChunk) {
      chunked.push(rows.slice(i, i + sizePerChunk));
    }

    return chunked;
  };

  const fetchStat = (row) => {
    const attemptid = row.getAttribute("data-attemptid");
    const cmid = row.getAttribute("data-cmid");
    const wsfunction = "quizaccess_quilgo_fetch_stat";
    const params = {
      attemptid: Number(attemptid),
      cmid: Number(cmid),
    };
    const request = {
      methodname: wsfunction,
      args: params,
    };

    if (Number(row.getAttribute('data-statstatus')) === -1) {
      row.textContent = loadingCaption;
    }

    return Ajax.call([request])[0];
  };

  const fetchStats = async (batches) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const batch of batches) {
      // eslint-disable-next-line no-await-in-loop
      await Promise.all(
        // eslint-disable-next-line no-loop-func, arrow-body-style
        batch.map((row) =>
          fetchStat(row)
            .then((res) => {
              row.textContent = res.confidenceLevel;
              row.classList.remove("text-muted");
              row.classList.add(res.textClass);

              if (res.reportStatus !== 1) {
                retryRows.push(row);
              }
            })
            .catch((err) => {
              Sentry.captureError(err);
              row.textContent = defaultFetchStatFailCaption;
              retryRows.push(row);
            }),
        ),
      );
    }

    if (retryRows.length > 0) {
      setTimeout(() => {
        const chunks = chunk(retryRows);
        retryRows = [];
        fetchStats(chunks);
      }, retryDelay);
    }
  };

  const initBehaviors = async () => {
    const stringsTemplate = await Str.get_strings([
      { key: 'report_table_row_stat_loading', component: 'quizaccess_quilgo' },
      { key: 'report_table_row_stat_not_ready', component: 'quizaccess_quilgo' },
    ]);
    [loadingCaption, defaultFetchStatFailCaption] = stringsTemplate;
    
    const rows = document.querySelectorAll('span#quilgo-report-stat:not([data-statstatus="1"])');
    const chunks = chunk(Array.from(rows));
    fetchStats(chunks);
  };

  return {
    init: async (_sentryOptions) => {
      Sentry.init(_sentryOptions);
      try {
        await initBehaviors();
      } catch (err) {
        Sentry.captureError(err);
      }
    },
  }
});
