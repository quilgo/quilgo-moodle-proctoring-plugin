define([
  "jquery",
  "core/ajax",
  "core/modal_factory",
  "core/modal",
  "core/templates",
  "core/str",
  "core/modal_events",
  "quizaccess_quilgo/sentry",
], ($, Ajax, ModalFactory, Modal, Templates, Str, ModalEvents, Sentry) => {
  let coreModal;
  let detailModal;
  let fetchReportTimeoutId = null;
  let loader;
  let contents;
  let errorContent;
  let notReadyContent;
  let reportExpiresInContent;
  let reportExpiresInCaption;
  let reportData;
  let reportExpired;
  let reportCover;
  let reportCameraEnabled;
  let reportScreenEnabled;
  let reportStat;
  let reportFacePresence;
  let reportPageUnfocused;
  let reportSuspiciousCaption;
  let reportImages;
  let reportSnapshots;
  let reportScreenshots;
  let reportSnapshotsTime;
  let reportSnapshotsContents;
  let reportScreenshotsTime;
  let reportScreenshotsContents;
  let previewModal;
  let previewTime;
  let previewFacesDetected;
  let previewPageUnfocused;
  let previewImage;
  let previewPrev;
  let previewNext;
  let reportPreviewInfo;
  let isPreviewTest = 0;
  let selectedPreview = null;
  let reportPatternsDetected;
  let reportPatternsDetectedValue;
  let reportUsedMultipleScreens;
  let reportUsedMultipleScreensValue;
  let reportQuestions;
  let reportQuestionsLoader;
  let reportSettingsRecommendation;
  let reportSettingsRecommendationContents;
  let reportQuestionsLoaded = false;

  const questionsIframeContentsHeight = () => {
    reportQuestionsLoader.addClass("quilgo-hidden");
    reportQuestions.css("height", reportQuestions.contents().height());
  };

  const initElements = () => {
    loader = $(".quilgo-report-loader");
    contents = $(".quilgo-report-contents");
    errorContent = $(".quilgo-report-error");
    notReadyContent = $(".quilgo-report-not-ready");
    reportExpired = $(".quilgo-report-expired");
    reportCover = $(".quilgo-report-cover");
    reportCameraEnabled = $(".quilgo-report-camera-enabled");
    reportScreenEnabled = $(".quilgo-report-screen-enabled");
    reportStat = $(".quilgo-report-stat");
    reportFacePresence = $(".quilgo-report-face-presence");
    reportPageUnfocused = $(".quilgo-report-page-unfocused");
    reportImages = $(".quilgo-report-images");
    reportSnapshots = $(".quilgo-report-snapshots");
    reportScreenshots = $(".quilgo-report-screenshots");
    reportSnapshotsTime = $(".quilgo-report-snapshots-time");
    reportSnapshotsContents = $(".quilgo-report-snapshots-contents");
    reportScreenshotsTime = $(".quilgo-report-screenshots-time");
    reportScreenshotsContents = $(".quilgo-report-screenshots-contents");
    reportExpiresInContent = $(".quilgo-report-expiresin");
    reportExpiresInCaption = $(".quilgo-report-expiresin-caption");
    reportSuspiciousCaption = $(".quilgo-report-suspicious-screenshots");
    reportPreviewInfo = $(".quilgo-preview-info");
    reportPatternsDetected = $(".quilgo-report-patterns-detected");
    reportPatternsDetectedValue = $(".quilgo-report-patterns-detected-value");
    reportUsedMultipleScreens = $(".quilgo-report-used-multiple-screens");
    reportUsedMultipleScreensValue = $(".quilgo-report-used-multiple-screens-value");
    reportQuestions = $(".quilgo-report-questions");
    reportQuestionsLoader = $(".quilgo-report-questions-loader");
    reportSettingsRecommendation = $(".quilgo-settings-recommendation");
    reportSettingsRecommendationContents = $(".quilgo-settings-recommendation-contents");

    reportQuestions.on("load", () => {
      questionsIframeContentsHeight();
      window.addEventListener("resize", questionsIframeContentsHeight);
    });
  };

  const setExpiresInInfo = async () => {
    if (
      reportData.expiresIn < 1 ||
      (reportData.snapshots.length === 0 && reportData.screenshots.length === 0)
    ) {
      reportExpiresInCaption.text("-");
      reportExpiresInContent.addClass("quilgo-hidden");
      reportImages.addClass("quilgo-hidden");

      return;
    }

    const expiresInfoCaption = await Str.get_string(
      "report_expires_in",
      "quizaccess_quilgo",
      reportData.expiresIn,
    );
    reportExpiresInCaption.text(expiresInfoCaption);
    reportExpiresInContent.removeClass("quilgo-hidden");
  };

  const hideCoverImage = () => {
    reportCover.attr("src", "");
    reportCover.addClass("quilgo-hidden");
  };

  const setCoverImage = () => {
    if (reportData.expiresIn < 1) {
      if (reportData.cameraEnabled === 1 || reportData.screenEnabled === 1) {
        reportExpired.removeClass("quilgo-hidden");
      }

      hideCoverImage();
      return;
    }

    if (reportData.snapshots == null) {
      hideCoverImage();
      return;
    }

    const coverImage =
      reportData.snapshots.filter((snapshot) => snapshot.facesAmount === 1)[0] ?? null;
    if (coverImage == null) {
      hideCoverImage();
      return;
    }

    reportCover.attr("src", coverImage.url);
    reportCover.removeClass("quilgo-hidden");
  };

  const setBaseInfo = async () => {
    if (reportData.cameraEnabled) {
      reportCameraEnabled.removeClass("quilgo-hidden");
    }

    if (reportData.screenEnabled) {
      reportScreenEnabled.removeClass("quilgo-hidden");
    }

    if (reportData.stat == null) {
      reportFacePresence.parent().addClass("quilgo-hidden");
      reportPageUnfocused.addClass("quilgo-hidden");
      reportStat.addClass("quilgo-hidden");
      return;
    }

    if (reportData.stat.facesPresence != null) {
      const facesPresencePct = reportData.stat.facesPresence * 100;

      reportFacePresence
        .removeClass("text-danger text-success")
        .addClass(facesPresencePct < 100 ? "text-danger" : "text-success")
        .text(`${facesPresencePct}%`);
      reportFacePresence.parent().removeClass("quilgo-hidden");
    }

    if (reportData.stat.pageUnfocusedCount != null) {
      let focusTextClass = "text-success";
      let focusCaption = await Str.get_string("report_focus_good", "quizaccess_quilgo");
      if (reportData.stat.pageUnfocusedCount > 0) {
        focusTextClass = "text-danger";
        if (reportData.stat.pageUnfocusedCount > 1) {
          focusCaption = await Str.get_string(
            "report_focus_not_good_multiple",
            "quizaccess_quilgo",
            reportData.stat.pageUnfocusedCount,
          );
        } else {
          focusCaption = await Str.get_string(
            "report_focus_not_good",
            "quizaccess_quilgo",
            reportData.stat.pageUnfocusedCount,
          );
        }
      }

      reportPageUnfocused
        .removeClass("quilgo-hidden text-danger text-success")
        .addClass(focusTextClass)
        .text(focusCaption);
    }

    if (reportData.patternsDetected > 0) {
      const seeAnswerCaption = await Str.get_string(
        "report_patterns_see_answer_below",
        "quizaccess_quilgo",
        reportData.stat.pageUnfocusedCount,
      );
      reportPatternsDetectedValue.html(
        `<strong>${reportData.patternsDetected}</strong> <span class="text-muted font-italic">(${seeAnswerCaption})</span>`,
      );
      reportPatternsDetected.removeClass("quilgo-hidden");
    }

    if (reportData.isUsedMultipleScreens) {
      const yesCaption = await Str.get_string(
        "general_yes",
        "quizaccess_quilgo",
        reportData.stat.pageUnfocusedCount,
      );
      reportUsedMultipleScreensValue.html(`<strong>${yesCaption}</strong>`);
      reportUsedMultipleScreens.removeClass("quilgo-hidden");
    }

    reportStat.removeClass("quilgo-hidden");

    if (reportData.screenshots != null) {
      const unfocusedPageCount = reportData.screenshots.filter(
        (screenshot) => screenshot.isPageUnfocused,
      ).length;
      if (unfocusedPageCount > 0) {
        const suspiciousContent = await Str.get_string(
          "report_suspicious_caption",
          "quizaccess_quilgo",
          unfocusedPageCount,
        );
        reportSuspiciousCaption.text(suspiciousContent);
        reportSuspiciousCaption.parent().removeClass("quilgo-hidden");
      }
    }

    if (isPreviewTest === 1) {
      reportPreviewInfo.removeClass("quilgo-hidden");
    }

    if (reportData.cameraEnabled === 0 || reportData.screenEnabled === 0) {
      const recommendationCaption = await Str.get_string(
        "report_settings_recommendation",
        "quizaccess_quilgo",
        reportData.quizSettingsUrl,
      );
      reportSettingsRecommendation.removeClass("quilgo-hidden");
      reportSettingsRecommendationContents.html(recommendationCaption);
    }
  };

  const initDetailModal = async () => {
    coreModal
      .create({
        title: Str.get_string("report_detail_title", "quizaccess_quilgo"),
        body: Templates.render("quizaccess_quilgo/report", { id: 1 }),
        show: false,
        large: true,
      })
      .then((modal) => {
        detailModal = modal;

        modal.getRoot().on(ModalEvents.shown, () => {
          // Override base modal dialog styles
          const modalDialog = $(".modal-dialog");
          modalDialog.removeClass("modal-lg modal-dialog-scrollable");
          modalDialog.addClass("modal-xl");

          initElements();
          loader.removeClass("quilgo-hidden");
        });

        modal.getRoot().on(ModalEvents.hidden, () => {
          loader.addClass("quilgo-hidden");
          contents.addClass("quilgo-hidden");
          errorContent.addClass("quilgo-hidden");
          notReadyContent.addClass("quilgo-hidden");
          reportExpired.addClass("quilgo-hidden");
          reportCover.attr("src", "");
          reportCameraEnabled.addClass("quilgo-hidden");
          reportScreenEnabled.addClass("quilgo-hidden");
          reportData = null;
          reportStat.addClass("quilgo-hidden");
          reportFacePresence.parent().addClass("quilgo-hidden");
          reportPageUnfocused.addClass("quilgo-hidden");
          reportImages.addClass("quilgo-hidden");
          reportSnapshots.addClass("quilgo-hidden");
          reportScreenshots.addClass("quilgo-hidden");
          reportSnapshotsTime.text("-");
          reportSnapshotsContents.html("");
          reportScreenshotsTime.text("-");
          reportScreenshotsContents.html("");
          reportExpiresInContent.addClass("quilgo-hidden");
          reportExpiresInCaption.text("-");
          reportSuspiciousCaption.parent().addClass("quilgo-hidden");
          reportSuspiciousCaption.text("-");
          reportPreviewInfo.addClass("quilgo-hidden");
          isPreviewTest = 0;
          reportPatternsDetected.addClass("quilgo-hidden");
          reportPatternsDetectedValue.html("-");
          reportUsedMultipleScreens.addClass("quilgo-hidden");
          reportUsedMultipleScreensValue.html("-");
          reportQuestions.addClass("quilgo-hidden");
          reportQuestions.attr("src", "");
          reportQuestions.css("height", 0);
          reportQuestionsLoaded = false;
          reportQuestionsLoader.addClass("quilgo-hidden");
          reportSettingsRecommendation.addClass("quilgo-hidden");
          reportSettingsRecommendationContents.html("");

          if (fetchReportTimeoutId != null) {
            clearTimeout(fetchReportTimeoutId);
            fetchReportTimeoutId = null;
          }
        });
      });
  };

  const getImageTime = (_time) => {
    const time = new Date(_time);
    const timeHours = time.getHours().toString().padStart(2, "0");
    const timeMinutes = time.getMinutes().toString().padStart(2, "0");

    return `${timeHours}:${timeMinutes}`;
  };

  const getImagesRangeTime = (images) => {
    const firstShot = [...images].sort(
      (a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
    )[0];
    const lastShot = [...images].sort(
      (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime(),
    )[0];

    const firstTime = getImageTime(firstShot.createdAt);
    const lastTime = getImageTime(lastShot.createdAt);
    const isSameTime = firstTime === lastTime;
    if (isSameTime) {
      return firstTime;
    }

    return `${firstTime} - ${lastTime}`;
  };

  const isCanPrevPreview = () => (selectedPreview != null ? selectedPreview.index > 0 : false);

  const isCanNextPreview = () => {
    if (selectedPreview == null) {
      return false;
    }

    const shots = reportData[selectedPreview.type];
    if (shots == null) {
      return false;
    }

    const totalImagesAsIndex = shots.length - 1;
    const currentImageIndex = selectedPreview.index;

    return totalImagesAsIndex !== currentImageIndex;
  };

  const selectShot = async (type, index) => {
    if (reportData[type][index] == null) {
      return;
    }

    const shot = reportData[type][index];

    const shotTime = new Date(shot.createdAt);
    const shotTimeHours = shotTime.getHours().toString().padStart(2, "0");
    const shotTimeMinutes = shotTime.getMinutes().toString().padStart(2, "0");
    const shotTimeSeconds = shotTime.getSeconds().toString().padStart(2, "0");

    selectedPreview = { type, index: Number(index) };
    previewTime.text(`${shotTimeHours}:${shotTimeMinutes}:${shotTimeSeconds}`);
    previewImage.attr("src", shot.url);
    if (shot.facesAmount != null) {
      const facesDetectedCaption = await Str.get_string(
        "report_preview_faces_detected",
        "quizaccess_quilgo",
        shot.facesAmount,
      );
      previewFacesDetected.text(facesDetectedCaption);
      previewFacesDetected.removeClass("quilgo-hidden");
    } else {
      previewFacesDetected.text("-");
      previewFacesDetected.addClass("quilgo-hidden");
    }

    if (shot.isPageUnfocused === true) {
      previewPageUnfocused.removeClass("quilgo-hidden");
    } else {
      previewPageUnfocused.addClass("quilgo-hidden");
    }

    if (isCanNextPreview()) {
      previewNext.removeClass("disabled");
    } else {
      previewNext.addClass("disabled");
    }

    if (isCanPrevPreview()) {
      previewPrev.removeClass("disabled");
    } else {
      previewPrev.addClass("disabled");
    }
  };

  const initPreviewImage = () => {
    const previewTrigger = $('img[id*="quilgo-image-trigger"]');

    previewTrigger.on("mouseover", (e) => {
      const triggerId = $(e.currentTarget).attr("id");
      $(`#${triggerId}-view`).removeClass("quilgo-hidden");
    });

    previewTrigger.on("mouseleave", (e) => {
      const triggerId = $(e.currentTarget).attr("id");
      $(`#${triggerId}-view`).addClass("quilgo-hidden");
    });

    previewTrigger.on("click", async (e) => {
      e.stopImmediatePropagation();
      const type = $(e.currentTarget).attr("data-type");
      const index = $(e.currentTarget).attr("data-index");

      await previewModal.show();
      selectShot(type, index);

      previewNext.on("click", (nextEvt) => {
        nextEvt.stopImmediatePropagation();
        if (isCanNextPreview() === false) {
          return;
        }

        selectShot(selectedPreview.type, selectedPreview.index + 1);
      });

      previewPrev.on("click", (prevEvt) => {
        prevEvt.stopImmediatePropagation();
        if (isCanPrevPreview() === false) {
          return;
        }

        selectShot(selectedPreview.type, selectedPreview.index - 1);
      });
    });
  };

  const getShotcontent = (imageShot, imageIndex, type) => {
    let borderIndicator = "";
    if (type === "snapshots") {
      borderIndicator =
        imageShot.facesAmount != null && imageShot.facesAmount === 1
          ? "border-transparent"
          : "border border-warning";
    } else {
      borderIndicator = imageShot.isPageUnfocused ? "border border-danger" : "border-transparent";
    }

    const shotTime = getImageTime(imageShot.createdAt);
    const shotPreviewTrigger = `quilgo-image-trigger-${imageShot.id}`;
    const shotPreview = `quilgo-image-trigger-${imageShot.id}-view`;

    const shotContents = `
      <div class="quilgo-report-frame h-auto">
        <a class="quilgo-report-frame-link d-block">
          <img class="quilgo-image-item loadable-img rounded ${borderIndicator}" id="${shotPreviewTrigger}" data-type="${type}" data-index="${imageIndex}" src="${imageShot.thumbnail}"></img>
          <img class="quilgo-report-preview-img quilgo-hidden" id="${shotPreview}" src="${imageShot.url}"></img>
        </a>
        <p class="text-center mb-0 mt-1 tiny text-capitalize">${shotTime}</p>
      </div>
    `;

    return shotContents;
  };

  const setSnapshots = () => {
    if (reportData.snapshots.length === 0 || reportData.expiresIn < 1) {
      reportSnapshots.addClass("quilgo-hidden");
      return;
    }

    let snapshotsContent = "";
    reportData.snapshots.forEach((snapshot, index) => {
      snapshotsContent += getShotcontent(snapshot, index, "snapshots");
    });

    reportSnapshotsContents.html(snapshotsContent);

    const snapshotsRangeTime = getImagesRangeTime(reportData.snapshots);
    reportSnapshotsTime.text(snapshotsRangeTime);
    initPreviewImage();
    reportSnapshots.removeClass("quilgo-hidden");
    reportImages.removeClass("quilgo-hidden");
  };

  const setScreenshots = () => {
    if (reportData.screenshots.length === 0 || reportData.expiresIn < 1) {
      reportScreenshots.addClass("quilgo-hidden");
      return;
    }

    let screenshotsContent = "";
    reportData.screenshots.forEach((screenshot, index) => {
      screenshotsContent += getShotcontent(screenshot, index, "screenshots");
    });

    reportScreenshotsContents.html(screenshotsContent);

    const screenshotsRangeTime = getImagesRangeTime(reportData.screenshots);
    reportScreenshotsTime.text(screenshotsRangeTime);
    initPreviewImage();
    reportScreenshots.removeClass("quilgo-hidden");
    reportImages.removeClass("quilgo-hidden");
  };

  const setQuestionsIframe = () => {
    if (reportData.questionsUrl.length > 0 && !reportQuestionsLoaded) {
      reportQuestionsLoader.removeClass("quilgo-hidden");
      reportQuestions.attr("src", reportData.questionsUrl);
      reportQuestions.removeClass("quilgo-hidden");
      reportQuestionsLoaded = true;
    }
  };

  const fetchReport = (attemptid, cmid) => {
    const wsfunction = "quizaccess_quilgo_fetch_report";
    const params = {
      attemptid: Number(attemptid),
      cmid: Number(cmid),
    };
    const request = {
      methodname: wsfunction,
      args: params,
    };

    Ajax.call([request])[0]
      .done((res) => {
        reportData = res;

        if (reportData.reportGeneratedCode === "0") {
          loader.addClass("quilgo-hidden");
          errorContent.removeClass("quilgo-hidden");

          return;
        }

        if (reportData.reportGeneratedCode === "2") {
          loader.addClass("quilgo-hidden");
          notReadyContent.removeClass("quilgo-hidden");

          return;
        }

        if (
          reportData.expiresIn >= 1 &&
          (reportData.snapshots.length > 0 || reportData.screenshots.length > 0)
        ) {
          if (reportData.imageExpirationSeconds != null && reportData.imageExpirationSeconds > 0) {
            const fetchSafeDelay = 5;
            const adjustedExpiration = reportData.imageExpirationSeconds - fetchSafeDelay;
            fetchReportTimeoutId = setTimeout(() => {
              fetchReport(attemptid, cmid);
            }, adjustedExpiration * 1000);
          }
        }

        setExpiresInInfo();
        setCoverImage();
        setBaseInfo();
        setSnapshots();
        setScreenshots();
        setQuestionsIframe();

        loader.addClass("quilgo-hidden");
        contents.removeClass("quilgo-hidden");
      })
      .fail(() => {
        loader.addClass("quilgo-hidden");
        errorContent.removeClass("quilgo-hidden");
      });
  };

  const showReport = (attemptid, cmid) => {
    detailModal.show();
    fetchReport(attemptid, cmid);
  };

  const initPreviewElements = async () => {
    previewTime = $(".quilgo-preview-time");
    previewFacesDetected = $(".quilgo-preview-faces-detected");
    previewPageUnfocused = $(".quilgo-preview-page-unfocused");
    previewImage = $(".quilgo-preview-image");
    previewPrev = $(".quilgo-preview-prev");
    previewNext = $(".quilgo-preview-next");
  };

  const initPreviewModal = async () => {
    coreModal
      .create({
        title: Str.get_string("report_preview_title", "quizaccess_quilgo"),
        body: Templates.render("quizaccess_quilgo/report-preview", { id: 2 }),
        show: false,
        large: true,
      })
      .then((modal) => {
        previewModal = modal;
        modal.getRoot().on(ModalEvents.shown, () => {
          const modalDialog = $(".modal-dialog");
          modalDialog.removeClass("modal-lg modal-dialog-scrollable");
          modalDialog.addClass("modal-lg");

          initPreviewElements();
        });

        modal.getRoot().on(ModalEvents.hidden, () => {
          previewTime.text("-");
          previewPageUnfocused.addClass("quilgo-hidden");
          previewFacesDetected.text("-");
          previewFacesDetected.addClass("quilgo-hidden");
          previewImage.attr("src", "");
          previewPrev.addClass("disabled");
          previewNext.addClass("disabled");
          selectedPreview = null;
        });
      });
  };

  const initBehaviors = async () => {
    /**
     * Starting moodle 4.3, 'ModalFactory.create' is depcrecated
     * As per other moodle script example starting moodle 4.3 they use 'Modal.create'
     */
    coreModal = ModalFactory;
    if (typeof Modal.create !== "undefined") {
      coreModal = Modal;
    }

    await initDetailModal();
    await initPreviewModal();

    $('a[id*="quilgo-report-detail"]').on("click", (e) => {
      e.preventDefault();
      if (detailModal == null) {
        return;
      }

      const attemptId = $(e.currentTarget).attr("data-attemptid");
      const cmId = $(e.currentTarget).attr("data-cmid");
      isPreviewTest = Number($(e.currentTarget).attr("data-preview"));
      showReport(attemptId, cmId);
    });
  };

  return {
    init: async (_sentryOptions) => {
      Sentry.init(_sentryOptions);
      try {
        await initBehaviors();
      } catch (err) {
        Sentry.captureError(err);
      }
    },
  };
});
