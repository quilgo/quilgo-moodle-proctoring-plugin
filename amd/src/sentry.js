define(['jquery', 'quizaccess_quilgo/utils'], ($, Utils) => {
  let isInitialized = false;
  let options = {
    pluginVersion: "",
    sentryDSN: "",
    env: "",
    moodleVersion: "",
  };
  let BASE_API_URL = "";
  let PUBLIC_KEY = "";
  let isApiRequestsSuppressed = false;

  /**
   * Parses Sentry DSN.
   * Extracted data used when interacting with Sentry API through HTTP.
   * Docs: https://develop.sentry.dev/sdk/overview/#parsing-the-dsn
   */
  const parseDsn = (dsn) => {
    const sentryDsnRegex =
      /^(?<protocol>\w+):\/\/(?<publicKey>\w+)(?::(?<secretKey>\w+))?@(?<host>[\w.-]+)(?<path>\/[\w/-]+)?\/(?<projectId>\w+)$/;
    const match = dsn.match(sentryDsnRegex);

    if (match == null) {
      throw new Error({ msg: "failed to parse sentry dsn" });
    }

    const { protocol, host, path, projectId, publicKey } = match.groups;
    const baseApiUrl = `${protocol}://${host}${path ?? ""}/api/${projectId}`;

    return { baseApiUrl, publicKey };
  };

  /**
   * Formats a JS error stacktrace into Sentry's Stack Trace Interface.
   * Docs: https://develop.sentry.dev/sdk/event-payloads/stacktrace/
   */
  const formatStackTraceForSentry = (error) => {
    const stack = error.stack || "";
    const stackFrames = stack.split("\n").slice(1); // Skip the first line containing the error message

    const frames = stackFrames.map((frame) => {
      // Typical stack frame format: " at functionName (filePath:line:column)"
      const match = frame.trim().match(/at\s+(.*?)\s+\((.*?):(\d+):(\d+)\)/);

      if (match) {
        const [, functionName, filename, lineno, colno] = match;
        return {
          function: functionName,
          filename,
          lineno: parseInt(lineno, 10),
          colno: parseInt(colno, 10),
        };
      }

      // Handle different or malformed stack traces
      return {
        function: "",
        filename: frame,
        lineno: undefined,
        colno: undefined,
      };
    });

    return frames.reverse();
  };

  /**
   * Converts any application error into Sentry's Exception.
   * Docs: https://develop.sentry.dev/sdk/event-payloads/exception/
   */
  const buildSentryExceptionFromAppError = (error, errorOptions = { isHandled: true }) => {
    const type = error.name || "Error";
    const value = error.message || error.description || error.toString();
    const stackFrames = formatStackTraceForSentry(error);
    const stacktrace = stackFrames.length > 0 ? { frames: stackFrames } : undefined;
    const mechanism = { type: "generic", handled: errorOptions.isHandled };

    return { type, value, stacktrace, mechanism };
  }

  // eslint-disable-next-line arrow-body-style
  const buildExceptionContexts = () => {
    return {
      browser: Utils.getBrowserData(),
      os: {
        name: Utils.getOSName(),
        version: Utils.getOSVersion(),
      },
    };
  };

  /**
   * Docs: https://develop.sentry.dev/sdk/rate-limiting/#rate-limit-enforcement
   */
  const handleRateLimitExceededError = (error) => {
    isApiRequestsSuppressed = true;

    const sentryDefaultRateLimitSec = 60;
    const retryAfterSec = error.response?.headers["Retry-After"] || sentryDefaultRateLimitSec;

    setTimeout(() => {
      isApiRequestsSuppressed = false;
    }, retryAfterSec * 1000);
  }

  const handleRequestError = (error) => {
    switch (error.status) {
      case 429:
        handleRateLimitExceededError(error);
        break;
      default:
        // eslint-disable-next-line no-console
        console.log(error);
    }
  }

  const sendEventToSentry = (payload) => {
    if (isApiRequestsSuppressed) {
      return;
    }

    const url = `${BASE_API_URL}/envelope/`;
    const headers = {
      "Content-Type": "text/plain",
      "X-Sentry-Auth": `Sentry sentry_version=7, sentry_key=${PUBLIC_KEY}`,
    };

    $.ajax({
      url,
      method: "POST",
      headers,
      dataType: "json",
      data: payload,
    })
      .fail((e) => handleRequestError(e));
  }

  /**
   * Build payload and send Sentry event request.
   */
  const processException = (exception, extraData) => {
    const eventId = Utils.generateUUID();
    const timestamp = new Date().toISOString();

    // Docs: https://develop.sentry.dev/sdk/envelopes/#headers
    const envelopeHeaders = JSON.stringify({
      sent_at: timestamp,
      event_id: eventId,
    });

    // Docs: https://develop.sentry.dev/sdk/event-payloads/
    const itemPayload = JSON.stringify({
      timestamp,
      platform: "javascript",
      level: "error",
      release: options.pluginVersion,
      environment: options.env,
      exception: [exception],
      extra: extraData,
      contexts: buildExceptionContexts(),
      tags: {
        domain: window.location.origin,
        moodle_version: options.moodleVersion,
      },
    });

    // Docs: https://develop.sentry.dev/sdk/envelopes/#event
    const itemHeaders = JSON.stringify({
      type: "event",
      length: new Blob([itemPayload]).size,
    });

    const requestPayload = `${envelopeHeaders}\n${itemHeaders}\n${itemPayload}\n`;
    sendEventToSentry(requestPayload);
  };

  const init = (initOptions) => {
    const { baseApiUrl, publicKey } = parseDsn(initOptions.sentryDSN);

    options = initOptions;
    BASE_API_URL = baseApiUrl;
    PUBLIC_KEY = publicKey;

    isInitialized = true;
  };

  const captureError = (error, extraData) => {
    if (!isInitialized) {
      return;
    }

    try {
      const exception = buildSentryExceptionFromAppError(error, { isHandled: true });
      processException(exception, extraData);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('"captureError" failed', err);
    }
  }

  return {
    init,
    captureError,
  }
});
