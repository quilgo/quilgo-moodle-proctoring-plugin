define(["jquery"], ($) => {
  let proctoringEnabledCheckbox;
  let proctoringCollectors = [];

  const onProctoringEnabledChange = () => {
    if (proctoringEnabledCheckbox.is(":checked")) {
      proctoringEnabledCheckbox.closest(".plasm-setting-option").addClass("proctoring-enabled");

      proctoringCollectors.forEach((collector) => {
        collector.closest(".plasm-setting-option").removeClass("proctoring-disabled");
      });

      return;
    }

    proctoringEnabledCheckbox.closest(".plasm-setting-option").removeClass("proctoring-enabled");

    proctoringCollectors.forEach((collector) => {
      collector.closest(".plasm-setting-option").addClass("proctoring-disabled");

      if (collector.is(":checkbox")) {
        collector.prop("checked", false);
      }
    });
  };

  return {
    initQuizSettingActions: () => {
      proctoringEnabledCheckbox = $("#id_plasm_enabled");
      proctoringCollectors = [
        $('[aria-label="plasm_focus"]'),
        $("#id_plasm_camera"),
        $("#id_plasm_screen"),
        $("#id_plasm_force"),
      ];

      proctoringEnabledCheckbox.on("change", () => {
        onProctoringEnabledChange();
      });

      onProctoringEnabledChange();
    },
  };
});
