define([
  "jquery",
  "core/ajax",
  "core/modal_factory",
  "core/modal",
  "core/templates",
  "core/str",
  "core/modal_events",
  "quizaccess_quilgo/sentry",
], ($, Ajax, ModalFactory, Modal, Templates, Str, ModalEvents, Sentry) => {
  let isInitiated = false;
  let coreModal;
  let confirmModal;
  let confirmForm;
  let emailInput;
  let submitButton;
  let askLater;
  let confirmLoadingIcon;
  let isConfirmOnProcess = false;
  let appData;
  let defaultErrorMessage;
  let confirmStart;
  let confirmFinish;

  const fetchAppData = () => {
    const wsfunction = "quizaccess_quilgo_fetch_app_data";
    const request = { methodname: wsfunction, args: {} };

    return Ajax.call([request])[0];
  };

  const setConfirmation = (isrequesttoasklater, email) => {
    const wsfunction = "quizaccess_quilgo_confirm_email";
    let params = { isrequesttoasklater };
    if (isrequesttoasklater === false) {
      params = { ...params, email };
    }
    const request = { methodname: wsfunction, args: params };

    return Ajax.call([request])[0];
  };

  const isValidEmailAndNotContainsLocalhost = (email) => {
    const isEmailContainsLocalhost = email.includes("localhost");
    if (isEmailContainsLocalhost) {
      return false;
    }

    const isValidEmail =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email,
      );

    return isValidEmail;
  };

  const toggleElementsOnProcess = (isConfirmEmail = true) => {
    emailInput.attr("disabled", isConfirmOnProcess);
    submitButton.attr("disabled", isConfirmOnProcess);
    askLater.attr("disabled", isConfirmOnProcess);

    if (isConfirmEmail) {
      confirmLoadingIcon.toggleClass("hidden");
    }
  };

  const initElements = () => {
    confirmForm = $(".quilgo-confirm-email-form");
    emailInput = $(".quilgo-confirm-email-input");
    submitButton = $(".quilgo-confirm-email-submit-button");
    askLater = $(".quilgo-confirm-email-ask-later");
    confirmLoadingIcon = $(".quilgo-confirm-loading-icon");
    defaultErrorMessage = $(".quilco-confirm-email-default-error-message");
    confirmStart = $(".quilgo-confirm-email-start");
    confirmFinish = $(".quilgo-confirm-email-finish");

    const appCurrentEmail = appData.email || "";
    const isValidAppCurrentEmail = isValidEmailAndNotContainsLocalhost(appCurrentEmail);
    if (isValidAppCurrentEmail) {
      emailInput.val(appCurrentEmail);
    }

    confirmForm.submit(async (e) => {
      e.preventDefault();

      if (isConfirmOnProcess) {
        return;
      }

      const confirmedEmail = emailInput.val();
      defaultErrorMessage.addClass("hidden");

      const isValidConfirmedEmail = isValidEmailAndNotContainsLocalhost(confirmedEmail);
      if (isValidConfirmedEmail === false) {
        defaultErrorMessage.removeClass("hidden");
        return;
      }

      isConfirmOnProcess = true;
      toggleElementsOnProcess();

      try {
        const response = await setConfirmation(false, confirmedEmail);
        if (response.issuccess) {
          confirmFinish.removeClass("hidden");
          confirmStart.addClass("hidden");

          setTimeout(() => {
            confirmModal.hide();
          }, 3_000);

          return;
        }

        defaultErrorMessage.removeClass("hidden");
      } catch (error) {
        defaultErrorMessage.removeClass("hidden");
      } finally {
        isConfirmOnProcess = false;
        toggleElementsOnProcess();
      }
    });

    /**
     * When "Ask me later" clicked, intentionally not handle the error (e.g show error message).
     * We just close the popup to avoid user blocked can't move to other menu.
     * The popup will shown again if visit the administration page again.
     */
    askLater.click(async () => {
      if (isConfirmOnProcess) {
        return;
      }

      isConfirmOnProcess = true;
      toggleElementsOnProcess(false);

      try {
        await setConfirmation(true);
      } catch (error) {
        Sentry.captureError(error);
      } finally {
        isConfirmOnProcess = false;
        toggleElementsOnProcess(false);

        confirmModal.hide();
      }
    });
  };

  const initConfirmModal = async () => {
    coreModal
      .create({
        title: Str.get_string("report_detail_title", "quizaccess_quilgo"),
        body: Templates.render("quizaccess_quilgo/email-confirmation", { id: 2 }),
        removeOnClose: true,
      })
      .then((modal) => {
        confirmModal = modal;
        confirmModal.show();

        modal.getRoot().on(ModalEvents.shown, () => {
          // Override base modal header styles
          const modalHeader = $(".modal-header");
          modalHeader.addClass("hidden");
        });

        modal.getRoot().on(ModalEvents.bodyRendered, () => {
          initElements();
        });

        modal.getRoot().on(ModalEvents.outsideClick, (e) => {
          // Prevent closing the modal when clicking outside of it.
          e.preventDefault();
        });
      });
  };

  const initBehaviors = async () => {
    /**
     * Starting moodle 4.3, 'ModalFactory.create' is depcrecated
     * As per other moodle script example starting moodle 4.3 they use 'Modal.create'
     */
    coreModal = ModalFactory;
    if (typeof Modal.create !== "undefined") {
      coreModal = Modal;
    }

    await initConfirmModal();
  };

  return {
    init: async (_sentryOptions) => {
      /**
       * Need to check with isInitiated because this email-confirmation.js possibly called twice.
       * Called twice because we "force" attach plugin menu for our plugin to use custom plugin
       * setting page. We "force" hide original plugin menu with css to avoid duplicate menu in admin page.
       */
      if (isInitiated) {
        return;
      }

      isInitiated = true;
      Sentry.init(_sentryOptions);

      try {
        appData = await fetchAppData();
        const isResponseDataReturnedProperly = Object.keys(appData).length > 0;
        if (isResponseDataReturnedProperly && appData.confirmedEmail == null) {
          await initBehaviors();
        }
      } catch (err) {
        Sentry.captureError(err);
      }
    },
  };
});
