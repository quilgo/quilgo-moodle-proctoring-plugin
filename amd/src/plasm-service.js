/* globals Plasm */

define(["jquery", "quizaccess_quilgo/sentry"], ($, Sentry) => {
  const iframeName = "quilgo-iframe";
  // in seconds
  const defaultExpiresIn = 3_600;
  // for preview mode we only set it 60s
  const previewModeDefaultExpiresIn = 60;
  // in ms
  const defaultPreviewModeShotsInterval = 6_000;
  let submitPreflightAttempt;
  let plasmIframe;
  let plasmIframeLoader;
  let pageWrapper;
  let isPlasmNotAvailable = false;
  let preflightForm;
  let quilgoCheckFinished;
  let cancelPreflightAttempt;
  let PLASM;
  let plasmConfig;
  const collectors = {};
  let cameraAccessTimeout;
  let desyncMS = null;
  let quizData = null;
  let cmid = null;
  let isPlasmPlayed = false;
  let attemptStartTime = null;
  let scheduledStopTimerId = null;
  let isPreviewUser = false;
  let activeTrackWithUnsupportedCollectors = [];
  const behaviors = {
    click: "click",
    dblclick: "dblclick",
    copy: "copy",
    cut: "cut",
    paste: "paste",
    load: "load",
    online: "online",
    offline: "offline",
    blur: "blur",
    focus: "focus",
    beforeunload: "beforeunload",
    contextmenu: "contextmenu",
    change: "change",
    drag: "drag",
    select: "select",
    resize: "resize",
    scroll: "scroll",
    mouseout: "mouseout",
    key: "key",
    unfocus: "unfocus",
    mousespeed: "mousespeed",
    keystrokespeed: "keystrokespeed",
  };
  let elementIntervalId;
  let isPreflightFormSubmitted = false;
  let plasmNotAvailableError;
  let quilgoSessionDuration;
  let quilgoToDisableDeviceWarning;
  let quilgoWarningDisableDeviceWarning;
  let quilgoWarningDisableCheckbox;
  let moodleVersion;
  let pluginVersion;

  const plasmSetConfig = (config) => {
    plasmConfig = config;
  };

  const setQuizData = (quiz) => {
    quizData = quiz;
  };

  const isPlasmCameraEnabled = () =>
    quizData == null ? false : Number(quizData.plasm_camera) === 1;

  const isPlasmScreenEnabled = () =>
    quizData == null ? false : Number(quizData.plasm_screen) === 1;

  const isPlasmForceEnabled = () => (quizData == null ? false : Number(quizData.plasm_force) === 1);

  /**
   * Converted time close of quiz data to milisecond time based
   * because the original is unix timestamps which has second time based
   */
  const quizTimeClose = () => {
    if (quizData == null) {
      return null;
    }

    if (quizData.timeclose != null && quizData.timeclose !== 0) {
      return new Date(Number(quizData.timeclose) * 1000).getTime();
    }

    return null;
  };

  const quizTimeLimit = () => {
    if (quizData == null) {
      return null;
    }

    if (quizData.timelimit != null && quizData.timelimit !== 0) {
      return Number(quizData.timelimit);
    }

    return null;
  };

  const syncedDateNow = () => {
    const dms = desyncMS != null ? desyncMS : 0;
    return Date.now() - dms;
  };

  /**
   * Converted attempt start time of quiz data to milisecond time based
   * because the original is unix timestamps which has second time based
   */
  const getAttemptStartTime = () =>
    attemptStartTime != null ? new Date(Number(attemptStartTime) * 1000).getTime() : Date.now();

  const getPlasmDuration = () => {
    // timeClose already ms time based
    const timeClose = quizTimeClose();
    // timeLimit is seconds based
    const timeLimit = quizTimeLimit();
    const isTimeCloseSet = timeClose != null && timeClose > 0;
    const isTimeLimitSet = timeLimit != null && timeLimit > 0;
    let duration = 0;

    if (isPreviewUser) {
      duration = previewModeDefaultExpiresIn;
    } else {
      duration = isTimeLimitSet ? timeLimit : defaultExpiresIn;
      if (isTimeCloseSet) {
        // attemptStatTime already ms time based
        const attemptStatTime = getAttemptStartTime();
        const timeCloseRemainingTimeInSeconds = (timeClose - attemptStatTime) / 1000;

        if (timeCloseRemainingTimeInSeconds < duration) {
          duration = timeCloseRemainingTimeInSeconds;
        }
      }
    }

    // Set "safe" to handle case when init Plasm very near test end time.
    duration = duration >= 0 && duration < 1 ? 1 : duration;

    // Set "safe" maximum duration because in moodle we can set time limt > 1 day
    const maxDurationInSeconds = 86_400;
    duration = Math.min(duration, maxDurationInSeconds);

    return duration;
  };

  const getImageCollectorInterval = (collectorId, maxShots, duration) => {
    const { snapshotsMinInterval, screenshotsMinInterval } = PLASM.config;
    const shotsMinInterval =
      collectorId === "camera" ? snapshotsMinInterval : screenshotsMinInterval;

    const shotsInterval = isPreviewUser
      ? defaultPreviewModeShotsInterval
      : (duration / maxShots) * 1000;

    return Math.max(shotsInterval, shotsMinInterval);
  };

  const getReady = async () => {
    if (!PLASM) {
      return;
    }

    await PLASM.getReady().catch((err) => Sentry.captureError(err));
  };

  const play = (attemptId, sessionId) => {
    if (PLASM == null || PLASM.state !== "stopped" || isPlasmPlayed) {
      return;
    }

    const collectibles = Object.values(behaviors);
    const collectQuestionAndOptionEvents = [
      behaviors.copy,
      behaviors.cut,
      behaviors.paste,
      behaviors.click,
      behaviors.dblclick,
      behaviors.change,
      behaviors.select,
    ];
    const plasmDurationForShotsInterval = getPlasmDuration();
    const { maxShotsPerQuiz } = plasmConfig;

    const startOptions = {
      sessionUuid: sessionId,
      collectibles,
      iframesUsed: true,
      addMetadata: (event) => {
        const meta = { attemptId };
        const collectQuestionAndOptionInfo = collectQuestionAndOptionEvents.includes(event.type);

        if (collectQuestionAndOptionInfo) {
          const element = event.target;
          const clickedQuestion = element.closest(".que");
          const iframeParentBody = event.srcElement.ownerDocument?.body || null;

          if (clickedQuestion) {
            meta.questionId = clickedQuestion.getAttribute("id");
          } else if (iframeParentBody) {
            // Example iframe parent body id q224:1_answer_id
            const iframeParentBodyId = iframeParentBody.getAttribute("data-id");
            if (iframeParentBodyId != null) {
              const questionIdFromIframeParentBody = iframeParentBodyId
                .replace("_answer_id", "")
                .replace(":", "-")
                .replace("q", "");

              // add "question-" suffix because question id from ".que" element is "question-224-1"
              meta.questionId = `question-${questionIdFromIframeParentBody}`;
            }
          }

          /**
           * For option id, tt the moment we only collect unique id for input radio type only
           * As per the other options there is no unique id
           * And at the moment in Plasm we no need the option id for patterns
           */
          const clickedRadio = element.closest('input[type="radio"]');
          if (clickedRadio) {
            meta.optionId = clickedRadio.getAttribute("id");
          }
        }

        return meta;
      },
    };

    PLASM.configuredCollectors.forEach((collector) => {
      if (collector.id === "camera") {
        startOptions.snapshotInterval = getImageCollectorInterval(
          "camera",
          maxShotsPerQuiz,
          plasmDurationForShotsInterval,
        );
      }

      if (collector.id === "screen") {
        startOptions.screenshotInterval = getImageCollectorInterval(
          "screen",
          maxShotsPerQuiz,
          plasmDurationForShotsInterval,
        );
      }
    });

    PLASM.start(startOptions)
      .then(() => {
        isPlasmPlayed = true;
      })
      .catch((err) => Sentry.captureError(err));
  };

  const plasmStop = () => {
    if (!PLASM) {
      return;
    }

    if (scheduledStopTimerId != null) {
      clearTimeout(scheduledStopTimerId);
    }

    PLASM.stop();
  };

  const plasmInit = async () => {
    if (isPlasmNotAvailable) {
      return false;
    }

    try {
      PLASM = new Plasm({
        debug: true,
        clientToken: plasmConfig.clientToken,
        plasmApiUrl: plasmConfig.apiEndpoint,
        useCollectBehavior: true,
        applicationMetadata: {
          clientVersion: moodleVersion,
          integrationVersion: pluginVersion,
        },
      });

      await PLASM.init();

      return true;
    } catch (error) {
      Sentry.captureError(error);

      return false;
    }
  };

  const showUnsupportedErrorMessage = () => {
    $(".plasm, .plasm-not-supported").removeClass("quilgo-hidden");
  };

  const getNextSetup = () => {
    let nextSetup;

    // eslint-disable-next-line no-restricted-syntax
    for (const collectorType in collectors) {
      if (collectors[collectorType] === null) {
        nextSetup = collectorType;

        break;
      }
    }

    return nextSetup;
  };

  const adjustCollectorsHints = (useCamera, useScreen) => {
    $(".plasm-hint-camera, .plasm-hint-screen, .plasm-hint-camera-and-screen").addClass(
      "quilgo-hidden",
    );

    if (useCamera) {
      $(".plasm-hint-camera").removeClass("quilgo-hidden");
      $(".consent-title > .plasm-hint-camera").addClass("text-capitalize");
    }

    if (useScreen) {
      $(".plasm-hint-screen").removeClass("quilgo-hidden");
      if (!useCamera) {
        $(".consent-title > .plasm-hint-screen").addClass("text-capitalize");
      }
    }

    if (useCamera && useScreen) {
      $(".plasm-hint-camera-and-screen").removeClass("quilgo-hidden");
    }
  };

  const openConfirmation = () => {
    const cameraStream = collectors.camera && collectors.camera.meta.stream;
    const screenStream = collectors.screen && collectors.screen.meta.stream;

    if (cameraStream) {
      const videoObj = $("#quilgo-video video")[0];

      videoObj.srcObject = cameraStream;
      videoObj.setAttribute("playsinline", true);
      videoObj.play();
    }

    if (screenStream) {
      const videoScreenObj = $("#plasm-screen-video video")[0];

      videoScreenObj.srcObject = screenStream;
      videoScreenObj.setAttribute("playsinline", true);
      videoScreenObj.play();
    }

    adjustCollectorsHints(cameraStream, screenStream);

    if (cameraStream || screenStream) {
      $(".plasm").removeClass("quilgo-hidden");
      $(".plasm-caliber").removeClass("quilgo-hidden");
      quilgoCheckFinished.parent().removeClass("quilgo-hidden");
    }
  };

  const nextSetup = () => {
    const setupNext = getNextSetup();

    if (setupNext === "navigation") {
      // eslint-disable-next-line no-use-before-define
      setup("navigation");
      return;
    }

    if (!setupNext) {
      openConfirmation();

      return;
    }

    $(".plasm-setup").removeClass("quilgo-hidden");
    $(".plasm-setup-collector").addClass("quilgo-hidden");
    $(`.plasm-setup-${setupNext}`).removeClass("quilgo-hidden");
  };

  const setup = (type) => {
    $(".plasm-setup").addClass("quilgo-hidden");
    $(".plasm-consent").remove();
    $(".plasm-error").addClass("quilgo-hidden");

    if (type === "camera") {
      $(".plasm-loading-setup").removeClass("quilgo-hidden");

      cameraAccessTimeout = setTimeout(() => {
        $(".plasm-loading-setup").addClass("quilgo-hidden");
      }, 2000);
    }

    const collectorOptions = {};

    switch (type) {
      case "camera":
      case "screen":
        // we will override this value during plasm start
        collectorOptions.interval = 1_000;
        break;
      case "navigation":
        collectorOptions.enableUnfocusedShots = true;
        break;
      default:
    }

    PLASM.setupCollector(type, collectorOptions).then((setupResults) => {
      if (type === "camera") {
        clearTimeout(cameraAccessTimeout);
        $(".plasm-loading-setup").addClass("quilgo-hidden");
      }

      switch (setupResults.status) {
        case "permission_denied":
        case "device_not_found":
          $(`.plasm-${type}-access-error`).removeClass("quilgo-hidden");
          break;
        case "not_supported":
          if (isPlasmForceEnabled()) {
            showUnsupportedErrorMessage();
            break;
          }

          nextSetup();
          break;
        case "success":
          if (
            type === "screen" &&
            setupResults.status !== "not_supported" &&
            setupResults.meta.selectedArea !== "monitor"
          ) {
            $(".plasm-screen-area-error").removeClass("quilgo-hidden");
            break;
          }

          collectors[type] = setupResults;
          if (type === "camera") {
            $(".plasm-setup-camera-enabled-hint").removeClass("quilgo-hidden");
          }

          nextSetup();
          break;
        default:
          break;
      }
    });
  };

  const requestConsent = (useCamera, useScreen) => {
    adjustCollectorsHints(useCamera, useScreen);
    $(".plasm").removeClass("quilgo-hidden");
    $(".plasm-consent").removeClass("quilgo-hidden");
  };

  const setupPermissions = async (requestCamera, requestScreen) => {
    const isInitPlasmSuccessfully = await plasmInit();

    if (isInitPlasmSuccessfully === false) {
      plasmNotAvailableError.removeClass("quilgo-hidden");
      submitPreflightAttempt.addClass("quilgo-hidden");
      cancelPreflightAttempt.addClass("quilgo-hidden");
      return;
    }

    const plasmSupportedCollectors = PLASM.supportedCollectors;
    const unsupportedCollectors = [];
    let isUnsupportedCameraOrScreenCollector = false;

    const isCameraCollectorIncluded = plasmSupportedCollectors.includes("camera");
    const useCamera = requestCamera && isCameraCollectorIncluded;
    if (requestCamera && isCameraCollectorIncluded === false) {
      isUnsupportedCameraOrScreenCollector = true;
    }

    if (useCamera) {
      collectors.camera = null;
    } else {
      unsupportedCollectors.push("camera");
    }

    const isScreenCollectorIncluded = plasmSupportedCollectors.includes("screen");
    const useScreen = requestScreen && isScreenCollectorIncluded;
    if (requestScreen && isScreenCollectorIncluded === false) {
      isUnsupportedCameraOrScreenCollector = true;
    }

    if (useScreen) {
      collectors.screen = null;
    } else {
      unsupportedCollectors.push("screen");
    }

    collectors.navigation = null;

    if (isUnsupportedCameraOrScreenCollector && isPlasmForceEnabled()) {
      showUnsupportedErrorMessage();
      return;
    }

    activeTrackWithUnsupportedCollectors = unsupportedCollectors;
    $(".plasm-setup-camera, .plasm-setup-camera-retry").on("click", (e) => {
      e.preventDefault();
      setup("camera");
    });
    $(".plasm-setup-screen, .plasm-setup-screen-retry").on("click", (e) => {
      e.preventDefault();
      setup("screen");
    });

    requestConsent(useCamera, useScreen);
    nextSetup();
  };

  const onIframeUrlChanged = (iframe, callback) => {
    let lastDispatched = null;

    const dispatchChange = () => {
      const newHref = iframe.contentWindow.location.href;
      plasmIframeLoader.removeClass("quilgo-hidden");

      if (newHref !== lastDispatched) {
        callback(newHref);
        lastDispatched = newHref;
      }
    };

    const unloadHandler = () => {
      window.focus();
      setTimeout(dispatchChange, 0);
    };

    const attachUnload = () => {
      iframe.contentWindow.removeEventListener("unload", unloadHandler);
      iframe.contentWindow.addEventListener("unload", unloadHandler);
    };

    iframe.addEventListener("load", () => {
      attachUnload();
      dispatchChange();

      setTimeout(() => {
        plasmIframeLoader.addClass("quilgo-hidden");
      }, 100);
      window.focus();
    });

    attachUnload();
  };

  const addIframe = () => {
    const iframe = document.createElement("iframe");
    iframe.setAttribute("name", iframeName);
    iframe.setAttribute("id", iframeName);
    iframe.setAttribute("class", `${iframeName} quilgo-hidden`);
    document.body.append(iframe);

    plasmIframe = $(`#${iframeName}`);
  };

  const schedulePlasmStop = () => {
    const timeLimit = getPlasmDuration();
    const startTime = getAttemptStartTime();
    const timeLimitEnd = new Date(startTime + timeLimit * 1000).getTime();

    const delay = timeLimitEnd - syncedDateNow();
    if (scheduledStopTimerId !== undefined) {
      clearTimeout(scheduledStopTimerId);
    }

    scheduledStopTimerId = window.setTimeout(() => {
      plasmStop();
    }, delay);
  };

  const getUrlFileLocation = (url) => {
    // Get .php file with location for example host.com/mod/quiz/attempt.php?x=1&y=2 to attempt.php
    const urlObject = new URL(url);

    return urlObject.pathname.split("/").pop();
  };

  const waitForIframeBodyElement = (iframeId, callback) => {
    elementIntervalId = setInterval(() => {
      const targetElement = document.getElementById(iframeId).contentWindow.document.body;
      if (targetElement) {
        clearInterval(elementIntervalId);
        callback(targetElement);
      }
    }, 500);
  };

  const addIframeEvent = () => {
    onIframeUrlChanged(document.getElementById(iframeName), async (newUrl) => {
      const fileLocation = getUrlFileLocation(newUrl);
      const urlLocationIframeShouldRun = ["attempt.php", "summary.php"];
      const iframeShouldRun = urlLocationIframeShouldRun.includes(fileLocation);

      if (iframeShouldRun) {
        const params = new URL(newUrl).searchParams;
        const attemptId = params.get("attempt");

        const plasmShouldRun = PLASM.state !== "started";
        if (plasmShouldRun) {
          waitForIframeBodyElement(iframeName, async (bodyElement) => {
            const quizSessionClassName = "quiz-quilgo-tracking-sessionid";
            const classNames = bodyElement.className.split(" ");
            const sessionIdClassName = classNames.find((className) =>
              className.includes(quizSessionClassName),
            );

            if (sessionIdClassName != null) {
              const sessionId = sessionIdClassName.replace(`${quizSessionClassName}-`, "");
              await getReady();
              play(attemptId, sessionId);
              schedulePlasmStop();
            }
          });
        }
      }

      if (iframeShouldRun === false) {
        plasmStop();
        window.location.href = newUrl;
      }
    });
  };

  const addPageLoader = () => {
    const loaderName = "quilgo-iframe-loader";
    const loaderContainer = document.createElement("div");
    loaderContainer.setAttribute("id", loaderName);
    loaderContainer.setAttribute("class", `${loaderName} quilgo-hidden`);
    document.body.append(loaderContainer);

    const loaderSpinner = document.createElement("div");
    loaderSpinner.setAttribute("class", "spinner-border text-primary");

    plasmIframeLoader = $(`#${loaderName}`);
    plasmIframeLoader.append(loaderSpinner);
  };

  const initElements = () => {
    quilgoCheckFinished = $("#id_quilgocheckfinished");

    addIframe();
    addPageLoader();
    submitPreflightAttempt = $("#id_submitbutton");
    submitPreflightAttempt.addClass("btn-lg");
    cancelPreflightAttempt = $("#id_cancel");
    cancelPreflightAttempt.addClass("btn-lg");
    pageWrapper = $("#page-wrapper");
    quilgoToDisableDeviceWarning = $(".quilgo-to-disable-device-warning");
    quilgoWarningDisableDeviceWarning = $(".quilgo-warning-disable-device");
    quilgoWarningDisableCheckbox = $("#quilgo-disable-device-warning-check");

    /**
     * Always init check finished as unchecked in case user redirected
     * to the startattempt page due other preflight check failed (e.g wrong password)
     */
    quilgoCheckFinished.attr("checked", false);

    plasmNotAvailableError = $(".plasm-not-available");
    quilgoSessionDuration = $(`input[name="quilgosessionduration"]`);

    quilgoCheckFinished.change(() => {
      const requestCamera = isPlasmCameraEnabled();
      const requestScreen = isPlasmScreenEnabled();

      if (quilgoCheckFinished.is(":checked")) {
        if (requestCamera || requestScreen) {
          quilgoToDisableDeviceWarning.removeClass("quilgo-hidden");
        } else {
          submitPreflightAttempt.removeClass("quilgo-hidden");
        }

        return;
      }

      if (requestCamera || requestScreen) {
        quilgoToDisableDeviceWarning.addClass("quilgo-hidden");
      } else {
        submitPreflightAttempt.addClass("quilgo-hidden");
      }
    });

    quilgoToDisableDeviceWarning.on("click", (e) => {
      e.preventDefault();
      $(".plasm-caliber").addClass("quilgo-hidden");
      quilgoWarningDisableDeviceWarning.removeClass("quilgo-hidden");
      quilgoToDisableDeviceWarning.addClass("quilgo-hidden");
      quilgoCheckFinished.parent().addClass("quilgo-hidden");
    });

    quilgoWarningDisableCheckbox.change(() => {
      if (quilgoWarningDisableCheckbox.is(":checked")) {
        submitPreflightAttempt.removeClass("quilgo-hidden");
        return;
      }

      submitPreflightAttempt.addClass("quilgo-hidden");
    });
  };

  const addSubmitPreflightEvent = () => {
    /**
     * Set start attempt button to the center
     */
    const formActionsContainer = preflightForm.getElementsByClassName("felement");
    if (formActionsContainer.length > 0) {
      const buttonContainer =
        formActionsContainer[formActionsContainer.length - 1].getElementsByClassName("d-flex");
      if (buttonContainer.length > 0) {
        buttonContainer[0].classList.add("justify-content-center");
      }
    }

    preflightForm.addEventListener("submit", (e) => {
      e.preventDefault();

      if (isPreflightFormSubmitted) {
        return;
      }
      isPreflightFormSubmitted = true;

      const plasmDurationForCreateSession = getPlasmDuration();
      quilgoSessionDuration.val(plasmDurationForCreateSession);

      $.ajax({
        url: e.target.action,
        method: "POST",
        data: $("#mod_quiz_preflight_form").serialize(),
      })
        .done((res) => {
          const parsedPreflightForm = $($.parseHTML(res)).find(
            'form[id="mod_quiz_preflight_form"]',
          );
          if (parsedPreflightForm.length === 1) {
            preflightForm.submit();

            return;
          }

          plasmIframeLoader.removeClass("quilgo-hidden");
          preflightForm.setAttribute("target", iframeName);
          preflightForm.submit();

          addIframeEvent();

          plasmIframe.removeClass("quilgo-hidden");
          cancelPreflightAttempt.click();

          pageWrapper.remove();
        })
        .fail(() => {
          isPreflightFormSubmitted = false;
        });
    });
  };

  const waitForElement = (selector, callback) => {
    elementIntervalId = setInterval(() => {
      const targetElement = document.querySelector(selector);
      if (targetElement) {
        clearInterval(elementIntervalId);
        callback(targetElement);
      }
    }, 500);
  };

  const initBehaviors = async (
    _plasmConfig,
    _quiz,
    _serverTime,
    _attemptStartTime,
    _cmid,
    _isPreviewUser,
  ) => {
    const dateNow = Date.now();
    desyncMS = dateNow - new Date(_serverTime).getTime();

    attemptStartTime = _attemptStartTime;
    initElements();

    cmid = _cmid;
    isPreviewUser = _isPreviewUser;
    setQuizData(_quiz);
    plasmSetConfig(_plasmConfig);
    const requestCamera = isPlasmCameraEnabled();
    const requestScreen = isPlasmScreenEnabled();
    isPlasmNotAvailable = typeof Plasm === "undefined";
    await setupPermissions(requestCamera, requestScreen);

    const isShouldNotAskConsent =
      (requestCamera === false && requestScreen === false) ||
      activeTrackWithUnsupportedCollectors.length === 2;

    cancelPreflightAttempt.addClass("quilgo-hidden");
    submitPreflightAttempt.addClass("quilgo-hidden");

    if (!isShouldNotAskConsent) {
      quilgoCheckFinished.parent().addClass("quilgo-hidden");
    }

    if (isPlasmNotAvailable) {
      Sentry.captureError(new Error("Plasm not available"));
      return;
    }

    waitForElement("#mod_quiz_preflight_form", (targetElement) => {
      // eslint-disable-next-line prefer-destructuring
      preflightForm = targetElement;

      addSubmitPreflightEvent();

      /**
       * Override url when user on startattempt.php to avoid user directly to attempt.php
       * without setup tacking. So in this case user will reloaded to view.php
       */
      const isOnStartattempt = getUrlFileLocation(window.location.href) === "startattempt.php";
      if (isOnStartattempt) {
        window.history.replaceState({}, "", `/mod/quiz/view.php?id=${cmid}`);
      }
    });
  };

  return {
    init: async (
      _plasmConfig,
      _quiz,
      _serverTime,
      _attemptStartTime,
      _cmid,
      _isPreviewUser,
      _sentryOptions,
      _moodleVersion,
      _pluginVersion,
    ) => {
      Sentry.init(_sentryOptions);
      moodleVersion = _moodleVersion;
      pluginVersion = _pluginVersion;

      try {
        await initBehaviors(
          _plasmConfig,
          _quiz,
          _serverTime,
          _attemptStartTime,
          _cmid,
          _isPreviewUser,
        );
      } catch (err) {
        Sentry.captureError(err);
      }
    },
  };
});
