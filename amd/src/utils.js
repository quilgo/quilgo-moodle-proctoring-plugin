define([], () => {
  const detectBrowser = () => {
    let sBrowser;
    const sUsrAg = navigator.userAgent;
  
    if (sUsrAg.indexOf("Firefox") > -1) sBrowser = "firefox";
    else if (sUsrAg.indexOf("SamsungBrowser") > -1) sBrowser = "samsung-internet";
    else if (sUsrAg.indexOf("Opera") > -1 || sUsrAg.indexOf("OPR") > -1) sBrowser = "opera";
    else if (sUsrAg.indexOf("Trident") > -1) sBrowser = "ie";
    else if (sUsrAg.indexOf("Edge") > -1) sBrowser = "edge";
    else if (sUsrAg.indexOf("Chrome") > -1) sBrowser = "chrome";
    else if (sUsrAg.indexOf("Safari") > -1) sBrowser = "safari";
    else sBrowser = "unknown";
  
    return sBrowser;
  };

  const getBrowserData = () => {
    const ua = navigator.userAgent;
    const name = detectBrowser();
    let version = "Unknown";

    // Chrome
    if (/Chrome/.test(ua)) {
      version = ua.match(/Chrome\/(\d+\.\d+)/)?.[1] ?? version;
    }
    // Firefox
    else if (/Firefox/.test(ua)) {
      version = ua.match(/Firefox\/(\d+\.\d+)/)?.[1] ?? version;
    }
    // Safari
    else if (/Safari/.test(ua)) {
      version = ua.match(/Version\/(\d+\.\d+)/)?.[1] ?? version;
    }
    // Edge
    else if (/Edge/.test(ua)) {
      version = ua.match(/Edge\/(\d+\.\d+)/)?.[1] ?? version;
    }
    // Internet Explorer
    else if (/Trident/.test(ua)) {
      version = ua.match(/rv:(\d+\.\d+)/)?.[1] ?? version;
    }

    return { name, version };
  };

  const getOSName = () => {
    const ua = navigator.userAgent;
    const { platform } = navigator;
    let os = "Unknown";

    if (platform.indexOf("Win") !== -1) {
      os = "Windows";
    } else if (platform.indexOf("Mac") !== -1) {
      os = "macOS";
    } else if (ua.indexOf("Android") !== -1) {
      os = "Android";
    } else if (ua.indexOf("Linux") !== -1) {
      os = "Linux";
    } else if (ua.indexOf("iPhone") !== -1) {
      os = "iOS";
    }

    return os;
  };

  const getOSVersion = () => {
    const ua = navigator.userAgent;
    let version = "Unknown";

    if (navigator.appVersion.indexOf("Win") !== -1) {
      version = String(parseFloat(ua.split("Windows NT")[1].split(";")[0]));
    } else if (ua.indexOf("Mac") !== -1) {
      [version] = ua.split("Mac OS X")[1].split(")")[0].split("_");
    } else if (ua.indexOf("Android") !== -1) {
      [version] = ua.split("Android")[1].split(";");
    } else if (ua.indexOf("iPhone") !== -1) {
      [version] = ua.split("iPhone OS")[1].split("_");
    }

    return version;
  };

  const generateUUID = () => {
    let uuid = "";
    const randomValues = window.crypto.getRandomValues(new Uint8Array(16));

    for (let i = 0; i < randomValues.length; i += 1) {
      const hexDigit = randomValues[i].toString(16);
      uuid += hexDigit.length === 1 ? `0${hexDigit}` : hexDigit;
    }

    // Set the version to 4 (0b0100) and the variant to 0b10
    uuid = `${uuid.substring(0, 12)}4${uuid.substring(13, 16)}a${uuid.substring(17)}`;

    return uuid;
  }

  return {
    detectBrowser,
    getBrowserData,
    getOSName,
    getOSVersion,
    generateUUID,
  }
});
