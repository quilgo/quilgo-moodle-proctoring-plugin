<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * API configuration page
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use quizaccess_quilgo\form\api_config;

require_once(__DIR__ . '/../../../../config.php');
require_once("{$CFG->libdir}/adminlib.php");
require_once($CFG->dirroot . '/mod/quiz/accessrule/quilgo/lib.php');

require_login();

admin_externalpage_setup('quizaccess_quilgo_settings');

$configform = new api_config();

$PAGE->requires->css('/mod/quiz/accessrule/quilgo/settings.css');

echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('config_title', 'quizaccess_quilgo'));

if ($configformdata = $configform->get_data()) {
    $resp = quizaccess_quilgo_register_site();
    if (empty($resp)) {
        echo $OUTPUT->notification(get_string('config_failed_register', 'quizaccess_quilgo'), 'danger', true);
    } else {
        purge_caches();
        $configform = new api_config();
        echo $OUTPUT->notification(get_string('config_success_register', 'quizaccess_quilgo'), 'success', true);
    }
}

$configform->display();

echo $OUTPUT->footer();
