<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Execute quizaccess_quilgo upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_quizaccess_quilgo_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2024050100) {
        // Define field generate_status to be added to quizaccess_quilgo_reports.
        $table = new xmldb_table('quizaccess_quilgo_reports');
        $field = new xmldb_field('generate_status', XMLDB_TYPE_INTEGER, '1', null, null, null, null, 'error_reason');

        // Conditionally launch add field generate_status.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field stat to be added to quizaccess_quilgo_reports.
        $table = new xmldb_table('quizaccess_quilgo_reports');
        $field = new xmldb_field('stat', XMLDB_TYPE_TEXT, null, null, null, null, null, 'generate_status');

        // Conditionally launch add field stat.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Quilgo savepoint reached.
        upgrade_plugin_savepoint(true, 2024050100, 'quizaccess', 'quilgo');
    }

    if ($oldversion < 2024070600) {
        // Define field proctoring_enabled to be added to quizaccess_quilgo_settings.
        $table = new xmldb_table('quizaccess_quilgo_settings');
        // Set default value as 0 because we expect once installed new version proctoring setting disabled by default.
        // We will update disabled/enabled in the next line consider to currently active proctoring method.
        $field = new xmldb_field('proctoring_enabled', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'force_enabled');

        // Conditionally launch add field proctoring_enabled.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Update existing quiz proctoring settings.
        $sql = "SELECT * FROM {quizaccess_quilgo_settings}";
        $affectedsettings = $DB->get_recordset_sql($sql);
        foreach ($affectedsettings as $setting) {
            // Need to check proctoring enabled == 0 to avoid re-updated value during development with always update version number.
            if ($setting->proctoring_enabled == 0) {
                $proctoringenabled = $setting->camera_enabled == 1 || $setting->screen_enabled == 1 ? 1 : 0;
                $DB->set_field('quizaccess_quilgo_settings', 'proctoring_enabled', $proctoringenabled, ['id' => $setting->id]);
            }
        }
        $affectedsettings->close();

        // Define field proctoring_enabled to be added to quizaccess_quilgo_reports.
        $table = new xmldb_table('quizaccess_quilgo_reports');
        // Set default value as 1 because we expect existing reports all proctoring enabled by default before this update.
        $field = new xmldb_field('proctoring_enabled', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1', 'stat');

        // Conditionally launch add field proctoring_enabled.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Changing nullability of field plasmsessionid on table quizaccess_quilgo_reports to null.
        // We need to store quiz proctoring settings when attempt started.
        // And there is possibility plasmsessionid is null because proctoring disabled.
        $table = new xmldb_table('quizaccess_quilgo_reports');
        $field = new xmldb_field('plasmsessionid', XMLDB_TYPE_CHAR, '100', null, null, null, null, 'attemptid');

        // Launch change of nullability for field plasmsessionid.
        $dbman->change_field_notnull($table, $field);

        // Quilgo savepoint reached.
        upgrade_plugin_savepoint(true, 2024070600, 'quizaccess', 'quilgo');
    }

    return true;
}
