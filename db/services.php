<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define external services for AJAX call
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$functions = [
    'quizaccess_quilgo_fetch_report' => [
        'classname' => 'quizaccess_quilgo_external',
        'methodname' => 'fetch_report',
        'description' => 'Fetch report',
        'type' => 'write',
        'ajax' => true,
        'capabilities' => 'quizaccess/quilgo:fetchreport',
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'quizaccess_quilgo_fetch_stat' => [
        'classname' => 'quizaccess_quilgo_external',
        'methodname' => 'fetch_stat',
        'description' => 'Fetch stat',
        'type' => 'write',
        'ajax' => true,
        'capabilities' => 'quizaccess/quilgo:fetchstat',
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'quizaccess_quilgo_fetch_app_data' => [
        'classname' => 'quizaccess_quilgo_external',
        'methodname' => 'fetch_app_data',
        'description' => 'Fetch application data',
        'type' => 'read',
        'ajax' => true,
        'capabilities' => 'quizaccess/quilgo:fetchappdata',
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'quizaccess_quilgo_generate_app_dashboard_url' => [
        'classname' => 'quizaccess_quilgo_external',
        'methodname' => 'generate_app_dashboard_url',
        'description' => 'Generate application dashboard url',
        'type' => 'read',
        'ajax' => true,
        'capabilities' => 'moodle/site:config',
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'quizaccess_quilgo_confirm_email' => [
        'classname' => 'quizaccess_quilgo_external',
        'methodname' => 'confirm_email',
        'description' => 'Confirm email',
        'type' => 'write',
        'ajax' => true,
        'capabilities' => 'moodle/site:config',
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
];
