<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Hindi strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'आपकी साइट का पंजीकरण करने में कुछ समस्या आई। कृपया बाद में पुनः प्रयास करें';
$string['config_register'] = 'मेरी साइट रजिस्टर करें';
$string['config_success_register'] = 'आपकी साइट सफलतापूर्वक पंजीकृत हो गई है';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'आपका क्विल्गो निगरानी प्लगइन अभी तक सक्रिय नहीं है। यह इसलिए है क्योंकि आपकी साइट का क्विल्गो निगरानी में प्लगइन स्थापना के दौरान सही तरीके से पंजीकरण नहीं हुआ था। आप "मेरी साइट रजिस्टर करें" क्लिक करके पुनः पंजीकरण का प्रयास कर सकते हैं।';
$string['confirm_email_ask_me_later'] = 'मुझसे बाद में पूछें';
$string['confirm_email_button_text'] = 'ईमेल पते की पुष्टि करें';
$string['confirm_email_default_failed_message'] = 'आपके ईमेल पते की पुष्टि करने का प्रयास करते समय कुछ गड़बड़ी हुई। कृपया बाद में पुनः प्रयास करें और सुनिश्चित करें कि आप मान्य ईमेल पते का उपयोग करें।';
$string['confirm_email_finished'] = 'ईमेल पते की पुष्टि करने के लिए धन्यवाद। क्विल्गो का उपयोग करके आनंद लें!';
$string['confirm_email_message'] = 'कृपया अपने वर्तमान ईमेल पते की पुष्टि करें ताकि आपको महत्वपूर्ण सुरक्षा और सुविधा अपडेट प्राप्त हो सकें।';
$string['confirm_email_short_desc'] = 'क्विल्गो एक अत्यधिक स्केलेबल प्लगइन है जो कैमरा, स्क्रीन और गतिविधि को ट्रैक करता है, तथा परीक्षण पूरा होने पर प्रॉक्टरिंग रिपोर्ट तैयार करता है।';
$string['confirm_email_title'] = 'क्विल्गो को चुनने के लिए धन्यवाद';
$string['create_session_error_default_description'] = 'प्रॉक्टरिंग सेटअप विफल हो गया है। कृपया क्विज़ पेज को रीफ़्रेश करने का प्रयास करें।';
$string['create_session_error_limit_reached_description'] = 'आपके शिक्षक ने इस क्विज़ के लिए स्वचालित प्रोक्टरिंग सक्षम कर दी है, लेकिन सीमा समाप्त हो गई है। कृपया कुछ मिनटों में क्विज़ शुरू करने के लिए पुनः प्रयास करें।';
$string['general_continue'] = 'जारी रखना';
$string['general_yes'] = 'हाँ';
$string['limit_info_contact_admin'] = 'अपग्रेड करने के लिए अपने मूडल प्रशासक से संपर्क करें या यदि आपको अधिक संख्या में छात्रों के लिए ट्रायल की आवश्यकता है तो हमसे <strong>hello@quilgo.com</strong> पर संपर्क करें।';
$string['limit_info_run_up'] = 'क्विल्गो फ्री एक साथ <strong><span class="free-limitation">0</span> प्रोक्टर्ड टेस्ट प्रयास चलाने की अनुमति देता है</strong>। यह सीमा सभी क्विज़ में साझा की जाती है।';
$string['limit_info_upgrade'] = 'यदि आपको अधिक संख्या में छात्रों के लिए परीक्षण की आवश्यकता है तो {$a}<strong><span class="quilgo-upgrade-cta">अपग्रेड करने के लिए यहां क्लिक करें</span></strong> या हमसे <strong>hello@quilgo.com</strong> पर संपर्क करें।';
$string['manage_subscription_failed_info'] = 'क्विल्गो सदस्यता प्रबंधित करने का प्रयास करते समय कुछ गड़बड़ी हुई। कृपया बाद में पुनः प्रयास करें।';
$string['manage_subscription_title'] = 'मेरी क्विल्गो सदस्यता प्रबंधित करें';
$string['plasm_camera'] = 'कैमरे की निगरानी';
$string['plasm_camera_help'] = 'सुनिश्चित करें कि उत्तरदाता क्विज़ पूरा होने तक अपनी सीट न छोड़ें';
$string['plasm_enabled'] = 'प्रॉक्टरिंग सक्षम करें';
$string['plasm_focus'] = 'गतिविधि ट्रैक करें (डिफ़ॉल्ट रूप से सक्षम)';
$string['plasm_focus_help'] = 'देखें कि परीक्षा में शामिल प्रतिभागी कितनी बार क्विज़ छोड़कर अन्य टैब या एप्लिकेशन पर जाते हैं';
$string['plasm_force'] = 'निगरानी अनिवार्य';
$string['plasm_force_help'] = 'क्विज़ का प्रयास करने के लिए चयनित निगरानी विधियों की आवश्यकता होती है';
$string['plasm_screen'] = 'स्क्रीन की निगरानी';
$string['plasm_screen_help'] = 'अपने उत्तरदाताओं की स्क्रीन और संदिग्ध गतिविधियों को उच्च गुणवत्ता के साथ स्वचालित रूप से रिकॉर्ड करें ताकि अनुचित व्यवहार को रोका जा सके';
$string['pluginname'] = 'क्विल्गो परीक्षण निगरानी';
$string['privacy:export:quizaccess_quilgo_reports'] = 'क्विल्गो प्रॉक्टरिंग रिपोर्ट';
$string['privacy:export:quizaccess_quilgo_settings'] = 'क्विलगो प्रॉक्टरिंग सेटिंग्स';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'कैमरा ट्रैकिंग अक्षम की गई';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'कैमरा ट्रैकिंग सक्षम';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'बलपूर्वक ट्रैकिंग अक्षम की गई';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'बलपूर्वक ट्रैकिंग सक्षम की गई';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'स्क्रीन ट्रैकिंग अक्षम';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'स्क्रीन ट्रैकिंग सक्षम';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'यह प्लगइन प्रॉक्टरिंग रिपोर्ट के लिए क्विलगो को बाहरी रूप से डेटा भेजता है।';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'प्रश्नोत्तरी प्रयास के दौरान विंडो इवेंट पर फोकस या अनफोकस करें।';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'प्रश्नोत्तरी प्रयास के दौरान कैमरे या स्क्रीन से ली गई छवियाँ।';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'उपयोगकर्ता प्रश्नोत्तरी प्रयास के प्रॉक्टरिंग सत्र के बारे में जानकारी।';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'उपयोगकर्ता प्रश्नोत्तरी प्रयास की आईडी.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'जब कोई उपयोगकर्ता प्रश्नोत्तरी का प्रयास करता है तो कैमरा ट्रैकिंग स्थिति सक्षम या अक्षम हो जाती है।';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'उपयोगकर्ता द्वारा प्रश्नोत्तरी का प्रयास समाप्त करने पर प्रॉक्टरिंग रिपोर्ट तैयार करने के दौरान त्रुटि होने पर त्रुटि विवरण।';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'जब कोई उपयोगकर्ता प्रश्नोत्तरी का प्रयास करता है तो बल ट्रैकिंग स्थिति सक्षम या अक्षम हो जाती है।';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'प्रॉक्टरिंग सत्र की आईडी.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'जब कोई उपयोगकर्ता प्रश्नोत्तरी का प्रयास करता है तो स्क्रीन ट्रैकिंग स्थिति सक्षम या अक्षम हो जाती है।';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'प्रश्नोत्तरी की प्रॉक्टरिंग सेटिंग के बारे में जानकारी.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'कैमरा ट्रैकिंग सक्षम या अक्षम है।';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'बल ट्रैकिंग सक्षम या अक्षम.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'एक प्रश्नोत्तरी की एक आईडी जो प्रॉक्टरिंग का उपयोग करती है।';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'स्क्रीन ट्रैकिंग सक्षम या अक्षम है.';
$string['proctoring_disabled_info'] = 'स्वचालित प्रॉक्टरिंग <i>अक्षम</i> है। सक्षम करने के लिए, <a href="{$a}"><u>क्विज़ सेटिंग</u></a> पर जाएँ, "प्रयासों पर अतिरिक्त प्रतिबंध" का विस्तार करें, "प्रॉक्टरिंग सक्षम करें" बॉक्स पर टिक करें, ट्रैकिंग विधियाँ चुनें, "सहेजें" पर क्लिक करें, और इस पृष्ठ पर वापस आएँ।';
$string['proctoring_setup_failed_title'] = 'प्रॉक्टरिंग सेटअप विफल';
$string['refresh_quiz_page'] = 'प्रश्नोत्तरी ताज़ा करें';
$string['report_camera_tracking'] = 'कैमरा निगरानी:';
$string['report_detail_title'] = 'निगरानी रिपोर्ट';
$string['report_empty'] = 'निगरानी अब सक्रिय है। परिणाम यहाँ दिखाई देंगे जब कम से कम एक क्विज़ प्रयास किया जाता है।';
$string['report_error'] = 'निगरानी रिपोर्ट प्राप्त करते समय कुछ गलत हो गया। कृपया बाद में पुनः प्रयास करें';
$string['report_expired'] = 'छवियों को उनकी वैधता अवधि समाप्त होने के कारण रिपोर्ट से हटा दिया गया';
$string['report_expires_in'] = 'छवियाँ {$a} दिनों में सिस्टम से हटा दी जाएंगी';
$string['report_face_presence'] = 'चेहरे की उपस्थिति:';
$string['report_focus_good'] = 'अच्छा';
$string['report_focus_not_good'] = 'परीक्षण से {$a} बार बाहर गए';
$string['report_focus_not_good_multiple'] = 'परीक्षण से {$a} बार बाहर गए';
$string['report_left_test'] = 'गतिविधि:';
$string['report_link_caption'] = 'निगरानी रिपोर्ट देखें | क्विल्गो<sup>®</sup>';
$string['report_notready'] = 'आपकी निगरानी रिपोर्ट अभी उपलब्ध नहीं है। कृपया कुछ समय और प्रतीक्षा करें।';
$string['report_patterns_detected'] = 'पैटर्न का पता चला:';
$string['report_patterns_see_answer_below'] = 'नीचे उत्तर देखें';
$string['report_patterns_step_change_answer'] = 'उत्तर बदलें';
$string['report_patterns_step_copy_question'] = 'प्रश्न कॉपी करें';
$string['report_patterns_step_leave_test'] = 'परीक्षण छोड़ें';
$string['report_patterns_step_paste_answer'] = 'उत्तर चिपकाएँ';
$string['report_patterns_step_return'] = 'वापस करना';
$string['report_patterns_used_multiple_screens'] = 'एकाधिक स्क्रीन का उपयोग किया गया:';
$string['report_preview_faces_detected'] = ' | चेहरे का पता चला: {$a}';
$string['report_preview_info'] = 'इस प्रयास का केवल <strong>पहला मिनट</strong> ट्रैक किया गया था क्योंकि यह एक <strong>प्रीव्यू अटेम्प्ट</strong> है। आपका <strong>छात्र प्रयास</strong> <strong>पूरी तरह से ट्रैक किया जाएगा</strong>';
$string['report_preview_page_unfocused'] = ' | अस्पष्ट';
$string['report_preview_time'] = 'समय:';
$string['report_preview_title'] = 'प्रीव्यू';
$string['report_proctoring_methods'] = 'निगरानी के तरीके:';
$string['report_screen_tracking'] = 'स्क्रीन निगरानी:';
$string['report_setting_camera'] = 'कैमरा';
$string['report_setting_focus'] = 'गतिविधि';
$string['report_setting_screen'] = 'स्क्रीन';
$string['report_settings_recommendation'] = 'हम बेहतर प्रॉक्टरिंग के लिए <strong>कैमरा और स्क्रीन ट्रैकिंग</strong> दोनों को सक्षम करने की अनुशंसा करते हैं। <a href="{$a}" target="_blank"><u>क्विज़ सेटिंग</u></a> पर जाएं, "प्रयासों पर अतिरिक्त प्रतिबंध" अनुभाग का विस्तार करें, ट्रैकिंग विधियों पर टिक करें और "सहेजें" पर क्लिक करें "अतिरिक्त ट्रैकिंग विधियों को सक्षम करने के लिए।';
$string['report_suspicious_caption'] = '{$a} संदिग्ध';
$string['report_suspicious_screenshots'] = 'स्क्रीनशॉट्स:';
$string['report_table_header_attempt'] = 'प्रयास #';
$string['report_table_header_attempt_results'] = 'प्रयास परिणाम';
$string['report_table_header_confidence_levels'] = 'आत्मविश्वास का स्तर बढ़ाना';
$string['report_table_header_email'] = 'ईमेल';
$string['report_table_header_name'] = 'नाम';
$string['report_table_header_proctoring_report'] = 'निगरानी रिपोर्ट';
$string['report_table_header_score'] = 'स्कोर';
$string['report_table_header_time_taken'] = 'अवधि';
$string['report_table_header_timefinish'] = 'जमा किया गया';
$string['report_table_row_confidence_level_high'] = 'उच्च';
$string['report_table_row_confidence_level_low'] = 'कम';
$string['report_table_row_confidence_level_moderate'] = 'मध्यम';
$string['report_table_row_notyetgraded'] = 'अभी तक मूल्यांकित नहीं';
$string['report_table_row_overdue'] = 'विलंबित: {$a}';
$string['report_table_row_review_attempt'] = 'प्रयास की समीक्षा करें';
$string['report_table_row_stat_loading'] = 'लोड हो रहा है...';
$string['report_table_row_stat_not_ready'] = 'कृपया एक मिनट तक प्रतीक्षा करें';
$string['report_table_row_stat_queued'] = 'कतारबद्ध';
$string['report_table_row_view_report'] = 'रिपोर्ट देखें';
$string['setting_group'] = '<strong>क्विल्गो<sup>®</sup> परीक्षण निगरानी</strong>';
$string['setup-additional-collector-description'] = 'अब कृपया अपनी स्क्रीन तक पहुंच प्रदान करें';
$string['setup-additional-collector-title'] = 'लगभग पूर्ण';
$string['setup_camera_error_desc'] = 'कैमरा निगरानी सक्रिय करने के लिए, आपको अपने कैमरे तक पहुंच की अनुमति देनी होगी। कृपया अपने कैमरे के लिए एक्सेस सेटिंग्स बदलें।';
$string['setup_camera_error_title'] = 'कैमरा एक्सेस विफल';
$string['setup_camera_hint'] = 'कैमरा';
$string['setup_connection_hint'] = 'और';
$string['setup_consent_activity_tracking_enabled'] = 'मैं प्रॉक्टरिंग डेटा को रिकॉर्ड करने, संसाधित करने और संग्रहीत करने तथा उसे अपने शिक्षक के साथ साझा करने की सहमति प्रदान करता हूं।';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'मैं अपनी स्क्रीन के स्क्रीनशॉट और स्वयं की तस्वीरों सहित प्रॉक्टरिंग डेटा को रिकॉर्ड करने, संसाधित करने और संग्रहीत करने तथा उन्हें अपने शिक्षक के साथ साझा करने की सहमति प्रदान करता हूं।';
$string['setup_consent_camera_tracking_enabled'] = 'मैं अपनी तस्वीरों सहित प्रॉक्टरिंग डेटा को रिकॉर्ड करने, संसाधित करने और संग्रहीत करने तथा उन्हें अपने शिक्षक के साथ साझा करने की सहमति प्रदान करता हूं।';
$string['setup_consent_provided_report'] = 'आपके क्विज़ प्रयास के पूरा होने के बाद आपके शिक्षक को एक रिपोर्ट प्रदर्शित की जाएगी';
$string['setup_consent_screen_tracking_enabled'] = 'मैं अपनी स्क्रीन के स्क्रीनशॉट सहित प्रॉक्टरिंग डेटा को रिकॉर्ड करने, संसाधित करने और संग्रहीत करने तथा उन्हें अपने शिक्षक के साथ साझा करने की सहमति प्रदान करता हूं।';
$string['setup_consent_snapshots_from'] = 'स्नैपशॉट्स की';
$string['setup_consent_snapshots_will_taken'] = 'आपके प्रयास के दौरान लिए जाएंगे';
$string['setup_consent_to_start_quiz'] = 'शुरू करने के लिए, आपको देनी होगी पहुंच';
$string['setup_disable_device_warning_attention'] = 'ध्यान!';
$string['setup_disable_device_warning_check_camera_only'] = 'मैं समझता हूं, मैं अपने परीक्षण के दौरान अपने कैमरे की पहुंच को अक्षम नहीं करूंगा';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'मैं समझता हूं, मैं अपने परीक्षण के दौरान अपने कैमरे या स्क्रीन तक पहुंच को अक्षम नहीं करूंगा';
$string['setup_disable_device_warning_check_screen_only'] = 'मैं समझता हूं, मैं अपने परीक्षण के दौरान अपनी स्क्रीन एक्सेस को अक्षम नहीं करूंगा';
$string['setup_disable_device_warning_description_camera_only'] = 'परीक्षण के दौरान अपना कैमरा बंद न करें क्योंकि इससे आपके परीक्षण परिणाम प्रभावित हो सकते हैं।';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'परीक्षण के दौरान अपना कैमरा या स्क्रीन बंद न करें क्योंकि इससे आपके परीक्षण परिणाम प्रभावित हो सकते हैं।';
$string['setup_disable_device_warning_description_screen_only'] = 'परीक्षण के दौरान अपनी स्क्रीन को अक्षम न करें क्योंकि इससे आपके परीक्षा परिणाम प्रभावित हो सकते हैं।';
$string['setup_finish_tick_box'] = 'सहमति देने के लिए नीचे दिए गए बॉक्स को टिक करें:';
$string['setup_finish_title'] = 'क्या आप देख रहे हैं';
$string['setup_finish_your_screen'] = 'अपनी स्क्रीन';
$string['setup_finish_yourself'] = 'अपने स्वयं को';
$string['setup_not_supported_error_desc'] = 'क्षमा करें, आपका डिवाइस कैमरा/स्क्रीन निगरानी का समर्थन नहीं करता है। कृपया क्विज़ को पूरा करने के लिए किसी अन्य डिवाइस का चयन करें';
$string['setup_not_supported_error_title'] = 'डिवाइस समर्थित नहीं है';
$string['setup_provide_access_camera'] = 'कैमरा एक्सेस की अनुमति दें';
$string['setup_provide_access_screen'] = 'स्क्रीन एक्सेस की अनुमति दें';
$string['setup_required_error'] = 'क्विज़ शुरू करने से पहले आपको सेटअप पूरा करना आवश्यक है';
$string['setup_retry'] = 'पुनः प्रयास करें';
$string['setup_screen_area_error_desc'] = 'कृपया पुनः प्रयास करें और सुनिश्चित करें कि आप <strong>पूरी स्क्रीन</strong> का चयन करें ताकि वह साझा की जा सके।';
$string['setup_screen_area_error_title'] = 'स्क्रीन क्षेत्र गलत चुना गया';
$string['setup_screen_error_desc'] = 'स्क्रीन रिकॉर्डिंग सक्रिय करने के लिए, आपको अपनी स्क्रीन तक पहुंच की अनुमति देनी होगी। कृपया सुनिश्चित करें कि आपने अपनी सिस्टम प्राथमिकताओं में स्क्रीन रिकॉर्डिंग को अस्वीकार नहीं किया है।';
$string['setup_screen_error_title'] = 'स्क्रीन एक्सेस विफल';
$string['setup_screen_hint'] = 'स्क्रीन';
$string['setup_tracking_enabled_hint'] = 'निगरानी सक्रिय';
$string['upgrade_failed_info'] = 'क्विल्गो को अपग्रेड करने का प्रयास करते समय कुछ गड़बड़ी हुई। कृपया बाद में पुनः प्रयास करें।';
