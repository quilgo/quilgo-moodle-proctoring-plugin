<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Filipino strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'May problema sa pagtatangkang irehistro ang iyong site. Pakisubukan ulit mamaya';
$string['config_register'] = 'Irehistro ang aking site';
$string['config_success_register'] = 'Matagumpay na narehistro ang iyong site';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'Ang iyong plugin ng Pagsubaybay ng Quilgo ay hindi pa aktibo. Ito ay dahil hindi matagumpay na narehistro ang iyong site sa Pagsubaybay ng Quilgo habang ikinakabit ang plugin. Maaari mong subukang magrehistro muli sa pamamagitan ng pag-click sa "Irehistro ang aking site" sa ibaba.';
$string['confirm_email_ask_me_later'] = 'Tanungin mo ako mamaya';
$string['confirm_email_button_text'] = 'Kumpirmahin ang email address';
$string['confirm_email_default_failed_message'] = 'Nagkaproblema noong sinusubukang kumpirmahin ang iyong email address. Pakisubukang muli sa ibang pagkakataon at tiyaking ginagamit mo ang wastong email address.';
$string['confirm_email_finished'] = 'Salamat sa pagkumpirma sa email address. Masiyahan sa paggamit ng Quilgo!';
$string['confirm_email_message'] = 'Pakikumpirma ang iyong kasalukuyang email address upang matiyak na makakatanggap ka ng mahalagang mga update sa seguridad at tampok.';
$string['confirm_email_short_desc'] = 'Ang Quilgo ay isang napakataas na nasusukat na plugin na sumusubaybay sa camera, screen at aktibidad, at gumagawa ng mga ulat ng proctoring sa pagkumpleto ng pagsubok.';
$string['confirm_email_title'] = 'Salamat sa pagpili kay Quilgo';
$string['create_session_error_default_description'] = 'Nabigo ang proctoring setup. Pakisubukang i-refresh ang pahina ng pagsusulit.';
$string['create_session_error_limit_reached_description'] = 'Pinagana ng iyong guro ang automated proctoring para sa pagsusulit na ito ngunit naabot na ang limitasyon. Pakisubukang muli upang simulan ang pagsusulit sa loob ng ilang minuto.';
$string['general_continue'] = 'Magpatuloy';
$string['general_yes'] = 'Oo';
$string['limit_info_contact_admin'] = 'Makipag-ugnayan sa iyong administrator ng Moodle upang mag-upgrade o makipag-ugnayan sa amin sa <strong>hello@quilgo.com</strong> kung kailangan mo ng pagsubok para sa mas malaking bilang ng mga mag-aaral.';
$string['limit_info_run_up'] = 'Nagbibigay-daan ang Quilgo Free na magpatakbo ng hanggang <strong><span class="free-limitation">0</span> na proctored na pagsubok na pagsubok nang sabay-sabay</strong>. Ang limitasyong ito ay ibinabahagi sa lahat ng pagsusulit.';
$string['limit_info_upgrade'] = 'Mag-click {$a}<strong><span class="quilgo-upgrade-cta">dito para mag-upgrade</span></strong> o makipag-ugnayan sa amin sa <strong>hello@quilgo.com</strong> kung kailangan mo ng pagsubok para sa mas malaking bilang ng mga mag-aaral.';
$string['manage_subscription_failed_info'] = 'Nagkaproblema noong subukang pamahalaan ang subscription sa Quilgo. Subukang muli mamaya.';
$string['manage_subscription_title'] = 'Pamahalaan ang aking Quilgo subscription';
$string['plasm_camera'] = 'Bantayan ang camera';
$string['plasm_camera_help'] = 'Siguraduhing hindi umaalis ang mga sumasagot mula sa kanilang upuan hanggang matapos ang quiz';
$string['plasm_enabled'] = 'Paganahin ang Proctoring';
$string['plasm_focus'] = 'Subaybayan ang aktibidad (naka-enable bilang default)';
$string['plasm_focus_help'] = 'Tingnan kung ilang beses iniwan ng mga kalahok sa pagsusulit ang quiz para sa ibang tab o aplikasyon';
$string['plasm_force'] = 'Sapilitang pagsubaybay';
$string['plasm_force_help'] = 'Nangangailangan ng napiling paraan ng pagsubaybay para subukan ang quiz';
$string['plasm_screen'] = 'Bantayan ang screen';
$string['plasm_screen_help'] = 'Awtomatikong i-record ang screen ng mga sumasagot at mga kahina-hinalang aktibidad nang may mataas na kalidad para maiwasan ang di-patas na gawi';
$string['pluginname'] = 'Pagsubaybay ng Quilgo';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Quilgo Proctoring Report';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Mga Setting ng Quilgo Proctoring';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Naka-disable ang pagsubaybay sa camera';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Pinagana ang pagsubaybay sa camera';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Na-disable ang puwersang pagsubaybay';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Pinagana ang puwersang pagsubaybay';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Hindi pinagana ang pagsubaybay sa screen';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Pinagana ang pagsubaybay sa screen';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'Ang plugin na ito ay nagpapadala ng data sa labas sa Quilgo para sa proctoring report.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Focus o unfocus window event a habang sinusubukang mag-quiz.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Mga larawang nakunan mula sa isang camera o screen sa panahon ng pagsubok sa pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Impormasyon tungkol sa proctoring session ng pagsubok sa pagsusulit ng user.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'Ang ID ng pagsubok ng pagsusulit ng user.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'Ang katayuan ng pagsubaybay sa camera ay pinagana o hindi pinagana kapag sinubukan ng isang user ang isang pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'Ang paglalarawan ng error kung mayroong error sa panahon ng pagbuo ng ulat ng proctoring kapag natapos ng user na subukan ang isang pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'Pinagana o hindi pinagana ang katayuan ng force tracking kapag sinubukan ng user ang isang pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'Ang ID ng proctoring session.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'Ang katayuan ng pagsubaybay sa screen ay pinagana o hindi pinagana kapag sinubukan ng isang user ang isang pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'Impormasyon tungkol sa mga setting ng proctoring ng pagsusulit.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'Isang ID ng isang pagsusulit na gumagamit ng proctoring.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'Pinagana o hindi pinagana ang force tracking.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'Isang ID ng isang pagsusulit na gumagamit ng proctoring.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'Pinagana o hindi pinagana ang pagsubaybay sa screen.';
$string['proctoring_disabled_info'] = 'Ang automated proctoring ay <i>naka-disable</i>. Upang paganahin, pumunta sa <a href="{$a}"><u>Mga Setting ng Pagsusulit</u></a>, palawakin ang "Mga karagdagang paghihigpit sa mga pagtatangka", lagyan ng tsek ang kahon na "Paganahin ang Proctoring," piliin ang pagsubaybay pamamaraan, i-click ang "I-save", at bumalik sa pahinang ito.';
$string['proctoring_setup_failed_title'] = 'Nabigo ang pag-setup ng proctoring';
$string['refresh_quiz_page'] = 'I-refresh ang pagsusulit';
$string['report_camera_tracking'] = 'Pagsubaybay ng camera:';
$string['report_detail_title'] = 'Ulat ng Pagsubaybay';
$string['report_empty'] = 'Ang pagsubaybay ay ngayon ay aktibo. Ang mga resulta ay lilitaw dito pagkatapos na mayroon na kahit isang pagsubok sa quiz na isinagawa.';
$string['report_error'] = 'May problema sa pagkuha ng ulat ng pagsubaybay. Pakiusap subukang muli mamaya';
$string['report_expired'] = 'Ang mga larawan ay tinanggal mula sa ulat dahil sa kanilang pag-expire';
$string['report_expires_in'] = 'Aalisin ang mga larawan sa system sa loob ng {$a} araw';
$string['report_face_presence'] = 'Presensya ng mukha:';
$string['report_focus_good'] = 'Mabuti';
$string['report_focus_not_good'] = 'Umalis sa pagsusulit ng {$a} beses';
$string['report_focus_not_good_multiple'] = 'Umalis sa pagsusulit ng {$a} beses';
$string['report_left_test'] = 'Aktibidad:';
$string['report_link_caption'] = 'Tingnan ang ulat ng pagsubaybay | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Hindi pa handa ang iyong ulat ng pagsubaybay. Pakiusap maghintay pa ng kaunti.';
$string['report_patterns_detected'] = 'Mga pattern na nakita:';
$string['report_patterns_see_answer_below'] = 'tingnan ang mga sagot sa ibaba';
$string['report_patterns_step_change_answer'] = 'Baguhin ang Sagot';
$string['report_patterns_step_copy_question'] = 'Kopyahin ang Tanong';
$string['report_patterns_step_leave_test'] = 'Umalis sa Pagsusulit';
$string['report_patterns_step_paste_answer'] = 'Idikit ang Sagot';
$string['report_patterns_step_return'] = 'Bumalik';
$string['report_patterns_used_multiple_screens'] = 'Gumamit ng maraming screen:';
$string['report_preview_faces_detected'] = ' | Mga mukhang natukoy: {$a}';
$string['report_preview_info'] = 'Tanging ang <strong>unang minuto</strong> ng pagsubok na ito ang nasubaybayan dahil ito ay isang <strong>trial preview</strong>. Ang iyong <strong>estudyante na pagsubok</strong> ay <strong>masusubaybayan nang buo</strong>';
$string['report_preview_page_unfocused'] = ' | Hindi nakapokus';
$string['report_preview_time'] = 'Oras:';
$string['report_preview_title'] = 'Preview';
$string['report_proctoring_methods'] = 'Mga paraan ng pagsubaybay:';
$string['report_screen_tracking'] = 'Pagsubaybay ng screen:';
$string['report_setting_camera'] = 'Camera';
$string['report_setting_focus'] = 'Aktibidad';
$string['report_setting_screen'] = 'Screen';
$string['report_settings_recommendation'] = 'Inirerekomenda naming i-enable ang parehong <strong>pagsubaybay sa camera at screen</strong> para sa mas mahusay na pagbabantay. Pumunta sa <a href="{$a}" target="_blank"><u>mga setting ng pagsusulit</u></a>, palawakin ang seksyong "Mga karagdagang paghihigpit sa mga pagtatangka," lagyan ng tsek ang mga paraan ng pagsubaybay at i-click ang "I-save" upang paganahin ang mga karagdagang paraan ng pagsubaybay.';
$string['report_suspicious_caption'] = '{$a} kahina-hinala';
$string['report_suspicious_screenshots'] = 'Mga screenshot:';
$string['report_table_header_attempt'] = 'Pagsubok #';
$string['report_table_header_attempt_results'] = 'Resulta ng Pagsubok';
$string['report_table_header_confidence_levels'] = 'Proctoring antas ng kumpiyansa';
$string['report_table_header_email'] = 'Email';
$string['report_table_header_name'] = 'Pangalan';
$string['report_table_header_proctoring_report'] = 'Ulat ng Pagsubaybay';
$string['report_table_header_score'] = 'Iskor';
$string['report_table_header_time_taken'] = 'Tagal';
$string['report_table_header_timefinish'] = 'Natapos';
$string['report_table_row_confidence_level_high'] = 'Mataas';
$string['report_table_row_confidence_level_low'] = 'Mababa';
$string['report_table_row_confidence_level_moderate'] = 'Katamtaman';
$string['report_table_row_notyetgraded'] = 'Hindi pa nasusuri';
$string['report_table_row_overdue'] = 'Huli na: {$a}';
$string['report_table_row_review_attempt'] = 'Repasuhin ang pagsubok';
$string['report_table_row_stat_loading'] = 'Naglo-load...';
$string['report_table_row_stat_not_ready'] = 'Mangyaring maghintay ng hanggang isang minuto';
$string['report_table_row_stat_queued'] = 'Nakapila';
$string['report_table_row_view_report'] = 'Tingnan ang ulat';
$string['setting_group'] = '<strong>Pagsubaybay ng Quilgo<sup>®</sup></strong>';
$string['setup-additional-collector-description'] = 'Ngayon, pakiusap bigyan ng access ang iyong screen';
$string['setup-additional-collector-title'] = 'Halos tapos na';
$string['setup_camera_error_desc'] = 'Para ma-activate ang pagsubaybay sa camera, kailangan mong pahintulutan ang access sa iyong camera. Pakiusap baguhin ang iyong mga setting ng access para sa camera.';
$string['setup_camera_error_title'] = 'Bigo ang access sa camera';
$string['setup_camera_hint'] = 'camera';
$string['setup_connection_hint'] = 'at';
$string['setup_consent_activity_tracking_enabled'] = 'Nagbibigay ako ng pahintulot na itala, iproseso at iimbak ang data ng proctoring at ibahagi ang mga ito sa aking guro.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'Nagbibigay ako ng pahintulot na i-record, iproseso at iimbak ang data ng proctoring, kasama ang mga screenshot ng aking screen at mga larawan ko, at ibahagi ang mga ito sa aking guro.';
$string['setup_consent_camera_tracking_enabled'] = 'Nagbibigay ako ng pahintulot na i-record, iproseso at iimbak ang data ng proctoring, kabilang ang mga larawan ko, at ibahagi ang mga ito sa aking guro.';
$string['setup_consent_provided_report'] = 'Ang ulat ay ipapakita sa iyong guro pagkatapos mong matapos ang iyong pagsusulit';
$string['setup_consent_screen_tracking_enabled'] = 'Nagbibigay ako ng pahintulot na i-record, iproseso at iimbak ang data ng proctoring, kasama ang mga screenshot ng aking screen, at ibahagi ang mga ito sa aking guro.';
$string['setup_consent_snapshots_from'] = 'Mga snapshot mula';
$string['setup_consent_snapshots_will_taken'] = 'ay kukunin habang sinusubukan mo';
$string['setup_consent_to_start_quiz'] = 'Para makapagsimula, kailangan mong magbigay ng access sa';
$string['setup_disable_device_warning_attention'] = 'Pansin!';
$string['setup_disable_device_warning_check_camera_only'] = 'Naiintindihan ko, HINDI ko idi-disable ang access ko sa camera sa panahon ng aking pagsubok';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'Naiintindihan ko, HINDI ko idi-disable ang aking camera o screen access sa panahon ng aking pagsubok';
$string['setup_disable_device_warning_check_screen_only'] = 'Naiintindihan ko, HINDI ko idi-disable ang aking screen access sa panahon ng aking pagsubok';
$string['setup_disable_device_warning_description_camera_only'] = 'HUWAG huwag paganahin ang iyong camera sa panahon ng iyong pagsubok dahil maaaring makaapekto ito sa iyong mga resulta ng pagsubok.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'HUWAG i-disable ang iyong camera o screen sa panahon ng iyong pagsubok dahil maaaring makaapekto ito sa iyong mga resulta ng pagsubok.';
$string['setup_disable_device_warning_description_screen_only'] = 'HUWAG huwag paganahin ang iyong screen sa panahon ng iyong pagsubok dahil maaaring makaapekto ito sa iyong mga resulta ng pagsubok.';
$string['setup_finish_tick_box'] = 'Lagyan ng tsek ang kahon sa ibaba para magbigay ng pahintulot:';
$string['setup_finish_title'] = 'Ano ang nakikita mo';
$string['setup_finish_your_screen'] = 'ang iyong screen';
$string['setup_finish_yourself'] = 'ang iyong sarili';
$string['setup_not_supported_error_desc'] = 'Paumanhin, hindi sinusuportahan ng iyong device ang pagsubaybay ng camera/screen. Pakiusap pumili ng ibang device para sa pagsagot sa quiz';
$string['setup_not_supported_error_title'] = 'Hindi suportado ang device';
$string['setup_provide_access_camera'] = 'Pahintulutan ang access sa camera';
$string['setup_provide_access_screen'] = 'Pahintulutan ang access sa screen';
$string['setup_required_error'] = 'Kailangan mong kumpletuhin ang setup bago magsimula ng quiz';
$string['setup_retry'] = 'Subukang muli';
$string['setup_screen_area_error_desc'] = 'Pakiusap subukang muli at tiyakin na pumili ka ng <strong>buong screen</strong> para maibahagi.';
$string['setup_screen_area_error_title'] = 'Maling pagpili ng area ng screen';
$string['setup_screen_error_desc'] = 'Para ma-activate ang pagrekord ng screen, kailangan mong pahintulutan ang access sa iyong screen. Siguraduhin din na hindi mo tinanggihan ang pagrekord ng screen sa iyong mga kagustuhan sa sistema.';
$string['setup_screen_error_title'] = 'Bigo ang access sa screen';
$string['setup_screen_hint'] = 'screen';
$string['setup_tracking_enabled_hint'] = 'pagsubaybay ay aktibo';
$string['upgrade_failed_info'] = 'Nagkaproblema noong subukang i-upgrade ang Quilgo. Subukang muli mamaya.';
