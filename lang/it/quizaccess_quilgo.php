<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Italian strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'Si è verificato un errore durante la registrazione del sito. Per favore, riprova più tardi';
$string['config_register'] = 'Registra il mio sito';
$string['config_success_register'] = 'Il tuo sito è stato registrato con successo';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'Il plugin Quilgo Proctoring non è ancora attivo perché il tuo sito non è stato registrato correttamente durante l\'installazione. Puoi riprovare facendo clic su "Registra il mio sito" qui sotto.';
$string['confirm_email_ask_me_later'] = 'Chiedimelo più tardi';
$string['confirm_email_button_text'] = 'Conferma l\'indirizzo e-mail';
$string['confirm_email_default_failed_message'] = 'Qualcosa è andato storto durante il tentativo di confermare il tuo indirizzo email. Riprova più tardi e assicurati di utilizzare un indirizzo email valido.';
$string['confirm_email_finished'] = 'Grazie per aver confermato l\'indirizzo email. Divertiti a usare Quilgo!';
$string['confirm_email_message'] = 'Ti preghiamo di confermare il tuo indirizzo email attuale per assicurarti di ricevere importanti aggiornamenti sulla sicurezza e sulle funzionalità.';
$string['confirm_email_short_desc'] = 'Quilgo è un plugin altamente scalabile che tiene traccia della telecamera, dello schermo e dell\'attività e produce report di controllo al termine dei test.';
$string['confirm_email_title'] = 'Grazie per aver scelto Quilgo';
$string['create_session_error_default_description'] = 'La configurazione della supervisione non è riuscita. Prova ad aggiornare la pagina del quiz.';
$string['create_session_error_limit_reached_description'] = 'Il tuo insegnante ha abilitato il controllo automatizzato per questo quiz ma è stato raggiunto il limite. Riprova ad avviare il quiz tra qualche minuto.';
$string['general_continue'] = 'Continuare';
$string['general_yes'] = 'Sì';
$string['limit_info_contact_admin'] = 'Contatta il tuo amministratore Moodle per eseguire l\'aggiornamento o contattaci a <strong>hello@quilgo.com</strong> se hai bisogno di una prova per un numero maggiore di studenti.';
$string['limit_info_run_up'] = 'Quilgo Free consente di eseguire fino a <strong><span class="free-limitation">0</span> tentativi di test controllati simultaneamente</strong>. Questo limite è condiviso tra tutti i quiz.';
$string['limit_info_upgrade'] = 'Fai clic {$a}<strong><span class="quilgo-upgrade-cta">qui per aggiornare</span></strong> o contattaci a <strong>hello@quilgo.com</strong> se hai bisogno di una prova per un numero maggiore di studenti.';
$string['manage_subscription_failed_info'] = 'Qualcosa è andato storto durante il tentativo di gestire l\'abbonamento a Quilgo. Per favore riprova più tardi.';
$string['manage_subscription_title'] = 'Gestisci il mio abbonamento Quilgo';
$string['plasm_camera'] = 'Traccia fotocamera';
$string['plasm_camera_help'] = 'Assicurati che il candidato sia la persona corretta e che non lasci il proprio posto fino al termine del quiz';
$string['plasm_enabled'] = 'Abilita supervisione';
$string['plasm_focus'] = 'Tieni traccia dell\'attività (abilitato per impostazione predefinita)';
$string['plasm_focus_help'] = 'Vedi quante volte un candidato ha lasciato il quiz per un\'altra scheda o app';
$string['plasm_force'] = 'Forza tracciamento';
$string['plasm_force_help'] = 'Richiedi le modalità di tracciamento selezionate per avviare il quiz';
$string['plasm_screen'] = 'Traccia schermo';
$string['plasm_screen_help'] = 'Registra automaticamente gli schermi dei tuoi partecipanti e le attività sospette in alta qualità per scoraggiare comportamenti scorretti';
$string['pluginname'] = 'Quilgo Proctoring';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Rapporto di sorveglianza Quilgo';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Impostazioni di sorveglianza Quilgo';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Tracciamento fotocamera disabilitato';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Tracciamento fotocamera abilitato';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Tracciamento forzato disabilitato';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Tracciamento forzato abilitato';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Tracciamento schermo disabilitato';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Tracciamento schermo abilitato';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'Questo plugin invia dati esternamente a Quilgo per il rapporto di sorveglianza.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Evento finestra focalizzata o non focalizzata durante un tentativo di quiz.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Immagini acquisite da una fotocamera o da uno schermo durante un tentativo di quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Informazioni sulla sessione di sorveglianza del tentativo del quiz dell\'utente.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'ID del tentativo di quiz dell\'utente.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'Lo stato del tracciamento della fotocamera è attivo o inattivo quando un utente tenta un quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'Descrizione dell\'errore se si verifica un problema durante la generazione del rapporto di sorveglianza quando l\'utente termina il tentativo di quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'Lo stato del tracciamento forzato è attivo o inattivo quando un utente tenta un quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'ID della sessione di sorveglianza.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'Lo stato del tracciamento dello schermo è attivo o inattivo quando un utente tenta un quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'Informazioni sulle impostazioni di sorveglianza del quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'Lo stato del tracciamento della fotocamera è abilitato o disabilitato.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'Lo stato del tracciamento forzato è abilitato o disabilitato.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'ID di un quiz che utilizza la sorveglianza.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'Lo stato del tracciamento dello schermo è abilitato o disabilitato.';
$string['proctoring_disabled_info'] = 'Il controllo automatizzato è <i>disabilitato</i>. Per abilitare, vai su <a href="{$a}"><u>Impostazioni quiz</u></a>, espandi "Restrizioni aggiuntive sui tentativi", seleziona la casella "Abilita supervisione", scegli il monitoraggio metodi, fare clic su "Salva" e tornare a questa pagina.';
$string['proctoring_setup_failed_title'] = 'Configurazione della supervisione non riuscita';
$string['refresh_quiz_page'] = 'Aggiorna il quiz';
$string['report_camera_tracking'] = 'Tracciamento della fotocamera:';
$string['report_detail_title'] = 'Rapporto di proctoring';
$string['report_empty'] = 'Il proctoring è ora abilitato. I risultati appariranno qui una volta effettuato almeno un tentativo di quiz.';
$string['report_error'] = 'Si è verificato un problema durante il recupero del rapporto di proctoring. Per favore, riprova più tardi';
$string['report_expired'] = 'Le immagini sono state rimosse dal rapporto a causa della loro scadenza';
$string['report_expires_in'] = 'Le immagini verranno rimosse dal sistema in {$a} giorni';
$string['report_face_presence'] = 'Presenza del volto:';
$string['report_focus_good'] = 'Buono';
$string['report_focus_not_good'] = 'Test abbandonato {$a} volta';
$string['report_focus_not_good_multiple'] = 'Test abbandonato {$a} volte';
$string['report_left_test'] = 'Attività:';
$string['report_link_caption'] = 'Visualizza i rapporti di proctoring | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Il rapporto di proctoring non è ancora pronto. Attendere un momento';
$string['report_patterns_detected'] = 'Schemi rilevati:';
$string['report_patterns_see_answer_below'] = 'vedi le risposte qui sotto';
$string['report_patterns_step_change_answer'] = 'Cambia risposta';
$string['report_patterns_step_copy_question'] = 'Copia domanda';
$string['report_patterns_step_leave_test'] = 'Lascia test';
$string['report_patterns_step_paste_answer'] = 'Incolla risposta';
$string['report_patterns_step_return'] = 'Ritorna';
$string['report_patterns_used_multiple_screens'] = 'Usati più schermi:';
$string['report_preview_faces_detected'] = ' | Volti rilevati: {$a}';
$string['report_preview_info'] = 'Solo il <strong>primo minuto</strong> di questo tentativo è stato tracciato perché si tratta di un <strong>tentativo di anteprima</strong>. I <strong>tentativi degli studenti</strong> saranno <strong>completamente tracciati</strong>';
$string['report_preview_page_unfocused'] = ' | Pagina non focalizzata';
$string['report_preview_time'] = 'Tempo:';
$string['report_preview_title'] = 'Anteprima';
$string['report_proctoring_methods'] = 'Metodi di proctoring:';
$string['report_screen_tracking'] = 'Tracciamento dello schermo:';
$string['report_setting_camera'] = 'Fotocamera';
$string['report_setting_focus'] = 'Attività';
$string['report_setting_screen'] = 'Schermo';
$string['report_settings_recommendation'] = 'Consigliamo di abilitare sia il <strong>tracciamento della fotocamera che dello schermo</strong> per un migliore monitoraggio. Vai a <a href="{$a}" target="_blank"><u>impostazioni del quiz</u></a>, espandi la sezione "Restrizioni aggiuntive sui tentativi", spunta i metodi di tracciamento e fai clic su "Salva" per abilitare i metodi di tracciamento aggiuntivi.';
$string['report_suspicious_caption'] = '{$a} sospetti';
$string['report_suspicious_screenshots'] = 'Screenshot:';
$string['report_table_header_attempt'] = 'Tentativo #';
$string['report_table_header_attempt_results'] = 'Risultati del tentativo';
$string['report_table_header_confidence_levels'] = 'Livello di fiducia nel proctoring';
$string['report_table_header_email'] = 'E-mail';
$string['report_table_header_name'] = 'Nome';
$string['report_table_header_proctoring_report'] = 'Rapporto di proctoring';
$string['report_table_header_score'] = 'Punteggio';
$string['report_table_header_time_taken'] = 'Durata';
$string['report_table_header_timefinish'] = 'Inviato';
$string['report_table_row_confidence_level_high'] = 'Alto';
$string['report_table_row_confidence_level_low'] = 'Basso';
$string['report_table_row_confidence_level_moderate'] = 'Moderato';
$string['report_table_row_notyetgraded'] = 'Non ancora valutato';
$string['report_table_row_overdue'] = 'In ritardo: {$a}';
$string['report_table_row_review_attempt'] = 'Rivedi tentativo';
$string['report_table_row_stat_loading'] = 'Caricamento...';
$string['report_table_row_stat_not_ready'] = 'Attendi fino a un minuto';
$string['report_table_row_stat_queued'] = 'In coda';
$string['report_table_row_view_report'] = 'Visualizza rapporto';
$string['setting_group'] = '<strong>Quilgo<sup>®</sup> Proctoring</strong>';
$string['setup-additional-collector-description'] = 'Ora consenti l\'accesso allo schermo';
$string['setup-additional-collector-title'] = 'Quasi finito';
$string['setup_camera_error_desc'] = 'Per attivare il tracciamento della fotocamera, devi consentire l\'accesso alla fotocamera. Modifica le impostazioni di accesso per la tua fotocamera.';
$string['setup_camera_error_title'] = 'Accesso alla fotocamera non riuscito';
$string['setup_camera_hint'] = 'fotocamera';
$string['setup_connection_hint'] = 'e';
$string['setup_consent_activity_tracking_enabled'] = 'Fornisco il consenso a registrare, elaborare e archiviare i dati di supervisione e condividerli con il mio insegnante.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'Fornisco il consenso a registrare, elaborare e archiviare i dati di supervisione, inclusi screenshot del mio schermo e foto di me stesso, e condividerli con il mio insegnante.';
$string['setup_consent_camera_tracking_enabled'] = 'Fornisco il consenso a registrare, elaborare e archiviare i dati di supervisione, comprese le mie foto, e a condividerli con il mio insegnante.';
$string['setup_consent_provided_report'] = 'Un rapporto verrà mostrato al tuo insegnante una volta completato il tentativo di quiz';
$string['setup_consent_screen_tracking_enabled'] = 'Fornisco il consenso a registrare, elaborare e archiviare i dati di supervisione, inclusi gli screenshot del mio schermo, e a condividerli con il mio insegnante.';
$string['setup_consent_snapshots_from'] = 'Le istantanee da';
$string['setup_consent_snapshots_will_taken'] = 'verranno scattate durante il tentativo';
$string['setup_consent_to_start_quiz'] = 'Per iniziare un tentativo, devi consentire l\'accesso a';
$string['setup_disable_device_warning_attention'] = 'Attenzione!';
$string['setup_disable_device_warning_check_camera_only'] = 'Capisco, NON disabiliterò l\'accesso alla mia fotocamera durante il test';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'Capisco, NON disabiliterò l\'accesso alla mia fotocamera o allo schermo durante il mio test';
$string['setup_disable_device_warning_check_screen_only'] = 'Capisco, NON disabiliterò l\'accesso allo schermo durante il test';
$string['setup_disable_device_warning_description_camera_only'] = 'NON disattivare la fotocamera durante il test poiché ciò potrebbe influenzare i risultati del test.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'NON disattivare la fotocamera o lo schermo durante il test poiché ciò potrebbe influenzare i risultati.';
$string['setup_disable_device_warning_description_screen_only'] = 'NON disattivare lo schermo durante il test poiché ciò potrebbe influenzare i risultati.';
$string['setup_finish_tick_box'] = 'Spunta la casella sottostante per fornire il consenso:';
$string['setup_finish_title'] = 'Vedi';
$string['setup_finish_your_screen'] = 'il tuo schermo';
$string['setup_finish_yourself'] = 'te stesso';
$string['setup_not_supported_error_desc'] = 'Spiacenti, il tuo dispositivo non supporta il tracciamento di fotocamera/schermo. Scegli un altro dispositivo per sostenere il quiz.';
$string['setup_not_supported_error_title'] = 'Dispositivo non supportato';
$string['setup_provide_access_camera'] = 'Consenti accesso alla fotocamera';
$string['setup_provide_access_screen'] = 'Consenti accesso allo schermo';
$string['setup_required_error'] = 'Devi completare la configurazione prima di iniziare il quiz';
$string['setup_retry'] = 'Riprova';
$string['setup_screen_area_error_desc'] = 'Per favore, riprova e assicurati di selezionare l\'<strong>intero schermo</strong> da condividere.';
$string['setup_screen_area_error_title'] = 'Area dello schermo selezionata errata';
$string['setup_screen_error_desc'] = 'Per attivare la registrazione dello schermo, devi consentire l\'accesso allo schermo. Assicurati inoltre di non aver rifiutato la registrazione dello schermo nelle preferenze del sistema.';
$string['setup_screen_error_title'] = 'Accesso allo schermo non riuscito';
$string['setup_screen_hint'] = 'schermo';
$string['setup_tracking_enabled_hint'] = 'il tracciamento è abilitato';
$string['upgrade_failed_info'] = 'Qualcosa è andato storto durante il tentativo di aggiornare Quilgo. Per favore riprova più tardi.';
