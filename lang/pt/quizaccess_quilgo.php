<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Portugese strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'Houve um problema ao tentar registrar o seu site. Por favor, tente novamente mais tarde';
$string['config_register'] = 'Registrar meu site';
$string['config_success_register'] = 'Seu site foi registrado com sucesso';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'Seu plugin de Supervisão Quilgo ainda não está ativo. Isso ocorre porque o seu site não foi registrado com sucesso na Supervisão Quilgo durante a instalação do plugin. Você pode tentar registrar novamente clicando em "Registrar meu site" abaixo.';
$string['confirm_email_ask_me_later'] = 'Pergunte-me mais tarde';
$string['confirm_email_button_text'] = 'Confirmar endereço de e-mail';
$string['confirm_email_default_failed_message'] = 'Algo correu mal ao tentar confirmar o seu endereço de e-mail. Tente novamente mais tarde e certifique-se de que utiliza um endereço de e-mail válido.';
$string['confirm_email_finished'] = 'Obrigado por confirmar o endereço de e-mail. Divirta-se a usar o Quilgo!';
$string['confirm_email_message'] = 'Confirme o seu endereço de e-mail atual para garantir que recebe atualizações importantes de segurança e de características.';
$string['confirm_email_short_desc'] = 'O Quilgo é um plugin altamente escalável que rastreia a câmara, o ecrã e a atividade, e produz relatórios de supervisão na conclusão do teste.';
$string['confirm_email_title'] = 'Obrigado por escolher Quilgo';
$string['create_session_error_default_description'] = 'A configuração de supervisão falhou. Por favor, tente atualizar a página do questionário.';
$string['create_session_error_limit_reached_description'] = 'Seu professor ativou a supervisão automatizada para este questionário, mas o limite foi atingido. Tente novamente para iniciar o teste em alguns minutos.';
$string['general_continue'] = 'Continuar';
$string['general_yes'] = 'Sim';
$string['limit_info_contact_admin'] = 'Entre em contato com o administrador do Moodle para atualizar ou entre em contato conosco pelo e-mail <strong>hello@quilgo.com</strong> se precisar de uma avaliação para um número maior de alunos.';
$string['limit_info_run_up'] = 'Quilgo Free permite executar até <strong><span class="free-limitation">0</span> tentativas de teste supervisionadas simultaneamente</strong>. Este limite é compartilhado por todos os questionários.';
$string['limit_info_upgrade'] = 'Clique {$a}<strong><span class="quilgo-upgrade-cta">aqui para atualizar</span></strong> ou entre em contato conosco em <strong>hello@quilgo.com</strong> se precisar de uma avaliação para um número maior de alunos.';
$string['manage_subscription_failed_info'] = 'Algo deu errado ao tentar gerenciar a assinatura do Quilgo. Por favor, tente novamente mais tarde.';
$string['manage_subscription_title'] = 'Gerenciar minha assinatura Quilgo';
$string['plasm_camera'] = 'Monitorar câmera';
$string['plasm_camera_help'] = 'Certifique-se de que os respondentes não deixem seus assentos até o quiz ser concluído';
$string['plasm_enabled'] = 'Habilitar supervisão';
$string['plasm_focus'] = 'Rastrear atividade (ativado por padrão)';
$string['plasm_focus_help'] = 'Veja quantas vezes os participantes do teste saem do quiz para outra aba ou aplicativo';
$string['plasm_force'] = 'Supervisão obrigatória';
$string['plasm_force_help'] = 'Exige métodos de supervisão selecionados para tentar o quiz';
$string['plasm_screen'] = 'Monitorar tela';
$string['plasm_screen_help'] = 'Registre automaticamente a tela dos respondentes e atividades suspeitas com alta qualidade para prevenir comportamentos injustos';
$string['pluginname'] = 'Supervisão Quilgo';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Relatório de supervisão Quilgo';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Configurações de supervisão do Quilgo';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Rastreamento de câmera desativado';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Rastreamento de câmera ativado';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Forçar rastreamento desativado';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Forçar rastreamento ativado';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Rastreamento de tela desativado';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Rastreamento de tela ativado';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'Este plugin envia dados externamente ao Quilgo para relatório de supervisão.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Focar ou desfocar o evento da janela a durante a tentativa de teste.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Imagens capturadas de uma câmera ou tela durante uma tentativa de teste.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Informações sobre a sessão de supervisão da tentativa de teste do usuário.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'O ID da tentativa de teste do usuário.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'O status de rastreamento da câmera é ativado ou desativado quando um usuário responde a um teste.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'A descrição do erro se houver um erro durante a geração de um relatório de supervisão quando o usuário terminar a tentativa de um teste.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'O status de rastreamento forçado ativado ou desativado quando um usuário tenta responder um teste.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'O ID da sessão de supervisão.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'O status de rastreamento de tela ativado ou desativado quando um usuário tenta responder um teste.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'O rastreamento da câmera está ativado ou desativado.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'O rastreamento da câmera está ativado ou desativado.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'O rastreamento de força ativado ou desativado.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'Um ID de um questionário que usa supervisão.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'O rastreamento de tela ativado ou desativado.';
$string['proctoring_disabled_info'] = 'A supervisão automatizada está <i>desativada</i>. Para ativar, acesse <a href="{$a}"><u>Configurações do questionário</u></a>, expanda "Restrições extras de tentativas", marque a caixa "Ativar supervisão", escolha o rastreamento métodos, clique em "Salvar" e retorne a esta página.';
$string['proctoring_setup_failed_title'] = 'Falha na configuração de supervisão';
$string['refresh_quiz_page'] = 'Atualizar questionário';
$string['report_camera_tracking'] = 'Supervisão da câmera:';
$string['report_detail_title'] = 'Relatório de Supervisão';
$string['report_empty'] = 'A supervisão agora está ativa. Os resultados aparecerão aqui depois que pelo menos uma tentativa de quiz for realizada.';
$string['report_error'] = 'Houve um erro ao obter o relatório de supervisão. Por favor, tente novamente mais tarde';
$string['report_expired'] = 'As imagens foram removidas do relatório devido à sua expiração';
$string['report_expires_in'] = 'As imagens serão removidas do sistema em {$a} dias';
$string['report_face_presence'] = 'Presença facial:';
$string['report_focus_good'] = 'Bom';
$string['report_focus_not_good'] = 'Deixou o teste {$a} vezes';
$string['report_focus_not_good_multiple'] = 'Deixou o teste {$a} vezes';
$string['report_left_test'] = 'Atividade:';
$string['report_link_caption'] = 'Ver relatório de supervisão | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Seu relatório de supervisão ainda não está disponível. Por favor, aguarde mais um pouco.';
$string['report_patterns_detected'] = 'Padrões detectados:';
$string['report_patterns_see_answer_below'] = 'veja as respostas abaixo';
$string['report_patterns_step_change_answer'] = 'Alterar resposta';
$string['report_patterns_step_copy_question'] = 'Copiar pergunta';
$string['report_patterns_step_leave_test'] = 'Sair do teste';
$string['report_patterns_step_paste_answer'] = 'Colar resposta';
$string['report_patterns_step_return'] = 'Retornar';
$string['report_patterns_used_multiple_screens'] = 'Várias telas usadas:';
$string['report_preview_faces_detected'] = ' | Rostos detectados: {$a}';
$string['report_preview_info'] = 'Apenas o <strong>primeiro minuto</strong> desta tentativa foi rastreado, pois é um <strong>teste de pré-visualização</strong>. Seu <strong>teste de aluno</strong> será <strong>totalmente rastreado</strong>';
$string['report_preview_page_unfocused'] = ' | Desfocado';
$string['report_preview_time'] = 'Tempo:';
$string['report_preview_title'] = 'Pré-visualização';
$string['report_proctoring_methods'] = 'Métodos de supervisão:';
$string['report_screen_tracking'] = 'Supervisão da tela:';
$string['report_setting_camera'] = 'Câmera';
$string['report_setting_focus'] = 'Atividade';
$string['report_setting_screen'] = 'Tela';
$string['report_settings_recommendation'] = 'Recomendamos ativar o <strong>rastreamento da câmera e da tela</strong> para uma melhor supervisão. Acesse <a href="{$a}" target="_blank"><u>configurações do questionário</u></a>, expanda a seção "Restrições extras às tentativas", marque os métodos de rastreamento e clique em "Salvar" para ativar métodos de rastreamento adicionais.';
$string['report_suspicious_caption'] = '{$a} suspeito';
$string['report_suspicious_screenshots'] = 'Capturas de tela suspeitas:';
$string['report_table_header_attempt'] = 'Tentativa #';
$string['report_table_header_attempt_results'] = 'Resultados da Tentativa';
$string['report_table_header_confidence_levels'] = 'Nível de confiança de supervisão';
$string['report_table_header_email'] = 'Email';
$string['report_table_header_name'] = 'Nome';
$string['report_table_header_proctoring_report'] = 'Relatório de Supervisão';
$string['report_table_header_score'] = 'Pontuação';
$string['report_table_header_time_taken'] = 'Duração';
$string['report_table_header_timefinish'] = 'Concluído';
$string['report_table_row_confidence_level_high'] = 'Alta';
$string['report_table_row_confidence_level_low'] = 'Baixo';
$string['report_table_row_confidence_level_moderate'] = 'Moderada';
$string['report_table_row_notyetgraded'] = 'Ainda não avaliado';
$string['report_table_row_overdue'] = 'Atrasado: {$a}';
$string['report_table_row_review_attempt'] = 'Revisar tentativa';
$string['report_table_row_stat_loading'] = 'Carregando...';
$string['report_table_row_stat_not_ready'] = 'Aguarde até um minuto';
$string['report_table_row_stat_queued'] = 'Enfileiradas';
$string['report_table_row_view_report'] = 'Ver relatório';
$string['setting_group'] = '<strong>Supervisão Quilgo<sup>®</sup></strong>';
$string['setup-additional-collector-description'] = 'Agora, por favor, conceda acesso à sua tela';
$string['setup-additional-collector-title'] = 'Quase terminando';
$string['setup_camera_error_desc'] = 'Para ativar a supervisão da câmera, você precisa permitir acesso à sua câmera. Por favor, altere as configurações de acesso para sua câmera.';
$string['setup_camera_error_title'] = 'Falha no acesso à câmera';
$string['setup_camera_hint'] = 'câmera';
$string['setup_connection_hint'] = 'e';
$string['setup_consent_activity_tracking_enabled'] = 'Dou consentimento para registrar, processar e armazenar os dados de supervisão e compartilhá-los com meu professor.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'Dou consentimento para registrar, processar e armazenar os dados de supervisão, incluindo capturas de tela da minha tela e fotos minhas, e compartilhá-los com meu professor.';
$string['setup_consent_camera_tracking_enabled'] = 'Dou consentimento para registrar, processar e armazenar os dados de supervisão, incluindo fotos minhas, e compartilhá-los com meu professor.';
$string['setup_consent_provided_report'] = 'Um relatório será exibido para o seu professor após você concluir sua tentativa de quiz';
$string['setup_consent_screen_tracking_enabled'] = 'Dou consentimento para registrar, processar e armazenar os dados de supervisão, incluindo capturas de tela da minha tela, e compartilhá-los com meu professor.';
$string['setup_consent_snapshots_from'] = 'Capturas de tela de';
$string['setup_consent_snapshots_will_taken'] = 'serão tiradas durante a sua tentativa';
$string['setup_consent_to_start_quiz'] = 'Para começar, você precisa conceder acesso a';
$string['setup_disable_device_warning_attention'] = 'Atenção!';
$string['setup_disable_device_warning_check_camera_only'] = 'Compreendo, NÃO irei desativar o acesso à minha câmara durante o teste';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'Compreendo que NÃO desativarei a minha câmara ou o acesso ao ecrã durante o meu teste';
$string['setup_disable_device_warning_check_screen_only'] = 'Compreendo, NÃO desativarei o meu acesso ao ecrã durante o teste';
$string['setup_disable_device_warning_description_camera_only'] = 'NÃO desative a sua câmara durante o teste, pois pode afetar os resultados do teste.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'NÃO desative a sua câmara ou ecrã durante o teste, pois pode afetar os resultados do teste.';
$string['setup_disable_device_warning_description_screen_only'] = 'NÃO desative o seu ecrã durante o teste, pois pode afetar os resultados do teste.';
$string['setup_finish_tick_box'] = 'Marque a caixa abaixo para dar seu consentimento:';
$string['setup_finish_title'] = 'O que você está vendo';
$string['setup_finish_your_screen'] = 'sua tela';
$string['setup_finish_yourself'] = 'você mesmo';
$string['setup_not_supported_error_desc'] = 'Desculpe, seu dispositivo não suporta a supervisão de câmera/tela. Por favor, escolha outro dispositivo para realizar o quiz';
$string['setup_not_supported_error_title'] = 'Dispositivo não suportado';
$string['setup_provide_access_camera'] = 'Permitir acesso à câmera';
$string['setup_provide_access_screen'] = 'Permitir acesso à tela';
$string['setup_required_error'] = 'Você deve completar a configuração antes de iniciar o quiz';
$string['setup_retry'] = 'Tentar novamente';
$string['setup_screen_area_error_desc'] = 'Por favor, tente novamente e certifique-se de selecionar <strong>toda a tela</strong> para compartilhar.';
$string['setup_screen_area_error_title'] = 'Área da tela selecionada incorretamente';
$string['setup_screen_error_desc'] = 'Para ativar a gravação da tela, você precisa permitir acesso à sua tela. Certifique-se também de que não recusou a gravação de tela nas suas preferências de sistema.';
$string['setup_screen_error_title'] = 'Falha no acesso à tela';
$string['setup_screen_hint'] = 'tela';
$string['setup_tracking_enabled_hint'] = 'supervisão ativa';
$string['upgrade_failed_info'] = 'Algo deu errado ao tentar atualizar o Quilgo. Por favor, tente novamente mais tarde.';
