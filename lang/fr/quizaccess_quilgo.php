<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * French strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'Une erreur s\'est produite lors de la tentative d\'enregistrement de votre site. Veuillez réessayer plus tard';
$string['config_register'] = 'Enregistrer mon site';
$string['config_success_register'] = 'Votre site a été enregistré avec succès';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'Votre plugin de surveillance Quilgo n\'est pas encore actif. Cela est dû au fait que votre site n\'a pas été correctement enregistré auprès de Quilgo lors de l\'installation du plugin. Vous pouvez réessayer l\'inscription en cliquant sur "Enregistrer mon site" ci-dessous.';
$string['confirm_email_ask_me_later'] = 'Demande-moi plus tard';
$string['confirm_email_button_text'] = 'Confirmez votre adresse email';
$string['confirm_email_default_failed_message'] = 'Une erreur s\'est produite lors de la confirmation de votre adresse e-mail. Veuillez réessayer ultérieurement et assurez-vous d\'utiliser une adresse e-mail valide.';
$string['confirm_email_finished'] = 'Merci d\'avoir confirmé l\'adresse e-mail. Bonne utilisation de Quilgo!';
$string['confirm_email_message'] = 'Veuillez confirmer votre adresse e-mail actuelle pour vous assurer de recevoir des mises à jour importantes en matière de sécurité et de fonctionnalités.';
$string['confirm_email_short_desc'] = 'Quilgo est un plugin hautement évolutif qui suit la caméra, l\'écran et l\'activité, et produit des rapports de surveillance à la fin des tests.';
$string['confirm_email_title'] = 'Merci d\'avoir choisi Quilgo';
$string['create_session_error_default_description'] = 'La configuration de la surveillance a échoué. Veuillez essayer d\'actualiser la page du quiz.';
$string['create_session_error_limit_reached_description'] = 'Votre enseignant a activé la surveillance automatisée pour ce quiz, mais la limite a été atteinte. Veuillez réessayer pour démarrer le quiz dans quelques minutes.';
$string['general_continue'] = 'Continuer';
$string['general_yes'] = 'Oui';
$string['limit_info_contact_admin'] = 'Contactez votre administrateur Moodle pour mettre à niveau ou contactez-nous à <strong>hello@quilgo.com</strong> si vous avez besoin d\'un essai pour un plus grand nombre d\'étudiants.';
$string['limit_info_run_up'] = 'Quilgo Free permet d\'exécuter jusqu\'à <strong><span class="free-limitation">0</span> tentatives de tests surveillés simultanément</strong>. Cette limite est partagée entre tous les quiz.';
$string['limit_info_upgrade'] = 'Cliquez {$a}<strong><span class="quilgo-upgrade-cta">ici pour mettre à niveau</span></strong> ou contactez-nous à <strong>hello@quilgo.com</strong> si vous avez besoin d\'un essai. pour un plus grand nombre d\'étudiants.';
$string['manage_subscription_failed_info'] = 'Une erreur s\'est produite lors de la tentative de gestion de l\'abonnement Quilgo. Veuillez réessayer plus tard.';
$string['manage_subscription_title'] = 'Gérer mon abonnement Quilgo';
$string['plasm_camera'] = 'Suivre la caméra';
$string['plasm_camera_help'] = 'Assurez-vous que le candidat est bien la personne qui devrait l\'être et qu\'il ne quitte pas son siège avant la fin du quiz';
$string['plasm_enabled'] = 'Activer la surveillance';
$string['plasm_focus'] = 'Suivre l\'activité (activé par défaut)';
$string['plasm_focus_help'] = 'Voir combien de fois un candidat a quitté le quiz pour un autre onglet ou une autre application';
$string['plasm_force'] = 'Forcer le suivi';
$string['plasm_force_help'] = 'Exiger les méthodes de suivi sélectionnées pour passer le quiz';
$string['plasm_screen'] = 'Suivre l\'écran';
$string['plasm_screen_help'] = 'Enregistrez automatiquement les écrans de vos répondants et leurs activités suspectes en haute qualité pour décourager tout comportement déloyal';
$string['pluginname'] = 'Quilgo Proctoring';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Rapport de surveillance Quilgo';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Paramètres de surveillance Quilgo';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Suivi de la caméra désactivé';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Suivi de la caméra activé';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Suivi forcé désactivé';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Suivi forcé activé';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Suivi de l\'écran désactivé';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Suivi de l\'écran activé';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'Ce plugin envoie des données externes à Quilgo pour le rapport de surveillance.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Événement fenêtre focalisée ou non focalisée pendant une tentative de quiz.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Images capturées par une caméra ou un écran pendant une tentative de quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Informations sur la session de surveillance de la tentative de quiz de l\'utilisateur.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'L\'ID de la tentative de quiz de l\'utilisateur.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'Le statut du suivi de la caméra activé ou désactivé lorsqu\'un utilisateur tente un quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'La description de l\'erreur s\'il y a une erreur lors de la génération du rapport de surveillance lorsque l\'utilisateur termine un quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'Le statut du suivi forcé activé ou désactivé lorsqu\'un utilisateur tente un quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'L\'ID de la session de surveillance.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'Le statut du suivi de l\'écran activé ou désactivé lorsqu\'un utilisateur tente un quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'Informations sur les paramètres de surveillance du quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'Le suivi de la caméra est activé ou désactivé.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'Le suivi forcé est activé ou désactivé.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'Un ID d\'un quiz qui utilise la surveillance.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'Le suivi de l\'écran est activé ou désactivé.';
$string['proctoring_disabled_info'] = 'La surveillance automatisée est <i>désactivée</i>. Pour l\'activer, accédez à <a href="{$a}"><u>Paramètres du quiz</u></a>, développez les "Restrictions supplémentaires sur les tentatives", cochez la case "Activer la surveillance", choisissez le suivi. méthodes, cliquez sur "Enregistrer" et revenez à cette page.';
$string['proctoring_setup_failed_title'] = 'Échec de la configuration de la surveillance';
$string['refresh_quiz_page'] = 'Actualiser le quiz';
$string['report_camera_tracking'] = 'Suivi de la caméra:';
$string['report_detail_title'] = 'Rapport de surveillance';
$string['report_empty'] = 'La surveillance est maintenant activée. Les résultats apparaîtront ici une fois qu\'au moins une tentative de quiz aura été effectuée.';
$string['report_error'] = 'Une erreur s\'est produite lors de l\'obtention du rapport de surveillance. Veuillez réessayer plus tard';
$string['report_expired'] = 'Les images ont été supprimées du rapport en raison de leur expiration';
$string['report_expires_in'] = 'Les images seront supprimées du système dans {$a} jours';
$string['report_face_presence'] = 'Présence du visage:';
$string['report_focus_good'] = 'Bien';
$string['report_focus_not_good'] = 'Test quitté {$a} fois';
$string['report_focus_not_good_multiple'] = 'Test quitté {$a} fois';
$string['report_left_test'] = 'Activité:';
$string['report_link_caption'] = 'Voir les rapports de surveillance | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Votre rapport de surveillance n\'est pas encore prêt. Veuillez patienter un instant';
$string['report_patterns_detected'] = 'Modèles détectés:';
$string['report_patterns_see_answer_below'] = 'voir les réponses ci-dessous';
$string['report_patterns_step_change_answer'] = 'Changer la réponse';
$string['report_patterns_step_copy_question'] = 'Copier la question';
$string['report_patterns_step_leave_test'] = 'Quitter le test';
$string['report_patterns_step_paste_answer'] = 'Coller la réponse';
$string['report_patterns_step_return'] = 'Revenir';
$string['report_patterns_used_multiple_screens'] = 'Utilisé plusieurs écrans:';
$string['report_preview_faces_detected'] = ' | Visages détectés: {$a}';
$string['report_preview_info'] = 'Seule la <strong>première minute</strong> de cette tentative a été suivie car il s\'agit d\'une <strong>tentative d\'aperçu</strong>. Les <strong>tentatives de vos élèves</strong> seront <strong>complètement suivies</strong>';
$string['report_preview_page_unfocused'] = ' | Page non focalisée';
$string['report_preview_time'] = 'Temps:';
$string['report_preview_title'] = 'Aperçu';
$string['report_proctoring_methods'] = 'Méthodes de surveillance:';
$string['report_screen_tracking'] = 'Suivi de l\'écran:';
$string['report_setting_camera'] = 'Caméra';
$string['report_setting_focus'] = 'Activité';
$string['report_setting_screen'] = 'Écran';
$string['report_settings_recommendation'] = 'Nous recommandons d\'activer à la fois le <strong>suivi de la caméra et de l\'écran</strong> pour une meilleure surveillance. Allez dans <a href="{$a}" target="_blank"><u>les paramètres du quiz</u></a>, développez la section "Restrictions supplémentaires sur les tentatives", cochez les méthodes de suivi et cliquez sur "Enregistrer" pour activer des méthodes de suivi supplémentaires.';
$string['report_suspicious_caption'] = '{$a} suspect';
$string['report_suspicious_screenshots'] = 'Captures d\'écran:';
$string['report_table_header_attempt'] = 'Tentative #';
$string['report_table_header_attempt_results'] = 'Résultats de la tentative';
$string['report_table_header_confidence_levels'] = 'Niveau de confiance de surveillance';
$string['report_table_header_email'] = 'E-mail';
$string['report_table_header_name'] = 'Nom';
$string['report_table_header_proctoring_report'] = 'Rapport de surveillance';
$string['report_table_header_score'] = 'Score';
$string['report_table_header_time_taken'] = 'Durée';
$string['report_table_header_timefinish'] = 'Soumis';
$string['report_table_row_confidence_level_high'] = 'Élevé';
$string['report_table_row_confidence_level_low'] = 'Faible';
$string['report_table_row_confidence_level_moderate'] = 'Modéré';
$string['report_table_row_notyetgraded'] = 'Pas encore noté';
$string['report_table_row_overdue'] = 'En retard: {$a}';
$string['report_table_row_review_attempt'] = 'Réviser la tentative';
$string['report_table_row_stat_loading'] = 'Chargement...';
$string['report_table_row_stat_not_ready'] = 'Veuillez patienter une minute';
$string['report_table_row_stat_queued'] = 'En file d\'attente';
$string['report_table_row_view_report'] = 'Voir le rapport';
$string['setting_group'] = '<strong>Quilgo<sup>®</sup> Proctoring</strong>';
$string['setup-additional-collector-description'] = 'Veuillez maintenant donner accès à votre écran';
$string['setup-additional-collector-title'] = 'Presque terminé';
$string['setup_camera_error_desc'] = 'Pour activer le suivi de la caméra, vous devez autoriser l\'accès à votre caméra. Veuillez modifier les paramètres d\'accès pour votre caméra.';
$string['setup_camera_error_title'] = 'Échec de l\'accès à la caméra';
$string['setup_camera_hint'] = 'caméra';
$string['setup_connection_hint'] = 'et';
$string['setup_consent_activity_tracking_enabled'] = 'J\'accepte d\'enregistrer, de traiter et de stocker les données de surveillance et de les partager avec mon professeur.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'J\'autorise à enregistrer, traiter et stocker les données de surveillance, y compris les captures d\'écran de mon écran et les photos de moi-même, et à les partager avec mon professeur.';
$string['setup_consent_camera_tracking_enabled'] = 'J\'autorise à enregistrer, traiter et stocker les données de surveillance, y compris les photos de moi-même, et à les partager avec mon professeur.';
$string['setup_consent_provided_report'] = 'Un rapport sera remis à votre enseignant une fois que vous aurez terminé votre tentative de quiz';
$string['setup_consent_screen_tracking_enabled'] = 'J\'accepte d\'enregistrer, de traiter et de stocker les données de surveillance, y compris les captures d\'écran de mon écran, et de les partager avec mon professeur.';
$string['setup_consent_snapshots_from'] = 'Des instantanés de votre';
$string['setup_consent_snapshots_will_taken'] = 'seront pris lors de votre tentative';
$string['setup_consent_to_start_quiz'] = 'Pour commencer un essai, vous devez autoriser l\'accès à votre';
$string['setup_disable_device_warning_attention'] = 'Attention!';
$string['setup_disable_device_warning_check_camera_only'] = 'Je comprends, je ne désactiverai PAS l\'accès à ma caméra pendant mon test';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'Je comprends, je ne désactiverai PAS l\'accès à ma caméra ou à mon écran pendant mon test';
$string['setup_disable_device_warning_check_screen_only'] = 'Je comprends, je ne désactiverai PAS l\'accès à mon écran pendant mon test';
$string['setup_disable_device_warning_description_camera_only'] = 'NE DÉSACTIVEZ PAS votre caméra pendant votre test car cela pourrait affecter vos résultats de test.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'NE DÉSACTIVEZ PAS votre caméra ou votre écran pendant votre test car cela pourrait affecter vos résultats de test.';
$string['setup_disable_device_warning_description_screen_only'] = 'NE DÉSACTIVEZ PAS votre écran pendant votre test car cela pourrait affecter vos résultats de test.';
$string['setup_finish_tick_box'] = 'Cochez la case ci-dessous pour donner votre consentement:';
$string['setup_finish_title'] = 'Voyez-vous';
$string['setup_finish_your_screen'] = 'votre écran';
$string['setup_finish_yourself'] = 'vous-même';
$string['setup_not_supported_error_desc'] = 'Désolé, votre appareil ne prend pas en charge le suivi de la caméra/l\'écran. Veuillez choisir un autre appareil pour passer le quiz.';
$string['setup_not_supported_error_title'] = 'Appareil non pris en charge';
$string['setup_provide_access_camera'] = 'Autoriser l\'accès à la caméra';
$string['setup_provide_access_screen'] = 'Autoriser l\'accès à l\'écran';
$string['setup_required_error'] = 'Vous devez terminer l\'installation avant de commencer le quiz';
$string['setup_retry'] = 'Réessayer';
$string['setup_screen_area_error_desc'] = 'Veuillez réessayer et assurez-vous de choisir <strong>l\'ensemble de l\'écran</strong> pour le partage.';
$string['setup_screen_area_error_title'] = 'Zone d\'écran incorrecte sélectionnée';
$string['setup_screen_error_desc'] = 'Pour activer l\'enregistrement d\'écran, vous devez autoriser l\'accès à votre écran. Assurez-vous également de ne pas avoir refusé l\'enregistrement d\'écran dans vos préférences système.';
$string['setup_screen_error_title'] = 'Échec de l\'accès à l\'écran';
$string['setup_screen_hint'] = 'écran';
$string['setup_tracking_enabled_hint'] = 'le suivi est activé';
$string['upgrade_failed_info'] = 'Une erreur s\'est produite lors de la tentative de mise à niveau de Quilgo. Veuillez réessayer plus tard.';
