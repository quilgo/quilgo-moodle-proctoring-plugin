<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Bengali strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'আপনার সাইট নিবন্ধনের চেষ্টা করার সময় একটি সমস্যা ঘটেছে। দয়া করে পরে আবার চেষ্টা করুন';
$string['config_register'] = 'আমার সাইট নিবন্ধন করুন';
$string['config_success_register'] = 'আপনার সাইটটি সফলভাবে নিবন্ধিত হয়েছে';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'আপনার কুইলগো প্রটরিং প্লাগইন এখনও সক্রিয় নয়। এটি হয়েছে কারণ আপনার সাইটটি প্লাগইন ইনস্টলেশনের সময় কুইলগো প্রটরিং-এ সফলভাবে নিবন্ধিত হয়নি। আপনি "আমার সাইট নিবন্ধন করুন" নীচে ক্লিক করে পুনরায় নিবন্ধনের চেষ্টা করতে পারেন।';
$string['confirm_email_ask_me_later'] = 'পরে জিজ্ঞাসা করো।';
$string['confirm_email_button_text'] = 'ইমেল ঠিকানা নিশ্চিত করুন';
$string['confirm_email_default_failed_message'] = 'আপনার ইমেল ঠিকানা নিশ্চিত করার চেষ্টা করার সময় কিছু সমস্যা হয়েছে। অনুগ্রহ করে পরে আবার চেষ্টা করুন এবং নিশ্চিত করুন যে আপনি বৈধ ইমেল ঠিকানাটি ব্যবহার করছেন।';
$string['confirm_email_finished'] = 'ইমেল ঠিকানা নিশ্চিত করার জন্য ধন্যবাদ। Quilgo ব্যবহার করে উপভোগ করুন!';
$string['confirm_email_message'] = 'গুরুত্বপূর্ণ নিরাপত্তা এবং বৈশিষ্ট্য আপডেটগুলি পেতে আপনার বর্তমান ইমেল ঠিকানাটি নিশ্চিত করুন।';
$string['confirm_email_short_desc'] = 'কুইলগো একটি অত্যন্ত স্কেলযোগ্য প্লাগইন যা ক্যামেরা, স্ক্রিন এবং কার্যকলাপ ট্র্যাক করে এবং পরীক্ষা সমাপ্তির উপর প্রক্টরিং রিপোর্ট তৈরি করে।';
$string['confirm_email_title'] = 'কুইলগো বেছে নেওয়ার জন্য আপনাকে ধন্যবাদ';
$string['create_session_error_default_description'] = 'প্রক্টরিং সেটআপ ব্যর্থ হয়েছে৷';
$string['create_session_error_limit_reached_description'] = 'আপনার শিক্ষক এই কুইজের জন্য স্বয়ংক্রিয় প্রক্টরিং সক্ষম করেছেন কিন্তু সীমা পৌঁছে গেছে। অনুগ্রহ করে কয়েক মিনিটের মধ্যে কুইজ শুরু করার জন্য আবার চেষ্টা করুন।';
$string['general_continue'] = 'চালিয়ে যান';
$string['general_yes'] = 'হ্যাঁ';
$string['limit_info_contact_admin'] = 'আপগ্রেড করতে আপনার মুডল অ্যাডমিনিস্ট্রেটরের সাথে যোগাযোগ করুন বা আপনার যদি বেশি সংখ্যক ছাত্রের জন্য ট্রায়ালের প্রয়োজন হয় তাহলে <strong>hello@quilgo.com</strong>-এ আমাদের সাথে যোগাযোগ করুন।';
$string['limit_info_run_up'] = 'Quilgo Free একযোগে <strong><span class="free-limitation">0</span> প্রক্টরযুক্ত পরীক্ষার প্রচেষ্টা চালানোর অনুমতি দেয়</strong>৷ এই সীমা সমস্ত কুইজ জুড়ে ভাগ করা হয়.';
$string['limit_info_upgrade'] = '{$a}<strong><span class="quilgo-upgrade-cta">আপগ্রেড করতে এখানে</span></strong> ক্লিক করুন অথবা আপনার যদি ট্রায়ালের প্রয়োজন হয় তাহলে <strong>hello@quilgo.com</strong> এ আমাদের সাথে যোগাযোগ করুন বৃহত্তর সংখ্যক ছাত্রদের জন্য।';
$string['manage_subscription_failed_info'] = 'Quilgo সদস্যতা ম্যানেজ করার সময় কিছু ভুল হয়েছে। অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন.';
$string['manage_subscription_title'] = 'আমার Quilgo সদস্যতা পরিচালনা করুন';
$string['plasm_camera'] = 'ক্যামেরা নিরীক্ষণ';
$string['plasm_camera_help'] = 'নিশ্চিত করুন যে উত্তরদাতারা কুইজ শেষ না হওয়া পর্যন্ত তাদের আসন ছেড়ে যায় না';
$string['plasm_enabled'] = 'প্রক্টরিং সক্ষম করুন';
$string['plasm_focus'] = 'ট্র্যাক কার্যকলাপ (ডিফল্টরূপে সক্ষম)';
$string['plasm_focus_help'] = 'দেখুন পরীক্ষার্থীরা কতবার অন্য ট্যাব বা অ্যাপ্লিকেশনে কুইজ থেকে চলে যায়';
$string['plasm_force'] = 'বাধ্যতামূলক নিরীক্ষণ';
$string['plasm_force_help'] = 'কুইজ চেষ্টা করার জন্য নির্বাচিত নিরীক্ষণ পদ্ধতি প্রয়োজন';
$string['plasm_screen'] = 'স্ক্রীন নিরীক্ষণ';
$string['plasm_screen_help'] = 'উত্তরদাতাদের স্ক্রীন এবং সন্দেহজনক কার্যক্রম উচ্চ মানের সাথে স্বয়ংক্রিয়ভাবে রেকর্ড করুন অন্যায় আচরণ প্রতিরোধের জন্য';
$string['pluginname'] = 'কুইলগো প্রটরিং';
$string['privacy:export:quizaccess_quilgo_reports'] = 'কুইলগো প্রক্টরিং রিপোর্ট';
$string['privacy:export:quizaccess_quilgo_settings'] = 'কুইলগো প্রক্টরিং সেটিংস';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'ক্যামেরা ট্র্যাকিং অক্ষম';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'ক্যামেরা ট্র্যাকিং সক্ষম';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'ফোর্স ট্র্যাকিং অক্ষম';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'ফোর্স ট্র্যাকিং সক্ষম';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'স্ক্রীন ট্র্যাকিং অক্ষম';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'স্ক্রীন ট্র্যাকিং সক্ষম';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'এই প্লাগইন প্রক্টরিং রিপোর্টের জন্য কুইলগোতে বাহ্যিকভাবে ডেটা পাঠায়।';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'ক্যুইজ প্রচেষ্টা চলাকালীন উইন্ডো ইভেন্টকে ফোকাস বা আনফোকাস করুন।';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'একটি ক্যুইজ প্রচেষ্টার সময় ক্যামেরা বা স্ক্রীন থেকে ধারণ করা ছবি।';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'ব্যবহারকারী কুইজ প্রচেষ্টার proctoring সেশন সম্পর্কে তথ্য.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'ব্যবহারকারী কুইজ প্রচেষ্টার আইডি.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'যখন একজন ব্যবহারকারী কুইজ করার চেষ্টা করেন তখন ক্যামেরা ট্র্যাকিং স্ট্যাটাস সক্ষম বা অক্ষম করা হয়।';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'ব্যবহারকারী যখন কুইজ শেষ করার চেষ্টা করে তখন প্রক্টরিং রিপোর্ট তৈরির সময় ত্রুটির বিবরণ।';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'যখন একজন ব্যবহারকারী কুইজ করার চেষ্টা করে তখন জোর করে ট্র্যাক করার স্থিতি সক্রিয় বা অক্ষম করা হয়।';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'প্রক্টরিং সেশনের আইডি।';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'স্ক্রীন ট্র্যাকিং স্থিতি সক্রিয় বা অক্ষম করা হয় যখন একজন ব্যবহারকারী একটি কুইজ চেষ্টা করে।';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'কুইজের প্রক্টরিং সেটিংস সম্পর্কে তথ্য।';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'ক্যামেরা ট্র্যাকিং সক্ষম বা অক্ষম।';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'বল ট্র্যাকিং সক্ষম বা নিষ্ক্রিয়.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'একটি ক্যুইজের একটি আইডি যা প্রক্টরিং ব্যবহার করে।';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'স্ক্রিন ট্র্যাকিং সক্ষম বা অক্ষম।';
$string['proctoring_disabled_info'] = 'স্বয়ংক্রিয় প্রক্টরিং <i>অক্ষম</i>। সক্ষম করতে, <a href="{$a}"><u>কুইজ সেটিংস</u></a> এ যান, "প্রচেষ্টার উপর অতিরিক্ত নিষেধাজ্ঞাগুলি প্রসারিত করুন", "প্রক্টরিং সক্ষম করুন" বাক্সে টিক দিন, ট্র্যাকিং চয়ন করুন পদ্ধতিতে, "সংরক্ষণ করুন" এ ক্লিক করুন এবং এই পৃষ্ঠায় ফিরে যান।';
$string['proctoring_setup_failed_title'] = 'প্রক্টরিং সেটআপ ব্যর্থ হয়েছে৷';
$string['refresh_quiz_page'] = 'কুইজ রিফ্রেশ করুন';
$string['report_camera_tracking'] = 'ক্যামেরা ট্র্যাকিং:';
$string['report_detail_title'] = 'প্রটরিং প্রতিবেদন';
$string['report_empty'] = 'নিরীক্ষণ এখন সক্রিয় আছে। ফলাফল এখানে প্রদর্শিত হবে যখন অন্তত একটি কুইজ প্রচেষ্টা সম্পন্ন হবে।';
$string['report_error'] = 'প্রটরিং প্রতিবেদন পাওয়ার সময় একটি সমস্যা ঘটেছে। অনুগ্রহ করে পরে আবার চেষ্টা করুন';
$string['report_expired'] = 'ছবিগুলো তাদের মেয়াদ শেষ হওয়ার কারণে প্রতিবেদন থেকে মুছে ফেলা হয়েছে';
$string['report_expires_in'] = 'ছবিগুলি {$a} দিনের মধ্যে সিস্টেম থেকে সরানো হবে৷';
$string['report_face_presence'] = 'মুখের উপস্থিতি:';
$string['report_focus_good'] = 'ভালো';
$string['report_focus_not_good'] = 'পরীক্ষা থেকে {$a} বার বেরিয়ে গেছে';
$string['report_focus_not_good_multiple'] = 'বাম পরীক্ষা {$a} বার';
$string['report_left_test'] = 'কার্যকলাপ:';
$string['report_link_caption'] = 'প্রটরিং প্রতিবেদন দেখুন | কুইলগো<sup>®</sup>';
$string['report_notready'] = 'আপনার প্রটরিং প্রতিবেদন এখনও প্রস্তুত নয়। দয়া করে আরেকটু অপেক্ষা করুন।';
$string['report_patterns_detected'] = 'নিদর্শন সনাক্ত করা হয়েছে:';
$string['report_patterns_see_answer_below'] = 'নিচে উত্তর দেখুন';
$string['report_patterns_step_change_answer'] = 'উত্তর পরিবর্তন করুন';
$string['report_patterns_step_copy_question'] = 'কপি প্রশ্ন';
$string['report_patterns_step_leave_test'] = 'পরীক্ষা ছেড়ে দিন';
$string['report_patterns_step_paste_answer'] = 'উত্তর পেস্ট করুন';
$string['report_patterns_step_return'] = 'প্রত্যাবর্তন';
$string['report_patterns_used_multiple_screens'] = 'একাধিক পর্দা ব্যবহার করা হয়েছে:';
$string['report_preview_faces_detected'] = ' | সনাক্তকৃত মুখগুলি: {$a}';
$string['report_preview_info'] = "এই প্রচেষ্টার <strong>প্রথম মিনিট</strong> শুধুমাত্র ট্র্যাক করা হয়েছিল কারণ এটি একটি <strong>প্রাক-দর্শনী প্রচেষ্টা</strong>। আপনার <strong>ছাত্র প্রচেষ্টা</strong> <strong>সম্পূর্ণরূপে ট্র্যাক করা হবে</strong>";
$string['report_preview_page_unfocused'] = ' | মনোযোগ নেই';
$string['report_preview_time'] = 'সময়:';
$string['report_preview_title'] = 'প্রাক-দর্শন';
$string['report_proctoring_methods'] = 'নিরীক্ষণ পদ্ধতি:';
$string['report_screen_tracking'] = 'স্ক্রীন ট্র্যাকিং:';
$string['report_setting_camera'] = 'ক্যামেরা';
$string['report_setting_focus'] = 'কার্যকলাপ';
$string['report_setting_screen'] = 'স্ক্রীন';
$string['report_settings_recommendation'] = 'আমরা ভাল প্রক্টরিংয়ের জন্য <strong>ক্যামেরা এবং স্ক্রিন ট্র্যাকিং</strong> উভয়ই সক্ষম করার পরামর্শ দিই। <a href="{$a}" target="_blank"><u>কুইজ সেটিংস</u></a>-এ যান, "প্রচেষ্টার উপর অতিরিক্ত সীমাবদ্ধতা" বিভাগটি প্রসারিত করুন, ট্র্যাকিং পদ্ধতিতে টিক দিন এবং "সংরক্ষণ করুন" এ ক্লিক করুন অতিরিক্ত ট্র্যাকিং পদ্ধতি সক্রিয় করতে।';
$string['report_suspicious_caption'] = '{$a} সন্দেহজনক';
$string['report_suspicious_screenshots'] = 'সন্দেহজনক স্ক্রীনশট:';
$string['report_table_header_attempt'] = 'প্রচেষ্টা #';
$string['report_table_header_attempt_results'] = 'প্রচেষ্টার ফলাফল';
$string['report_table_header_confidence_levels'] = 'প্রক্টরিং আত্মবিশ্বাসের স্তর';
$string['report_table_header_email'] = 'ইমেল';
$string['report_table_header_name'] = 'নাম';
$string['report_table_header_proctoring_report'] = 'প্রটরিং প্রতিবেদন';
$string['report_table_header_score'] = 'স্কোর';
$string['report_table_header_time_taken'] = 'সময় লাগেছে';
$string['report_table_header_timefinish'] = 'সমাপ্ত';
$string['report_table_row_confidence_level_high'] = 'উচ্চ';
$string['report_table_row_confidence_level_low'] = 'কম';
$string['report_table_row_confidence_level_moderate'] = 'পরিমিত';
$string['report_table_row_notyetgraded'] = 'এখনও গ্রেড দেওয়া হয়নি';
$string['report_table_row_overdue'] = 'বিলম্বিত: {$a}';
$string['report_table_row_review_attempt'] = 'প্রচেষ্টা পর্যালোচনা করুন';
$string['report_table_row_stat_loading'] = 'লোড হচ্ছে...';
$string['report_table_row_stat_not_ready'] = 'অনুগ্রহ করে এক মিনিট পর্যন্ত অপেক্ষা করুন';
$string['report_table_row_stat_queued'] = 'সারিবদ্ধ';
$string['report_table_row_view_report'] = 'প্রতিবেদন দেখুন';
$string['setting_group'] = '<strong>কুইলগো<sup>®</sup> প্রটরিং</strong>';
$string['setup-additional-collector-description'] = 'এখন আপনার স্ক্রীনে অ্যাক্সেস প্রদান করুন';
$string['setup-additional-collector-title'] = 'প্রায় সম্পন্ন';
$string['setup_camera_error_desc'] = 'ক্যামেরা নিরীক্ষণ সক্রিয় করতে, আপনাকে আপনার ক্যামেরায় অ্যাক্সেস অনুমতি দিতে হবে। আপনার ক্যামেরার জন্য অ্যাক্সেস সেটিংস পরিবর্তন করুন।';
$string['setup_camera_error_title'] = 'ক্যামেরা অ্যাক্সেস ব্যর্থ';
$string['setup_camera_hint'] = 'ক্যামেরা';
$string['setup_connection_hint'] = 'এবং';
$string['setup_consent_activity_tracking_enabled'] = 'আমি প্রক্টরিং ডেটা রেকর্ড, প্রক্রিয়া এবং সংরক্ষণ করার জন্য সম্মতি দিই এবং সেগুলি আমার শিক্ষকের সাথে শেয়ার করি।';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'আমি আমার স্ক্রীনের স্ক্রিনশট এবং নিজের ফটো সহ প্রক্টরিং ডেটা রেকর্ড, প্রক্রিয়া এবং সঞ্চয় করার জন্য সম্মতি প্রদান করি এবং সেগুলি আমার শিক্ষকের সাথে শেয়ার করি।';
$string['setup_consent_camera_tracking_enabled'] = 'আমি আমার ফটো সহ প্রক্টরিং ডেটা রেকর্ড, প্রক্রিয়া এবং সংরক্ষণ করার সম্মতি প্রদান করি এবং সেগুলি আমার শিক্ষকের সাথে শেয়ার করি।';
$string['setup_consent_provided_report'] = 'আপনি আপনার কুইজ প্রচেষ্টা শেষ করার পর আপনার শিক্ষককে একটি প্রতিবেদন দেখানো হবে';
$string['setup_consent_screen_tracking_enabled'] = 'আমি আমার স্ক্রীনের স্ক্রিনশট সহ প্রক্টরিং ডেটা রেকর্ড, প্রক্রিয়া এবং সঞ্চয় করার জন্য সম্মতি প্রদান করি এবং সেগুলি আমার শিক্ষকের সাথে শেয়ার করি।';
$string['setup_consent_snapshots_from'] = 'স্ন্যাপশট থেকে';
$string['setup_consent_snapshots_will_taken'] = 'আপনার চেষ্টার সময় নেওয়া হবে';
$string['setup_consent_to_start_quiz'] = 'শুরু করতে, আপনাকে অনুমতি দিতে হবে';
$string['setup_disable_device_warning_attention'] = 'মনোযোগ!';
$string['setup_disable_device_warning_check_camera_only'] = 'আমি বুঝতে পারছি, আমার পরীক্ষার সময় আমি আমার ক্যামেরা অ্যাক্সেস অক্ষম করব না';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'আমি বুঝি, আমার পরীক্ষার সময় আমি আমার ক্যামেরা বা স্ক্রীন অ্যাক্সেস অক্ষম করব না';
$string['setup_disable_device_warning_check_screen_only'] = 'আমি বুঝতে পেরেছি, আমি আমার পরীক্ষার সময় আমার স্ক্রিন অ্যাক্সেস অক্ষম করব না';
$string['setup_disable_device_warning_description_camera_only'] = 'আপনার পরীক্ষার সময় আপনার ক্যামেরা অক্ষম করবেন না কারণ এটি আপনার পরীক্ষার ফলাফলকে প্রভাবিত করতে পারে।';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'আপনার পরীক্ষার সময় আপনার ক্যামেরা বা স্ক্রীন অক্ষম করবেন না কারণ এটি আপনার পরীক্ষার ফলাফলকে প্রভাবিত করতে পারে।';
$string['setup_disable_device_warning_description_screen_only'] = 'আপনার পরীক্ষার সময় আপনার স্ক্রীন অক্ষম করবেন না কারণ এটি আপনার পরীক্ষার ফলাফলকে প্রভাবিত করতে পারে।';
$string['setup_finish_tick_box'] = 'সম্মতি প্রদানের জন্য নিচের বাক্সটি চেক করুন:';
$string['setup_finish_title'] = 'আপনি কি দেখছেন';
$string['setup_finish_your_screen'] = 'আপনার স্ক্রীন';
$string['setup_finish_yourself'] = 'নিজেকে';
$string['setup_not_supported_error_desc'] = 'দুঃখিত, আপনার ডিভাইস ক্যামেরা/স্ক্রীন নিরীক্ষণ সমর্থন করে না। অনুগ্রহ করে কুইজ সম্পন্ন করার জন্য অন্য ডিভাইস নির্বাচন করুন।';
$string['setup_not_supported_error_title'] = 'ডিভাইস সমর্থিত নয়';
$string['setup_provide_access_camera'] = 'ক্যামেরা অ্যাক্সেস অনুমতি দিন';
$string['setup_provide_access_screen'] = 'স্ক্রীন অ্যাক্সেস অনুমতি দিন';
$string['setup_required_error'] = 'আপনাকে কুইজ শুরু করার আগে সেটআপ সম্পন্ন করতে হবে';
$string['setup_retry'] = 'আবার চেষ্টা করুন';
$string['setup_screen_area_error_desc'] = 'দয়া করে আবার চেষ্টা করুন এবং নিশ্চিত করুন যে আপনি <strong>পুরো স্ক্রীন</strong> শেয়ার করার জন্য নির্বাচন করেছেন।';
$string['setup_screen_area_error_title'] = 'স্ক্রীনের এলাকা ভুলভাবে নির্বাচিত';
$string['setup_screen_error_desc'] = 'স্ক্রীন রেকর্ডিং সক্রিয় করতে, আপনাকে আপনার স্ক্রীনে অ্যাক্সেস অনুমতি দিতে হবে। নিশ্চিত করুন যে আপনি আপনার সিস্টেমের পছন্দসমূহে স্ক্রীন রেকর্ডিং প্রত্যাখ্যান করেন নি।';
$string['setup_screen_error_title'] = 'স্ক্রীন অ্যাক্সেস ব্যর্থ';
$string['setup_screen_hint'] = 'স্ক্রীন';
$string['setup_tracking_enabled_hint'] = 'নিরীক্ষণ সক্রিয়';
$string['upgrade_failed_info'] = 'Quilgo আপগ্রেড করার চেষ্টা করার সময় কিছু ভুল হয়েছে। অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন.';
