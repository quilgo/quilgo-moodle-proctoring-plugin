<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * English strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'Something went wrong when try to register your site. Please try again later';
$string['config_register'] = 'Register my site';
$string['config_success_register'] = 'Your site successfully registered';
$string['config_title'] = 'Quilgo Proctoring';
$string['config_warning_not_active_yet'] = 'Your Quilgo Proctoring plugin is not active yet. This because your site is failed registered to Quilgo Proctoring during plugin installation. You can retry registration with click "Register my site" below.';
$string['confirm_email_ask_me_later'] = 'Ask me later';
$string['confirm_email_button_text'] = 'Confirm email address';
$string['confirm_email_default_failed_message'] = 'Something went wrong when trying to confirm your email address. Please try again later and make sure you use the valid email address.';
$string['confirm_email_finished'] = 'Thanks for confirming the email address. Enjoy using Quilgo!';
$string['confirm_email_message'] = 'Please confirm your current email address to ensure you receive important security and feature updates.';
$string['confirm_email_short_desc'] = 'Quilgo is a highly scalable plugin that tracks camera, screen and activity, and produces proctoring reports on test completion.';
$string['confirm_email_title'] = 'Thank you for choosing Quilgo';
$string['create_session_error_default_description'] = 'Proctoring setup has failed. Please try to refresh quiz page.';
$string['create_session_error_limit_reached_description'] = 'Your teacher has enabled automated proctoring for this quiz but the limit has reached. Please try again to start the quiz in a few minutes.';
$string['general_continue'] = 'Continue';
$string['general_yes'] = 'Yes';
$string['limit_info_contact_admin'] = 'Contact your Moodle administrator to upgrade or contact us at <strong>hello@quilgo.com</strong> if you need a trial for a larger number of students.';
$string['limit_info_run_up'] = 'Quilgo Free allows to run up to <strong><span class="free-limitation">0</span> proctored test attempts simultaneously</strong>. This limit is shared across all quizzes.';
$string['limit_info_upgrade'] = 'Click {$a}<strong><span class="quilgo-upgrade-cta">here to upgrade</span></strong> or contact us at <strong>hello@quilgo.com</strong> if you need a trial for a larger number of students.';
$string['manage_subscription_failed_info'] = 'Something went wrong when try to manage Quilgo subscription. Please try again later.';
$string['manage_subscription_title'] = 'Manage my Quilgo subscription';
$string['plasm_camera'] = 'Track camera';
$string['plasm_camera_help'] = 'Make sure the respondent is the one who it should be does not leave their seat until the quiz is complete';
$string['plasm_enabled'] = 'Enable Proctoring';
$string['plasm_focus'] = 'Track activity (enabled by default)';
$string['plasm_focus_help'] = 'See how many times a test taker has left the quiz for another tab or app';
$string['plasm_force'] = 'Force tracking';
$string['plasm_force_help'] = 'Require selected methods of tracking to attempt the quiz';
$string['plasm_screen'] = 'Track screen';
$string['plasm_screen_help'] = "Auto-record your respondents' screens and suspicious activity in high quality to discourage unfair behavior";
$string['pluginname'] = 'Quilgo Proctoring';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Quilgo Proctoring Report';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Quilgo Proctoring Settings';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Camera tracking disabled';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Camera tracking enabled';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Force tracking disabled';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Force tracking enabled';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Screen tracking disabled';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Screen tracking enabled';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'This plugin sends data externally to Quilgo for proctoring report.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Focus or unfocus window event a during quiz attempt.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Images captured from a camera or screen during a quiz attempt.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Information about proctoring session of the user quiz attempt.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'The ID of the user quiz attempt.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'The camera tracking status enabled or disabled when a user attempts a quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'The error description if there is error during a proctoring report generation when user finish attempt a quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'The force tracking status enabled or disabled when a user attempts a quiz.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'The ID of proctoring session.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'The screen tracking status enabled or disabled when a user attempts a quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'Information about proctoring settings of the quiz.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'The camera tracking enabled or disabled.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'The force tracking enabled or disabled.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'An ID of a quiz that uses proctoring.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'The screen tracking enabled or disabled.';
$string['proctoring_disabled_info'] = 'Automated proctoring is <i>disabled</i>. To enable, go to <a href="{$a}"><u>Quiz Settings</u></a>, expand the "Extra restrictions on attempts", tick the "Enable Proctoring" box, choose the tracking methods, click "Save", and return to this page.';
$string['proctoring_setup_failed_title'] = 'Proctoring setup failed';
$string['refresh_quiz_page'] = 'Refresh quiz';
$string['report_camera_tracking'] = 'Camera tracking:';
$string['report_detail_title'] = 'Proctoring Report';
$string['report_empty'] = 'Proctoring is now enabled. The results will appear here once at least one quiz attempt is made.';
$string['report_error'] = 'Something went wrong when getting proctoring report. Please try again later';
$string['report_expired'] = 'The images were removed from the report due to their expiration';
$string['report_expires_in'] = 'Images will be removed from the system in {$a} days';
$string['report_face_presence'] = 'Face presence:';
$string['report_focus_good'] = 'Good';
$string['report_focus_not_good'] = 'Left test {$a} time';
$string['report_focus_not_good_multiple'] = 'Left test {$a} times';
$string['report_left_test'] = 'Activity:';
$string['report_link_caption'] = 'View proctoring reports | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Your proctoring report is not ready yet. Please wait a moment';
$string['report_patterns_detected'] = 'Patterns detected:';
$string['report_patterns_see_answer_below'] = 'see answers below';
$string['report_patterns_step_change_answer'] = 'Change Answer';
$string['report_patterns_step_copy_question'] = 'Copy Question';
$string['report_patterns_step_leave_test'] = 'Leave Test';
$string['report_patterns_step_paste_answer'] = 'Paste Answer';
$string['report_patterns_step_return'] = 'Return';
$string['report_patterns_used_multiple_screens'] = 'Used multiple screens:';
$string['report_preview_faces_detected'] = ' | Faces detected: {$a}';
$string['report_preview_info'] = "Only the <strong>first minute</strong> of this attempt has been tracked because this is a <strong>preview attempt</strong>. Your <strong>students' attempts</strong> will be <strong>fully tracked</strong>";
$string['report_preview_page_unfocused'] = ' | Page unfocused';
$string['report_preview_time'] = 'Time:';
$string['report_preview_title'] = 'Preview';
$string['report_proctoring_methods'] = 'Proctoring methods:';
$string['report_screen_tracking'] = 'Screen tracking:';
$string['report_setting_camera'] = 'Camera';
$string['report_setting_focus'] = 'Activity';
$string['report_setting_screen'] = 'Screen';
$string['report_settings_recommendation'] = 'We recommend enabling both <strong>camera and screen tracking</strong> for better proctoring. Go to <a href="{$a}" target="_blank"><u>quiz settings</u></a>, expand the "Extra restrictions on attempts" section, tick the tracking methods and click "Save" to enable additional tracking methods.';
$string['report_suspicious_caption'] = '{$a} suspicious';
$string['report_suspicious_screenshots'] = 'Screenshots:';
$string['report_table_header_attempt'] = 'Attempt #';
$string['report_table_header_attempt_results'] = 'Attempt Results';
$string['report_table_header_confidence_levels'] = 'Proctoring confidence level';
$string['report_table_header_email'] = 'Email';
$string['report_table_header_name'] = 'Name';
$string['report_table_header_proctoring_report'] = 'Proctoring report';
$string['report_table_header_score'] = 'Score';
$string['report_table_header_time_taken'] = 'Duration';
$string['report_table_header_timefinish'] = 'Submitted';
$string['report_table_row_confidence_level_high'] = 'High';
$string['report_table_row_confidence_level_low'] = 'Low';
$string['report_table_row_confidence_level_moderate'] = 'Moderate';
$string['report_table_row_notyetgraded'] = 'Not yet graded';
$string['report_table_row_overdue'] = 'Overdue: {$a}';
$string['report_table_row_review_attempt'] = 'Review attempt';
$string['report_table_row_stat_loading'] = 'Loading...';
$string['report_table_row_stat_not_ready'] = 'Please wait for up to a minute';
$string['report_table_row_stat_queued'] = 'Queued';
$string['report_table_row_view_report'] = 'View report';
$string['setting_group'] = '<strong>Quilgo<sup>®</sup> Proctoring</strong>';
$string['setup-additional-collector-description'] = 'Now please provide access to your screen';
$string['setup-additional-collector-title'] = 'Almost done';
$string['setup_camera_error_desc'] = 'In order to enable camera tracking you need to allow access to your camera. Please change access settings for you camera.';
$string['setup_camera_error_title'] = 'Camera access failed';
$string['setup_camera_hint'] = 'camera';
$string['setup_connection_hint'] = 'and';
$string['setup_consent_activity_tracking_enabled'] = 'I provide consent to record, process and store the proctoring data and share them with my teacher.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'I provide consent to record, process and store the proctoring data, including screenshots of my screen and photos of myself, and share them with my teacher.';
$string['setup_consent_camera_tracking_enabled'] = 'I provide consent to record, process and store the proctoring data, including photos of myself, and share them with my teacher.';
$string['setup_consent_provided_report'] = 'A report will be shown to your teacher once you finish your quiz attempt';
$string['setup_consent_screen_tracking_enabled'] = 'I provide consent to record, process and store the proctoring data, including screenshots of my screen, and share them with my teacher.';
$string['setup_consent_snapshots_from'] = 'Snapshots from your';
$string['setup_consent_snapshots_will_taken'] = 'will be taken during your attempt';
$string['setup_consent_to_start_quiz'] = 'To start an attempt you need to provide access to your';
$string['setup_disable_device_warning_attention'] = 'Attention!';
$string['setup_disable_device_warning_check_camera_only'] = 'I understand, I will NOT disable my camera access during my test';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'I understand, I will NOT disable my camera or screen access during my test';
$string['setup_disable_device_warning_check_screen_only'] = 'I understand, I will NOT disable my screen access during my test';
$string['setup_disable_device_warning_description_camera_only'] = 'Do NOT disable your camera during your test as this may affect your test results.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'Do NOT disable your camera or screen during your test as this may affect your test results.';
$string['setup_disable_device_warning_description_screen_only'] = 'Do NOT disable your screen during your test as this may affect your test results.';
$string['setup_finish_tick_box'] = 'Tick the box below to provide consent:';
$string['setup_finish_title'] = 'Do you see';
$string['setup_finish_your_screen'] = 'your screen';
$string['setup_finish_yourself'] = 'yourself';
$string['setup_not_supported_error_desc'] = 'Sorry, your device does not support camera/screen tracking. Please choose another device to take the quiz.';
$string['setup_not_supported_error_title'] = 'Device not supported';
$string['setup_provide_access_camera'] = 'Allow camera access';
$string['setup_provide_access_screen'] = 'Allow screen access';
$string['setup_required_error'] = 'You must finish the setup before start the quiz';
$string['setup_retry'] = 'Retry';
$string['setup_screen_area_error_desc'] = 'Please try again and make sure to choose the <strong>entire screen</strong> to share.';
$string['setup_screen_area_error_title'] = 'Wrong screen area selected';
$string['setup_screen_error_desc'] = 'In order to enable screen recording you need to allow access to your screen. Please also make sure you have not denied screen recording in your system preferences.';
$string['setup_screen_error_title'] = 'Screen access failed';
$string['setup_screen_hint'] = 'screen';
$string['setup_tracking_enabled_hint'] = 'tracking is enabled';
$string['upgrade_failed_info'] = 'Something went wrong when try to upgrade Quilgo. Please try again later.';
