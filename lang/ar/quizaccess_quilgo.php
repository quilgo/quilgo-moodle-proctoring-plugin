<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Arabic strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'حدث خطأ أثناء محاولة تسجيل موقعك. يرجى المحاولة مرة أخرى لاحقًا';
$string['config_register'] = 'سجل موقعي';
$string['config_success_register'] = 'تم تسجيل موقعك بنجاح';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'لم يتم تفعيل إضافة المراقبة في Quilgo بعد. هذا بسبب فشل تسجيل موقعك في المراقبة في Quilgo أثناء تثبيت الإضافة. يمكنك محاولة التسجيل مرة أخرى بالنقر على "سجل موقعي" أدناه.';
$string['confirm_email_ask_me_later'] = 'اسألني لاحقا';
$string['confirm_email_button_text'] = 'تأكيد عنوان البريد الإلكتروني';
$string['confirm_email_default_failed_message'] = 'حدث خطأ ما أثناء محاولة تأكيد عنوان بريدك الإلكتروني. يُرجى المحاولة مرة أخرى لاحقًا والتأكد من استخدام عنوان البريد الإلكتروني الصحيح.';
$string['confirm_email_finished'] = 'شكرًا لك على تأكيد عنوان البريد الإلكتروني. استمتع باستخدام Quilgo!';
$string['confirm_email_message'] = 'يرجى تأكيد عنوان بريدك الإلكتروني الحالي لضمان حصولك على تحديثات الأمان والميزات المهمة.';
$string['confirm_email_short_desc'] = 'Quilgo هو مكون إضافي قابل للتطوير بدرجة كبيرة يتتبع الكاميرا والشاشة والنشاط، وينتج تقارير مراقبة عند اكتمال الاختبار.';
$string['confirm_email_title'] = 'شكرا لاختيارك كويلجو';
$string['create_session_error_default_description'] = 'فشل إعداد المراقبة. يرجى محاولة تحديث صفحة الاختبار.';
$string['create_session_error_limit_reached_description'] = 'لقد قام معلمك بتمكين المراقبة الآلية لهذا الاختبار ولكن تم الوصول إلى الحد الأقصى. يرجى المحاولة مرة أخرى لبدء الاختبار خلال بضع دقائق.';
$string['general_continue'] = 'يكمل';
$string['general_yes'] = 'نعم';
$string['limit_info_contact_admin'] = 'اتصل بمسؤول Moodle الخاص بك للترقية أو اتصل بنا على <strong>hello@quilgo.com</strong> إذا كنت بحاجة إلى نسخة تجريبية لعدد أكبر من الطلاب.';
$string['limit_info_run_up'] = 'يسمح برنامج Quilgo Free بتشغيل ما يصل إلى <strong><span class="free-limitation">0</span> من محاولات الاختبار الخاضعة للمراقبة في وقت واحد</strong>. تتم مشاركة هذا الحد عبر جميع الاختبارات.';
$string['limit_info_upgrade'] = 'انقر {$a}<strong><span class="quilgo-upgrade-cta">هنا للترقية</span></strong> أو اتصل بنا على <strong>hello@quilgo.com</strong> إذا كنت بحاجة إلى نسخة تجريبية لعدد أكبر من الطلاب.';
$string['manage_subscription_failed_info'] = 'حدث خطأ ما عند محاولة إدارة اشتراك Quilgo. الرجاء معاودة المحاولة في وقت لاحق.';
$string['manage_subscription_title'] = 'إدارة اشتراكي في Quilgo';
$string['plasm_camera'] = 'راقب الكاميرا';
$string['plasm_camera_help'] = 'تأكد من أن المستجيبين لا يغادرون مقاعدهم حتى ينتهي الاختبار';
$string['plasm_enabled'] = 'تمكين المراقبة';
$string['plasm_focus'] = 'تتبع النشاط (ممكّن افتراضيًا)';
$string['plasm_focus_help'] = 'اعرف عدد المرات التي يترك فيها المشاركون في الاختبار الامتحان لعلامة تبويب أو تطبيق آخر';
$string['plasm_force'] = 'الرقابة الإجبارية';
$string['plasm_force_help'] = 'يتطلب طرق المراقبة المختارة لمحاولة الاختبار';
$string['plasm_screen'] = 'راقب الشاشة';
$string['plasm_screen_help'] = 'سجل شاشة المستجيبين والأنشطة المشبوهة تلقائيًا بجودة عالية لمنع التصرفات غير العادلة';
$string['pluginname'] = 'المراقبة في Quilgo';
$string['privacy:export:quizaccess_quilgo_reports'] = 'تقرير مراقبة كويلغو';
$string['privacy:export:quizaccess_quilgo_settings'] = 'إعدادات مراقبة Quilgo';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'تم تعطيل تتبع الكاميرا';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'تم تمكين تتبع الكاميرا';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'تم تعطيل تتبع القوة';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'تم تمكين تتبع القوة';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'تم تعطيل تتبع الشاشة';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'تم تمكين تتبع الشاشة';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'يرسل هذا البرنامج المساعد البيانات خارجيًا إلى Quilgo لمراقبة التقرير.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'حدث نافذة التركيز أو إلغاء التركيز أ أثناء محاولة الاختبار.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'الصور الملتقطة من الكاميرا أو الشاشة أثناء محاولة الاختبار.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'معلومات حول جلسة المراقبة لمحاولة اختبار المستخدم.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'معرف محاولة اختبار المستخدم.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'يتم تمكين حالة تتبع الكاميرا أو تعطيلها عندما يحاول المستخدم إجراء اختبار.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'وصف الخطأ إذا كان هناك خطأ أثناء إنشاء تقرير المراقبة عندما ينتهي المستخدم من محاولة إجراء اختبار.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'يتم تمكين حالة تتبع القوة أو تعطيلها عندما يحاول المستخدم إجراء اختبار.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'معرف جلسة المراقبة.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'يتم تمكين حالة تتبع الشاشة أو تعطيلها عندما يحاول المستخدم إجراء اختبار.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'معلومات حول إعدادات المراقبة للاختبار.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'تم تمكين أو تعطيل تتبع الكاميرا.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'تم تمكين تتبع القوة أو تعطيله.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'معرف الاختبار الذي يستخدم المراقبة.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'تم تمكين أو تعطيل تتبع الشاشة.';
$string['proctoring_disabled_info'] = 'المراقبة الآلية <i>معطلة</i>. للتمكين، انتقل إلى <a href="{$a}"><u>إعدادات الاختبار</u></a>، وقم بتوسيع "القيود الإضافية على المحاولات"، وحدد مربع "تمكين المراقبة"، واختر التتبع الطرق، انقر على "حفظ"، ثم عد إلى هذه الصفحة.';
$string['proctoring_setup_failed_title'] = 'فشل إعداد المراقبة';
$string['refresh_quiz_page'] = 'تحديث الاختبار';
$string['report_camera_tracking'] = 'مراقبة الكاميرا:';
$string['report_detail_title'] = 'تقرير المراقبة';
$string['report_empty'] = 'المراقبة الآن مفعلة. ستظهر النتائج هنا بعد إجراء محاولة واحدة على الأقل للامتحان.';
$string['report_error'] = 'حدث خطأ أثناء الحصول على تقرير المراقبة. يرجى المحاولة مرة أخرى لاحقًا';
$string['report_expired'] = 'تم حذف الصور من التقرير بسبب انتهاء صلاحيتها';
$string['report_expires_in'] = 'ستتم إزالة الصور من النظام خلال {$a} من الأيام';
$string['report_face_presence'] = 'وجود الوجه:';
$string['report_focus_good'] = 'جيد';
$string['report_focus_not_good'] = 'غادر الاختبار {$a} مرات';
$string['report_focus_not_good_multiple'] = 'الاختبار الأيسر {$a} مرة';
$string['report_left_test'] = 'نشاط:';
$string['report_link_caption'] = 'عرض تقرير المراقبة | Quilgo<sup>®</sup>';
$string['report_notready'] = 'تقرير المراقبة الخاص بك ليس جاهزًا بعد. يرجى الانتظار قليلًا.';
$string['report_patterns_detected'] = 'الأنماط المكتشفة:';
$string['report_patterns_see_answer_below'] = 'انظر الإجابات أدناه';
$string['report_patterns_step_change_answer'] = 'تغيير الإجابة';
$string['report_patterns_step_copy_question'] = 'نسخ السؤال';
$string['report_patterns_step_leave_test'] = 'ترك الاختبار';
$string['report_patterns_step_paste_answer'] = 'لصق الإجابة';
$string['report_patterns_step_return'] = 'يعود';
$string['report_patterns_used_multiple_screens'] = 'شاشات متعددة مستعملة:';
$string['report_preview_faces_detected'] = ' | وجوه تم اكتشافها: {$a}';
$string['report_preview_info'] = "تم تتبع <strong>الدقيقة الأولى</strong> فقط من هذه المحاولة لأنها <strong>محاولة معاينة</strong>. سيتم تتبع <strong>محاولة الطالب</strong> <strong>بشكل كامل</strong>";
$string['report_preview_page_unfocused'] = ' | غير مركز';
$string['report_preview_time'] = 'الوقت:';
$string['report_preview_title'] = 'معاينة';
$string['report_proctoring_methods'] = 'طرق المراقبة:';
$string['report_screen_tracking'] = 'مراقبة الشاشة:';
$string['report_setting_camera'] = 'الكاميرا';
$string['report_setting_focus'] = 'نشاط';
$string['report_setting_screen'] = 'الشاشة';
$string['report_settings_recommendation'] = 'نوصي بتمكين <strong>تتبع الكاميرا والشاشة</strong> لتحسين المراقبة. انتقل إلى <a href="{$a}" target="_blank"><u>إعدادات الاختبار</u></a>، وقم بتوسيع قسم "القيود الإضافية على المحاولات"، ثم حدد طرق التتبع وانقر على "حفظ" " لتمكين طرق التتبع الإضافية.';
$string['report_suspicious_caption'] = '{$a} مشبوه';
$string['report_suspicious_screenshots'] = 'لقطات الشاشة المشبوهة:';
$string['report_table_header_attempt'] = 'محاولة #';
$string['report_table_header_attempt_results'] = 'نتائج المحاولة';
$string['report_table_header_confidence_levels'] = 'مراقبة مستوى الثقة';
$string['report_table_header_email'] = 'البريد الإلكتروني';
$string['report_table_header_name'] = 'الاسم';
$string['report_table_header_proctoring_report'] = 'تقرير المراقبة';
$string['report_table_header_score'] = 'النتيجة';
$string['report_table_header_time_taken'] = 'المدة الزمنية';
$string['report_table_header_timefinish'] = 'مكتمل';
$string['report_table_row_confidence_level_high'] = 'عالي';
$string['report_table_row_confidence_level_low'] = 'قليل';
$string['report_table_row_confidence_level_moderate'] = 'معتدل';
$string['report_table_row_notyetgraded'] = 'لم يتم تقييمه بعد';
$string['report_table_row_overdue'] = 'متأخر: {$a}';
$string['report_table_row_review_attempt'] = 'مراجعة المحاولة';
$string['report_table_row_stat_loading'] = 'تحميل...';
$string['report_table_row_stat_not_ready'] = 'يرجى الانتظار لمدة تصل إلى دقيقة';
$string['report_table_row_stat_queued'] = 'في قائمة الانتظار';
$string['report_table_row_view_report'] = 'عرض التقرير';
$string['setting_group'] = '<strong>المراقبة في Quilgo<sup>®</sup></strong>';
$string['setup-additional-collector-description'] = 'الآن يرجى منح الوصول إلى شاشتك';
$string['setup-additional-collector-title'] = 'شارف على الانتهاء';
$string['setup_camera_error_desc'] = 'لتفعيل المراقبة بالكاميرا، يجب السماح بالوصول إلى كاميرتك. يرجى تغيير إعدادات الوصول لكاميرتك.';
$string['setup_camera_error_title'] = 'فشل الوصول للكاميرا';
$string['setup_camera_hint'] = 'كاميرا';
$string['setup_connection_hint'] = 'و';
$string['setup_consent_activity_tracking_enabled'] = 'أوافق على تسجيل بيانات المراقبة ومعالجتها وتخزينها ومشاركتها مع معلمي.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'أوافق على تسجيل بيانات المراقبة ومعالجتها وتخزينها، بما في ذلك لقطات الشاشة لشاشتي والصور الخاصة بي، ومشاركتها مع معلمي.';
$string['setup_consent_camera_tracking_enabled'] = 'أوافق على تسجيل بيانات المراقبة ومعالجتها وتخزينها، بما في ذلك الصور الخاصة بي، ومشاركتها مع معلمي.';
$string['setup_consent_provided_report'] = 'سيتم عرض تقرير لمعلمك بعد أن تنهي محاولتك للاختبار';
$string['setup_consent_screen_tracking_enabled'] = 'أوافق على تسجيل بيانات المراقبة ومعالجتها وتخزينها، بما في ذلك لقطات الشاشة لشاشتي ومشاركتها مع معلمي.';
$string['setup_consent_snapshots_from'] = 'لقطات من';
$string['setup_consent_snapshots_will_taken'] = 'ستؤخذ أثناء محاولتك';
$string['setup_consent_to_start_quiz'] = 'لبدء، تحتاج إلى إعطاء الوصول إلى';
$string['setup_disable_device_warning_attention'] = 'انتباه!';
$string['setup_disable_device_warning_check_camera_only'] = 'أتفهم أنني لن أقوم بتعطيل وصول الكاميرا الخاصة بي أثناء الاختبار';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'أتفهم أنني لن أقوم بتعطيل الوصول إلى الكاميرا أو الشاشة أثناء الاختبار';
$string['setup_disable_device_warning_check_screen_only'] = 'أتفهم أنني لن أقوم بتعطيل الوصول إلى شاشتي أثناء الاختبار';
$string['setup_disable_device_warning_description_camera_only'] = 'لا تقم بتعطيل الكاميرا أثناء الاختبار لأن هذا قد يؤثر على نتائج الاختبار.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'لا تقم بتعطيل الكاميرا أو الشاشة أثناء الاختبار لأن هذا قد يؤثر على نتائج الاختبار.';
$string['setup_disable_device_warning_description_screen_only'] = 'لا تقم بتعطيل الشاشة أثناء الاختبار لأن هذا قد يؤثر على نتائج الاختبار.';
$string['setup_finish_tick_box'] = 'ضع علامة في المربع أدناه لإعطاء الموافقة:';
$string['setup_finish_title'] = 'ماذا ترى؟';
$string['setup_finish_your_screen'] = 'شاشتك';
$string['setup_finish_yourself'] = 'نفسك';
$string['setup_not_supported_error_desc'] = 'عذرًا، جهازك لا يدعم مراقبة الكاميرا/الشاشة. يرجى اختيار جهاز آخر لإجراء الاختبار';
$string['setup_not_supported_error_title'] = 'الجهاز غير مدعوم';
$string['setup_provide_access_camera'] = 'السماح بالوصول للكاميرا';
$string['setup_provide_access_screen'] = 'السماح بالوصول للشاشة';
$string['setup_required_error'] = 'يجب عليك إكمال الإعداد قبل بدء الاختبار';
$string['setup_retry'] = 'حاول مرة أخرى';
$string['setup_screen_area_error_desc'] = 'يرجى المحاولة مرة أخرى وتأكد من اختيار <strong>الشاشة بأكملها</strong> للمشاركة.';
$string['setup_screen_area_error_title'] = 'تم اختيار منطقة الشاشة بشكل خاطئ';
$string['setup_screen_error_desc'] = 'لتفعيل تسجيل الشاشة، يجب السماح بالوصول إلى شاشتك. تأكد أيضًا من عدم رفض تسجيل الشاشة في تفضيلات نظامك.';
$string['setup_screen_error_title'] = 'فشل الوصول للشاشة';
$string['setup_screen_hint'] = 'شاشة';
$string['setup_tracking_enabled_hint'] = 'المراقبة مفعلة';
$string['upgrade_failed_info'] = 'حدث خطأ ما أثناء محاولة ترقية Quilgo. الرجاء معاودة المحاولة في وقت لاحق.';
