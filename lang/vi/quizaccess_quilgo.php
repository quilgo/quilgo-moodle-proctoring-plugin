<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Vietnamese strings.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['config_api_token'] = 'API TOKEN';
$string['config_client_token'] = 'CLIENT TOKEN';
$string['config_failed_register'] = 'Có lỗi xảy ra khi cố gắng đăng ký trang web của bạn. Vui lòng thử lại sau';
$string['config_register'] = 'Đăng ký trang web của tôi';
$string['config_success_register'] = 'Trang web của bạn đã được đăng ký thành công';
$string['config_title'] = 'Quilgo';
$string['config_warning_not_active_yet'] = 'Plugin Giám Sát Quilgo của bạn chưa được kích hoạt. Điều này là do trang web của bạn không đăng ký thành công với Giám Sát Quilgo trong quá trình cài đặt plugin. Bạn có thể thử đăng ký lại bằng cách nhấp vào "Đăng ký trang web của tôi" bên dưới.';
$string['confirm_email_ask_me_later'] = 'Hỏi tôi sau nhé';
$string['confirm_email_button_text'] = 'Xác nhận địa chỉ email';
$string['confirm_email_default_failed_message'] = 'Có lỗi xảy ra khi cố gắng xác nhận địa chỉ email của bạn. Vui lòng thử lại sau và đảm bảo bạn sử dụng địa chỉ email hợp lệ.';
$string['confirm_email_finished'] = 'Cảm ơn bạn đã xác nhận địa chỉ email. Chúc bạn sử dụng Quilgo vui vẻ!';
$string['confirm_email_message'] = 'Vui lòng xác nhận địa chỉ email hiện tại của bạn để đảm bảo bạn nhận được các bản cập nhật quan trọng về bảo mật và tính năng.';
$string['confirm_email_short_desc'] = 'Quilgo là một plugin có khả năng mở rộng cao, theo dõi camera, màn hình và hoạt động, đồng thời tạo báo cáo giám sát khi hoàn thành bài kiểm tra.';
$string['confirm_email_title'] = 'Cảm ơn bạn đã chọn Quilgo';
$string['create_session_error_default_description'] = 'Thiết lập Proctoring đã thất bại. Hãy thử làm mới trang bài kiểm tra.';
$string['create_session_error_limit_reached_description'] = 'Giáo viên của bạn đã kích hoạt tính năng giám sát tự động cho bài kiểm tra này nhưng đã đạt đến giới hạn. Vui lòng thử lại để bắt đầu bài kiểm tra sau vài phút nữa.';
$string['general_continue'] = 'Tiếp tục';
$string['general_yes'] = 'Đúng';
$string['limit_info_contact_admin'] = 'Hãy liên hệ với quản trị viên Moodle của bạn để nâng cấp hoặc liên hệ với chúng tôi theo địa chỉ <strong>hello@quilgo.com</strong> nếu bạn cần bản dùng thử cho số lượng học viên lớn hơn.';
$string['limit_info_run_up'] = 'Quilgo Free cho phép chạy đồng thời tối đa <strong><span class="free-limitation">0</span> lần thử nghiệm được giám sát</strong>. Giới hạn này được chia sẻ trên tất cả các câu đố.';
$string['limit_info_upgrade'] = 'Nhấp vào {$a}<strong><span class="quilgo-upgrade-cta">đây để nâng cấp</span></strong> hoặc liên hệ với chúng tôi theo địa chỉ <strong>hello@quilgo.com</strong> nếu bạn cần dùng thử cho số lượng học sinh lớn hơn.';
$string['manage_subscription_failed_info'] = 'Đã xảy ra lỗi khi cố gắng quản lý đăng ký Quilgo. Vui lòng thử lại sau.';
$string['manage_subscription_title'] = 'Quản lý đăng ký Quilgo của tôi';
$string['plasm_camera'] = 'Giám sát camera';
$string['plasm_camera_help'] = 'Đảm bảo rằng người trả lời không rời khỏi chỗ ngồi của họ cho đến khi bài quiz hoàn thành';
$string['plasm_enabled'] = 'Bật giám sát';
$string['plasm_focus'] = 'Theo dõi hoạt động (được bật theo mặc định)';
$string['plasm_focus_help'] = 'Xem số lần người tham gia bài kiểm tra rời bỏ bài quiz để chuyển sang tab hoặc ứng dụng khác';
$string['plasm_force'] = 'Giám sát bắt buộc';
$string['plasm_force_help'] = 'Yêu cầu phương pháp giám sát được chọn để thử làm quiz';
$string['plasm_screen'] = 'Giám sát màn hình';
$string['plasm_screen_help'] = 'Tự động ghi lại màn hình và hoạt động đáng ngờ của người trả lời với chất lượng cao để ngăn chặn hành vi không công bằng';
$string['pluginname'] = 'Giám Sát Quilgo';
$string['privacy:export:quizaccess_quilgo_reports'] = 'Báo cáo giám sát Quilgo';
$string['privacy:export:quizaccess_quilgo_settings'] = 'Cài đặt giám sát Quilgo';
$string['privacy:export:quizaccess_quilgo_settings:camera_disabled'] = 'Đã tắt tính năng theo dõi của máy ảnh';
$string['privacy:export:quizaccess_quilgo_settings:camera_enabled'] = 'Đã bật theo dõi camera';
$string['privacy:export:quizaccess_quilgo_settings:force_disabled'] = 'Theo dõi lực lượng bị vô hiệu hóa';
$string['privacy:export:quizaccess_quilgo_settings:force_enabled'] = 'Đã bật theo dõi lực lượng';
$string['privacy:export:quizaccess_quilgo_settings:screen_disabled'] = 'Theo dõi màn hình bị vô hiệu hóa';
$string['privacy:export:quizaccess_quilgo_settings:screen_enabled'] = 'Đã bật tính năng theo dõi màn hình';
$string['privacy:metadata:quizaccess_quilgo_proctoring'] = 'Plugin này gửi dữ liệu ra bên ngoài tới Quilgo để giám sát báo cáo.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:focuses'] = 'Sự kiện cửa sổ tập trung hoặc không tập trung trong quá trình làm bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_proctoring:images'] = 'Hình ảnh được chụp từ máy ảnh hoặc màn hình trong khi làm bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_reports'] = 'Thông tin về phiên giám sát của bài kiểm tra của người dùng.';
$string['privacy:metadata:quizaccess_quilgo_reports:attemptid'] = 'ID của bài kiểm tra của người dùng.';
$string['privacy:metadata:quizaccess_quilgo_reports:camera_enabled'] = 'Trạng thái theo dõi của camera được bật hoặc tắt khi người dùng thực hiện bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_reports:error_reason'] = 'Mô tả lỗi nếu có lỗi trong quá trình tạo báo cáo giám sát khi người dùng hoàn thành bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_reports:force_enabled'] = 'Trạng thái theo dõi lực được bật hoặc tắt khi người dùng thực hiện bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_reports:plasmsessionid'] = 'ID của phiên giám sát.';
$string['privacy:metadata:quizaccess_quilgo_reports:screen_enabled'] = 'Trạng thái theo dõi màn hình được bật hoặc tắt khi người dùng thực hiện bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_settings'] = 'Thông tin về cài đặt giám sát của bài kiểm tra.';
$string['privacy:metadata:quizaccess_quilgo_settings:camera_enabled'] = 'Tính năng theo dõi camera được bật hoặc tắt.';
$string['privacy:metadata:quizaccess_quilgo_settings:force_enabled'] = 'Tính năng theo dõi lực được bật hoặc tắt.';
$string['privacy:metadata:quizaccess_quilgo_settings:quizid'] = 'ID của bài kiểm tra sử dụng giám thị.';
$string['privacy:metadata:quizaccess_quilgo_settings:screen_enabled'] = 'Tính năng theo dõi màn hình được bật hoặc tắt.';
$string['proctoring_disabled_info'] = 'Giám sát tự động bị <i>tắt</i>. Để bật, hãy đi tới <a href="{$a}"><u>Cài đặt câu hỏi</u></a>, mở rộng "Hạn chế bổ sung đối với các lần thử", đánh dấu vào ô "Bật giám sát", chọn theo dõi phương pháp, nhấp vào "Lưu" và quay lại trang này.';
$string['proctoring_setup_failed_title'] = 'Thiết lập giám sát không thành công';
$string['refresh_quiz_page'] = 'Làm mới bài kiểm tra';
$string['report_camera_tracking'] = 'Giám sát camera:';
$string['report_detail_title'] = 'Báo cáo Giám Sát';
$string['report_empty'] = 'Giám sát hiện đã được kích hoạt. Kết quả sẽ xuất hiện tại đây sau khi ít nhất một lần thử quiz được thực hiện.';
$string['report_error'] = 'Có lỗi xảy ra khi lấy báo cáo giám sát. Vui lòng thử lại sau';
$string['report_expired'] = 'Các hình ảnh đã bị xóa khỏi báo cáo do hết hạn';
$string['report_expires_in'] = 'Hình ảnh sẽ bị xóa khỏi hệ thống sau {$a} ngày';
$string['report_face_presence'] = 'Sự hiện diện của khuôn mặt:';
$string['report_focus_good'] = 'Tốt';
$string['report_focus_not_good'] = 'Rời bỏ bài kiểm tra {$a} lần';
$string['report_focus_not_good_multiple'] = 'Rời bỏ bài kiểm tra {$a} lần';
$string['report_left_test'] = 'Hoạt động:';
$string['report_link_caption'] = 'Xem báo cáo giám sát | Quilgo<sup>®</sup>';
$string['report_notready'] = 'Báo cáo giám sát của bạn chưa sẵn sàng. Vui lòng đợi thêm một chút nữa.';
$string['report_patterns_detected'] = 'Các mẫu được phát hiện:';
$string['report_patterns_see_answer_below'] = 'xem câu trả lời dưới đây';
$string['report_patterns_step_change_answer'] = 'Thay đổi câu trả lời';
$string['report_patterns_step_copy_question'] = 'Sao chép câu hỏi';
$string['report_patterns_step_leave_test'] = 'Rời khỏi bài kiểm tra';
$string['report_patterns_step_paste_answer'] = 'Dán câu trả lời';
$string['report_patterns_step_return'] = 'Trở lại';
$string['report_patterns_used_multiple_screens'] = 'Đã sử dụng nhiều màn hình:';
$string['report_preview_faces_detected'] = ' | Khuôn mặt được phát hiện: {$a}';
$string['report_preview_info'] = 'Chỉ <strong>phút đầu tiên</strong> của lần thử này được theo dõi vì đây là một <strong>lần thử xem trước</strong>. <strong>Lần thử của học sinh</strong> bạn sẽ được <strong>theo dõi đầy đủ</strong>';
$string['report_preview_page_unfocused'] = ' | Không tập trung';
$string['report_preview_time'] = 'Thời gian:';
$string['report_preview_title'] = 'Xem trước';
$string['report_proctoring_methods'] = 'Phương pháp giám sát:';
$string['report_screen_tracking'] = 'Giám sát màn hình:';
$string['report_setting_camera'] = 'Camera';
$string['report_setting_focus'] = 'Hoạt động';
$string['report_setting_screen'] = 'Màn hình';
$string['report_settings_recommendation'] = 'Chúng tôi khuyên bạn nên bật cả <strong>theo dõi camera và màn hình</strong> để giám sát tốt hơn. Đi tới <a href="{$a}" target="_blank"><u>cài đặt bài kiểm tra</u></a>, mở rộng phần "Hạn chế bổ sung đối với các lần thử", đánh dấu vào các phương pháp theo dõi và nhấp vào "Lưu" để bật các phương pháp theo dõi bổ sung.';
$string['report_suspicious_caption'] = '{$a} nghi ngờ';
$string['report_suspicious_screenshots'] = 'Ảnh chụp màn hình:';
$string['report_table_header_attempt'] = 'Lần thử #';
$string['report_table_header_attempt_results'] = 'Kết quả Lần Thử';
$string['report_table_header_confidence_levels'] = 'Mức độ tin cậy của giám sát';
$string['report_table_header_email'] = 'Email';
$string['report_table_header_name'] = 'Tên';
$string['report_table_header_proctoring_report'] = 'Báo cáo Giám Sát';
$string['report_table_header_score'] = 'Điểm';
$string['report_table_header_time_taken'] = 'Thời gian thực hiện';
$string['report_table_header_timefinish'] = 'Hoàn thành';
$string['report_table_row_confidence_level_high'] = 'Cao';
$string['report_table_row_confidence_level_low'] = 'Thấp';
$string['report_table_row_confidence_level_moderate'] = 'Vừa phải';
$string['report_table_row_notyetgraded'] = 'Chưa được đánh giá';
$string['report_table_row_overdue'] = 'Quá hạn: {$a}';
$string['report_table_row_review_attempt'] = 'Xem lại lần thử';
$string['report_table_row_stat_loading'] = 'Đang tải...';
$string['report_table_row_stat_not_ready'] = 'Vui lòng đợi tối đa một phút';
$string['report_table_row_stat_queued'] = 'Đã xếp hàng';
$string['report_table_row_view_report'] = 'Xem báo cáo';
$string['setting_group'] = '<strong>Giám Sát Quilgo<sup>®</sup></strong>';
$string['setup-additional-collector-description'] = 'Bây giờ, vui lòng cung cấp quyền truy cập vào màn hình của bạn';
$string['setup-additional-collector-title'] = 'Gần hoàn tất';
$string['setup_camera_error_desc'] = 'Để kích hoạt giám sát camera, bạn cần cho phép truy cập vào camera của mình. Vui lòng thay đổi cài đặt truy cập cho camera của bạn.';
$string['setup_camera_error_title'] = 'Lỗi truy cập camera';
$string['setup_camera_hint'] = 'camera';
$string['setup_connection_hint'] = 'và';
$string['setup_consent_activity_tracking_enabled'] = 'Tôi đồng ý ghi lại, xử lý và lưu trữ dữ liệu giám sát và chia sẻ chúng với giáo viên của tôi.';
$string['setup_consent_camera_and_screen_tracking_enabled'] = 'Tôi đồng ý ghi lại, xử lý và lưu trữ dữ liệu giám sát, bao gồm ảnh chụp màn hình và ảnh của chính tôi, đồng thời chia sẻ chúng với giáo viên của tôi.';
$string['setup_consent_camera_tracking_enabled'] = 'Tôi đồng ý ghi lại, xử lý và lưu trữ dữ liệu giám sát, bao gồm cả ảnh của chính tôi và chia sẻ chúng với giáo viên của tôi.';
$string['setup_consent_provided_report'] = 'Báo cáo sẽ được hiển thị cho giáo viên của bạn sau khi bạn hoàn thành thử nghiệm quiz của mình';
$string['setup_consent_screen_tracking_enabled'] = 'Tôi đồng ý ghi lại, xử lý và lưu trữ dữ liệu giám sát, bao gồm ảnh chụp màn hình của tôi và chia sẻ chúng với giáo viên của tôi.';
$string['setup_consent_snapshots_from'] = 'Ảnh chụp từ';
$string['setup_consent_snapshots_will_taken'] = 'sẽ được chụp trong quá trình bạn thử nghiệm';
$string['setup_consent_to_start_quiz'] = 'Để bắt đầu, bạn cần cung cấp quyền truy cập vào';
$string['setup_disable_device_warning_attention'] = 'Chú ý!';
$string['setup_disable_device_warning_check_camera_only'] = 'Tôi hiểu, tôi sẽ KHÔNG vô hiệu hóa quyền truy cập máy ảnh của mình trong quá trình thử nghiệm';
$string['setup_disable_device_warning_check_camera_or_screen'] = 'Tôi hiểu, tôi sẽ KHÔNG tắt máy ảnh hoặc quyền truy cập màn hình trong quá trình kiểm tra của mình';
$string['setup_disable_device_warning_check_screen_only'] = 'Tôi hiểu, tôi sẽ KHÔNG vô hiệu hóa quyền truy cập màn hình của mình trong quá trình thử nghiệm';
$string['setup_disable_device_warning_description_camera_only'] = 'KHÔNG tắt camera trong khi làm bài kiểm tra vì điều này có thể ảnh hưởng đến kết quả kiểm tra.';
$string['setup_disable_device_warning_description_camera_or_screen'] = 'KHÔNG tắt camera hoặc màn hình trong khi làm bài kiểm tra vì điều này có thể ảnh hưởng đến kết quả kiểm tra của bạn.';
$string['setup_disable_device_warning_description_screen_only'] = 'KHÔNG tắt màn hình trong khi làm bài kiểm tra vì điều này có thể ảnh hưởng đến kết quả kiểm tra.';
$string['setup_finish_tick_box'] = 'Đánh dấu vào ô bên dưới để đồng ý:';
$string['setup_finish_title'] = 'Bạn có thấy gì';
$string['setup_finish_your_screen'] = 'màn hình của bạn';
$string['setup_finish_yourself'] = 'bản thân bạn';
$string['setup_not_supported_error_desc'] = 'Rất tiếc, thiết bị của bạn không hỗ trợ giám sát camera/màn hình. Vui lòng chọn một thiết bị khác để thực hiện quiz';
$string['setup_not_supported_error_title'] = 'Thiết bị không được hỗ trợ';
$string['setup_provide_access_camera'] = 'Cho phép truy cập camera';
$string['setup_provide_access_screen'] = 'Cho phép truy cập màn hình';
$string['setup_required_error'] = 'Bạn phải hoàn thành thiết lập trước khi bắt đầu quiz';
$string['setup_retry'] = 'Thử lại';
$string['setup_screen_area_error_desc'] = 'Vui lòng thử lại và chắc chắn rằng bạn đã chọn <strong>toàn bộ màn hình</strong> để chia sẻ.';
$string['setup_screen_area_error_title'] = 'Lựa chọn khu vực màn hình không chính xác';
$string['setup_screen_error_desc'] = 'Để kích hoạt ghi màn hình, bạn cần cho phép truy cập vào màn hình của mình. Hãy chắc chắn rằng bạn không từ chối ghi màn hình trong tùy chọn hệ thống của bạn.';
$string['setup_screen_error_title'] = 'Lỗi truy cập màn hình';
$string['setup_screen_hint'] = 'màn hình';
$string['setup_tracking_enabled_hint'] = 'giám sát được kích hoạt';
$string['upgrade_failed_info'] = 'Đã xảy ra lỗi khi cố gắng nâng cấp Quilgo. Vui lòng thử lại sau.';
