const webpackConfig = require("./webpack.config.js");

const config = (grunt) => {
  grunt.initConfig({
    webpack: {
      prod: webpackConfig,
      dev: { ...webpackConfig, watch: true },
    },
    stylelint: {
      options: {
        configFile: ".stylelintrc.json",
      },
      src: ["**/*.css"],
    },
  });

  grunt.loadNpmTasks("grunt-webpack");
  grunt.loadNpmTasks("grunt-stylelint");

  grunt.registerTask("dev", ["webpack:dev"]);
  // amd and stylelint:css will executed by Moodle root Gruntfile.js
  grunt.registerTask("amd", ["webpack:prod"]);
  grunt.registerTask("stylelint:css", ["stylelint"]);
};

module.exports = config;
