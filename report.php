<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Proctoring report list page
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use quizaccess_quilgo\local\report\report_list;

require_once(__DIR__ . '/../../../../config.php');
require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');
// Starting moodle 4.2, 'quiz' from autoloader is depcrecated. Suggested to use from 'mod\quiz_settings'.
if (class_exists('mod_quiz\quiz_settings')) {
    class_alias('\mod_quiz\quiz_settings', '\quiz_settings_alias');
} else {
    require_once($CFG->dirroot . '/mod/quiz/locallib.php');
    class_alias('\quiz', '\quiz_settings_alias');
}

$cid = required_param('cid', PARAM_INT);
$cmid = required_param('cmid', PARAM_INT);
$qid = required_param('qid', PARAM_INT);

$context = context_module::instance($cmid, MUST_EXIST);

list($course, $cm) = get_course_and_cm_from_cmid($cmid, 'quiz');

$PAGE->set_context($context);
$PAGE->set_cm($cm, $course);

require_login($course, true, $cm);

require_capability('quizaccess/quilgo:viewreport', $context);

$reporturl = quizaccess_quilgo_get_report_url($cid, $cmid, $qid);
$reportcaption = get_string('report_link_caption', 'quizaccess_quilgo');
$quizobj = quiz_settings_alias::create($qid);
$sentryoptions = quizaccess_quilgo_get_sentry_client_options();

$PAGE->set_url($reporturl);
$PAGE->set_pagelayout('report');
$PAGE->set_title("$course->shortname: $reportcaption");
$PAGE->set_heading("$course->fullname: $reportcaption");
$PAGE->navbar->add($reportcaption, $reporturl);
$PAGE->requires->css('/mod/quiz/accessrule/quilgo/styles.css');
$PAGE->requires->js_call_amd(
  'quizaccess_quilgo/report',
  'init',
  [$sentryoptions],
);
$PAGE->requires->js_call_amd(
  'quizaccess_quilgo/stat',
  'init',
  [$sentryoptions],
);

echo $OUTPUT->header();

$reportlist = new report_list($cid, $cmid, $qid, $quizobj);
$reportlist->show();

echo $OUTPUT->footer();
