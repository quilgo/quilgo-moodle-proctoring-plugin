# Quilgo® Proctoring for Moodle

Quilgo® is the leading online assessment and proctoring software that makes examination process easy and trustworthy. Quilgo® Proctoring for Moodle allows you to enable camera, screen and activity tracking and view proctoring reports on submitted quiz attempts.

## Features
- Capture camera snapshots throughout quiz attempts
- Capture suspicious screenshots
- Force camera and screen tracking if enabled
- View proctoring reports
- Works with any existing quiz setup

## Install

1. Download the plugin .zip file
2. Unzip the file in **/mod/quiz/accessrule/quilgo** folder of your Moodle installation _or_ upload the zip file via **Site Administration -> Plugins -> Install Plugins -> Upload zip file**

## Configure

1. Go to your quiz _Settings_ page

    <kbd><img src="https://drive.google.com/uc?id=1slZCGrYsxDsX4D22zulVY9iVW0jQGNsI" width="400px"/></kbd>
2. Expand the _"Extra restrictions on attempts"_ section

    <kbd><img src="https://drive.google.com/uc?id=1j83bitOuNhMzihaS5eHkO-nOdYtdc5CX" width="400px"/></kbd>
3. Enable the needed proctoring methods.

## Test yourself

1. Go to your quiz page

    <kbd><img src="https://drive.google.com/uc?id=17yJ6U9a3ER_LHHOwzWjMG23jqUEXrD-W" width="400px"/></kbd>
2. Click the "Preview test" button 
3. Read the instructions carefully and proceed with granting access to the camera and/or screen.

    <kbd><img src="https://drive.google.com/uc?id=1bC_n1Uk0Xalhll9_UhMs7iTAzXdedq_6" width="400px"/></kbd>
4. Provide consent and start an attempt.

_**Note**: in the preview mode only the first minute of the attempt is recorded. In real attempts the length of the record depends on the quiz duration._ 

## View Reports

1. Go to your quiz page

   <kbd><img src="https://drive.google.com/uc?id=17yJ6U9a3ER_LHHOwzWjMG23jqUEXrD-W" width="400px"/></kbd>
2. Click the "View proctoring reports" button 

    <kbd><img src="https://drive.google.com/uc?id=1fivcp3yXbi-fTotFV3MJKDKdVKTS5nL_" width="200px"/></kbd>
3. Click the "View proctoring" button corresponding to an attempt

    <kbd><img src="https://drive.google.com/uc?id=1hJuUUtJ0h_WSuQyODW2iZd-0jKRPrfao" width="400px"/></kbd>
    
    <kbd><img src="https://drive.google.com/uc?id=1esD1gOHn-bxbAxdfKfSR4TC7V48gOMFp" width="200px"/></kbd>
   


## Important information

### Screen tracking and mobile devices

Screen tracking is not supported on mobile devices including tablets. If you enable screen tracking and **do not force** the selected tracking methods, the screen tracking will be skipped for the students who use mobile devices. If you enable screen tracking and **force** the selected tracking methods, then the students who use a mobile device will need to switch their device to a desktop or a laptop to take a quiz attempt.

## Feedback

Please send us your feedback at hello@quilgo.com.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
