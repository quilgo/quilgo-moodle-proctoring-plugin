/* eslint-disable import/no-extraneous-dependencies */
require("dotenv").config();

const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");

module.exports = {
  mode: process.env.NODE_ENV === "development" ? "development" : "production",
  context: path.join(__dirname, "amd"),
  entry: {
    "plasm-service": {
      import: ["./src/plasm-service.js"],
      filename: "plasm-service.min.js",
    },
    report: {
      import: ["./src/report.js"],
      filename: "report.min.js",
    },
    stat: {
      import: ["./src/stat.js"],
      filename: "stat.min.js",
    },
    sentry: {
      import: ["./src/sentry.js"],
      filename: "sentry.min.js",
    },
    utils: {
      import: ["./src/utils.js"],
      filename: "utils.min.js",
    },
    setting: {
      import: ["./src/setting.js"],
      filename: "setting.min.js",
    },
    application: {
      import: ["./src/application.js"],
      filename: "application.min.js",
    },
    "email-confirmation": {
      import: ["./src/email-confirmation.js"],
      filename: "email-confirmation.min.js",
    },
  },
  output: {
    library: "quizaccess_quilgo/[name]",
    libraryTarget: "amd",
    path: path.join(__dirname, "amd/build"),
    clean: true,
  },
  resolve: {
    extensions: [".js"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader", "eslint-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: { sourceMap: true },
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [autoprefixer],
              },
            },
          },
        ],
      },
      {
        test: /\.css/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
  performance: {
    maxEntrypointSize: 1300000,
    maxAssetSize: 1000000,
  },
  externals: {
    jquery: {
      amd: "jquery",
    },
    "core/ajax": {
      amd: "core/ajax",
    },
    "core/modal_factory": {
      amd: "core/modal_factory",
    },
    "core/modal": {
      amd: "core/modal",
    },
    "core/templates": {
      amd: "core/templates",
    },
    "core/str": {
      amd: "core/str",
    },
    "core/modal_events": {
      amd: "core/modal_events",
    },
    "quizaccess_quilgo/utils": {
      amd: "quizaccess_quilgo/utils",
    },
    "quizaccess_quilgo/sentry": {
      amd: "quizaccess_quilgo/sentry",
    },
  },
};
