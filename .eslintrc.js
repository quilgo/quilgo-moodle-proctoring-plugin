module.exports = {
  root: true,
  env: {
    amd: true,
    browser: true,
    es2021: true,
  },
  ignorePatterns: ["**/build/*.js"],
  extends: ["airbnb-base", "prettier"],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
  },
  rules: {
    "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
    "no-param-reassign": 0,
    "import/no-amd": 0,
  },
};
