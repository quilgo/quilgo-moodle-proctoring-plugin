<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Add plugin administration link.
 * @package     quizaccess_quilgo
 * @copyright   2023 Native Platform Ltd <hello@quilgo.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot. '/mod/quiz/accessrule/quilgo/lib.php');

if ($hassiteconfig) {
    global $PAGE;

    $apiconfigurl = new moodle_url(QUIZACCESS_QUILGO_API_SETTINGS_PATH);
    $apiconfigpage = new admin_externalpage(
        'quizaccess_quilgo_settings',
        get_string('config_title', 'quizaccess_quilgo'),
        $apiconfigurl->out(false),
    );
    $ADMIN->add('modsettingsquizcat', $apiconfigpage);

    $isshouldconfirmemail = quizaccess_quilgo_is_should_confirm_email();
    if ($isshouldconfirmemail) {
        $sentryoptions = quizaccess_quilgo_get_sentry_client_options();
        $PAGE->requires->js_call_amd(
            'quizaccess_quilgo/email-confirmation',
            'init',
            [$sentryoptions],
        );
    }
}
